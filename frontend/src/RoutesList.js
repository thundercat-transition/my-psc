import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Route, Switch } from "react-router-dom";

// Constants
import { PATH } from "./resources/constants";

// Components
import PrivateRoute from "./components/commons/PrivateRoute";
import SplashPage from "./pages/SplashPage";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import Error404Page from "./pages/Error404Page";
import Status from "./pages/Status";
import RegistrationForm from "./pages/RegistrationForm";
import Dashboard from "./pages/Dashboard";

export const RoutesList = props => {
  const { authenticated, hasAccount } = props;

  return (
    <Switch>
      <Route path={PATH.splash} exact component={SplashPage} />
      <Route path={PATH.home} exact component={HomePage} />

      <PrivateRoute
        exact
        auth={!authenticated}
        path={PATH.login}
        component={LoginPage}
        redirectTo={PATH.home}
      />

      <PrivateRoute
        auth={authenticated}
        path={PATH.registrationForm}
        component={RegistrationForm}
      />

      <PrivateRoute
        auth={authenticated && hasAccount}
        path={PATH.dashboard}
        component={Dashboard}
      />

      <Route path={PATH.status} exact component={Status} />

      {/* Handles all other routes; has to be last, since Switch maps for the first match */}
      <Route component={Error404Page} />
    </Switch>
  );
};

RoutesList.propTypes = {
  authenticated: PropTypes.bool
};

RoutesList.defaultProps = {
  authenticated: false
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.loginReducer.authenticated,
    hasAccount: state.loginReducer.hasAccount
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RoutesList);
