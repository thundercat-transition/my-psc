import { RESET_REDUX } from "./actionTypes";

// eslint-disable-next-line import/prefer-default-export
export const resetRedux = () => {
  return {
    type: RESET_REDUX
  };
};
