import {
  SET_AUTHENTICATED_STATE,
  SET_CURRENT_USER_INFO,
  SET_HAS_ACCOUNT_STATE,
  SET_AUTH_AND_HAS_ACCOUNT_STATE,
  REGISTRATION
} from "./actionTypes";

export const setAuthenticationState = authenticated => {
  return {
    type: SET_AUTHENTICATED_STATE,
    payload: authenticated
  };
};

export const setCurrentUserInfo = userInfo => {
  return {
    type: SET_CURRENT_USER_INFO,
    payload: userInfo
  };
};

export const setHasAccount = hasAccount => {
  return {
    type: SET_HAS_ACCOUNT_STATE,
    payload: hasAccount
  };
};

export const setAuthAndHasAccount = (authenticated, hasAccount) => {
  return {
    type: SET_AUTH_AND_HAS_ACCOUNT_STATE,
    authenticated: authenticated,
    hasAccount: hasAccount
  };
};

export const updatePageHasErrorState = pageHasError => ({
  type: REGISTRATION,
  pageHasError
});
