/**
 * This file contains all of the action type names (constants)
 * Action names are separated by their first alphabetical letter here
 */

export const RESET_REDUX = "RESET_REDUX";

export const SET_LANGUAGE = "SET_LANGUAGE";

export const SET_AUTHENTICATED_STATE = "SET_AUTHENTICATED_STATE";

export const SET_CURRENT_USER_INFO = "SET_CURRENT_USER_INFO";

export const SET_HAS_ACCOUNT_STATE = "SET_HAS_ACCOUNT_STATE";

export const SET_AUTH_AND_HAS_ACCOUNT_STATE = "SET_AUTH_AND_HAS_ACCOUNT_STATE";

export const REGISTRATION = "REGISTRATION";
