import {
  SET_CURRENT_USER_INFO,
  SET_AUTHENTICATED_STATE,
  SET_HAS_ACCOUNT_STATE,
  SET_AUTH_AND_HAS_ACCOUNT_STATE,
  RESET_REDUX,
  REGISTRATION
} from "./actionTypes";

export const initialState = {
  authenticated: false,
  pageHasError: false,
  userInfo: {
    username: null
  },
  hasAccount: false
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESET_REDUX:
      return { ...initialState };

    case SET_AUTHENTICATED_STATE:
      return {
        ...state,
        authenticated: action.payload
      };

    case SET_CURRENT_USER_INFO:
      return {
        ...state,
        userInfo: action.payload
      };

    case SET_HAS_ACCOUNT_STATE:
      return {
        ...state,
        hasAccount: action.payload
      };
    case SET_AUTH_AND_HAS_ACCOUNT_STATE:
      return {
        ...state,
        authenticated: action.authenticated,
        hasAccount: action.hasAccount
      };
    case REGISTRATION:
      return {
        ...state,
        pageHasError: action.pageHasError
      };

    default:
      return state;
  }
};

export default loginReducer;
