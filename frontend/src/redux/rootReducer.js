/**
 * This file combines all the reducers into 1 reducer
 */
import { combineReducers } from "redux";

// Individual reducers
import accommodations from "./accommodationsReducer";
import localize from "./localizationReducer";
import loginReducer from "./loginReducer";
import datePicker from "./DatePickerRedux";

export default combineReducers({
  localize,
  loginReducer,
  accommodations,
  datePicker
});
