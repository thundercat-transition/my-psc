import { initialState } from "./localizationActions";
import { RESET_REDUX, SET_LANGUAGE } from "./actionTypes";

const localize = (state = initialState, action) => {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        language: action.language
      };

    case RESET_REDUX:
      return { ...initialState };

    default:
      return state;
  }
};

export default localize;
