import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
const SET_FONT_FAMILY = "accommodations/SET_FONT_FAMILY";
const SET_FONT_SIZE = "accommodations/SET_FONT_SIZE";
const SET_ACCOMMODATIONS = "accommodations/SET_ACCOMMODATIONS";
const ACCOMMODATIONS_SAVED_LOADED = "accommodations/ACCOMMODATIONS_SAVED_LOADED";
const RESET_STATE = "accommodations/RESET_STATE";
const SET_SPACING = "accommodations/SET_SPACING";
const TRIGGER_RERENDER = "accommodations/TRIGGER_RERENDER";
const TRIGGER_CLOSING_DIALOG = "accommodations/TRIGGER_CLOSING_DIALOG";
const TRIGGER_FOCUS_TO_MAIN_NAV_LINK = "accommodations/TRIGGER_FOCUS_TO_MAIN_NAV_LINK";

function getUserAccommodations() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-user-accommodations`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function saveUserAccommodations(accomms) {
  const newObj = {
    ...accomms
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/save-user-accommodations`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

const getLineSpacingCSS = () => ({
  paddingTop: "0.5em",
  lineHeight: "1.5em",
  paddingBottom: "1.5em",
  letterSpacing: "0.12em",
  wordSpacing: "0.16em"
});

// Action Creators
const setFontFamily = fontFamily => ({
  type: SET_FONT_FAMILY,
  fontFamily
});

const setFontSize = fontSize => ({
  type: SET_FONT_SIZE,
  fontSize
});

const setAccommodations = accomms => ({
  type: SET_ACCOMMODATIONS,
  accomms
});
const setAccommodationsSavedAndLoaded = () => ({
  type: ACCOMMODATIONS_SAVED_LOADED
});
const resetAccommodations = () => ({
  type: RESET_STATE
});
const setSpacing = spacing => ({
  type: SET_SPACING,
  spacing
});
const triggerRerender = () => ({
  type: TRIGGER_RERENDER
});
const triggerClosingDialog = () => ({
  type: TRIGGER_CLOSING_DIALOG
});
const triggerFocusToMainNavLink = () => ({
  type: TRIGGER_FOCUS_TO_MAIN_NAV_LINK
});

// Initial State
export const initialState = {
  fontFamily: "Nunito Sans",
  fontSize: "16px",
  loadedSaved: false,
  spacing: false,
  triggerRerender: false,
  triggerClosingDialog: false,
  triggerFocusToMainNavLink: false
};

// Reducer
const accommodations = (state = initialState, action) => {
  switch (action.type) {
    case SET_FONT_FAMILY:
      return {
        ...state,
        fontFamily: action.fontFamily
      };
    case SET_FONT_SIZE:
      return {
        ...state,
        fontSize: action.fontSize
      };
    case SET_SPACING:
      return {
        ...state,
        spacing: action.spacing
      };
    case SET_ACCOMMODATIONS:
      const { accomms } = action;
      return {
        ...state,
        fontFamily: accomms.font_family,
        fontSize: accomms.font_size,
        spacing: accomms.spacing
      };
    case ACCOMMODATIONS_SAVED_LOADED:
      return {
        ...state,
        loadedSaved: true
      };
    case TRIGGER_RERENDER:
      return {
        ...state,
        triggerRerender: !state.triggerRerender
      };
    case TRIGGER_CLOSING_DIALOG:
      return {
        ...state,
        triggerClosingDialog: !state.triggerClosingDialog
      };
    case TRIGGER_FOCUS_TO_MAIN_NAV_LINK:
      return {
        ...state,
        triggerFocusToMainNavLink: !state.triggerFocusToMainNavLink
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default accommodations;
export {
  setFontFamily,
  setFontSize,
  getUserAccommodations,
  setAccommodations,
  saveUserAccommodations,
  setAccommodationsSavedAndLoaded,
  resetAccommodations,
  setSpacing,
  getLineSpacingCSS,
  triggerRerender,
  triggerFocusToMainNavLink,
  triggerClosingDialog
};
