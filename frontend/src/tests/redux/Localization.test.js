import { setLanguage, initialState } from "../../redux/localizationActions";
import localize from "../../redux/localizationReducer";

describe("localizationReducer", () => {
  it("Should have the language set to English by default;", () => {
    expect(initialState).toEqual({ language: "en" });
  });

  it("Should update language;", () => {
    const setFrenchLocality = setLanguage("fr");
    expect(localize(initialState, setFrenchLocality)).toEqual({ language: "fr" });

    const setEnglishLocality = setLanguage("en");
    expect(localize(initialState, setEnglishLocality)).toEqual({ language: "en" });
  });
});
