import React from "react";

// Testing-related imports
import { MemoryRouter } from "react-router-dom";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import store from "../store-index";

// Constants
import LOCALIZE from "../resources/textResources";
import { PATH } from "../resources/constants";

// Tested components imports
import { RoutesList } from "../RoutesList";
import HomePage from "../pages/HomePage";
import Error404Page from "../pages/Error404Page";

// ===============
// Actual Routing Tests
// ===============

describe("Routing should navigate to the correct pages when visiting particular URLs", () => {
  it("routes to the Home page correctly", () => {
    const url = PATH.home;
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={[url]}>
          <RoutesList />
        </MemoryRouter>
      </Provider>
    );
    // Test to see if the correct title string is rendered
    expect(wrapper.find("h1").filterWhere(n => n.text() === LOCALIZE.homePage.title)).toHaveLength(
      1
    );

    // Test to see if the current path name is correct
    expect(wrapper.find("Router").prop("history").location.pathname).toEqual(url);
  });

  it("routes to the Login page correctly", () => {
    const url = PATH.login;
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={[url]}>
          <RoutesList />
        </MemoryRouter>
      </Provider>
    );

    // Test to see if the correct title string is rendered
    expect(wrapper.find("h1").filterWhere(n => n.text() === LOCALIZE.loginPage.title)).toHaveLength(
      1
    );

    // Test to see if the current path name is correct
    expect(wrapper.find("Router").prop("history").location.pathname).toEqual(url);
  });

  it("routes to the Admin page correctly if logged in", () => {
    const url = PATH.admin;
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={[url]}>
          <RoutesList authenticated />
        </MemoryRouter>
      </Provider>
    );

    // Test to see if the correct title string is rendered
    expect(wrapper.find("h1").filterWhere(n => n.text() === LOCALIZE.adminPage.title)).toHaveLength(
      1
    );

    // Test to see if the current path name is correct
    expect(wrapper.find("Router").prop("history").location.pathname).toEqual(url);
  });
});

describe("Route redirection with PrivateRoutes should work correctly", () => {
  it("routes to the Home page correctly if you visit the Login Page while logged in", () => {
    const url = PATH.login;
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={[url]}>
          <RoutesList authenticated />
        </MemoryRouter>
      </Provider>
    );

    // Check if page actually rendered
    expect(wrapper.exists()).toBe(true);

    // Test to see if the current path name is correct
    expect(wrapper.find("Router").prop("history").location.pathname).toEqual(PATH.home);
  });

  it("routes to the Login page correctly if you visit the Admin Page while logged out", () => {
    const url = PATH.admin;
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={[url]}>
          <RoutesList />
        </MemoryRouter>
      </Provider>
    );

    // Check if page actually rendered
    expect(wrapper.exists()).toBe(true);

    // Test to see if the current path name is correct
    expect(wrapper.find("Router").prop("history").location.pathname).toEqual(PATH.login);
  });

  it("routes to the Error404 page correctly if trying to access a non-existing page", () => {
    const url = "/non-existing-page-keyboardmash-uioqwfiowhiuabgviaubh";
    const wrapper = mount(
      <Provider store={store}>
        <MemoryRouter initialEntries={[url]}>
          <RoutesList />
        </MemoryRouter>
      </Provider>
    );

    // Check if page actually rendered
    expect(wrapper.exists()).toBe(true);

    expect(wrapper.find(HomePage)).toHaveLength(0);
    expect(wrapper.find(Error404Page)).toHaveLength(1);
  });
});
