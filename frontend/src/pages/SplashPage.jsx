import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import { Helmet } from "react-helmet";

// Redux
import { resetRedux } from "../redux/otherActions";
import { setLanguage } from "../redux/localizationActions";

// Constants
import LOCALIZE from "../resources/textResources";
import { PATH } from "../resources/constants";

// Components & Visual assets
import "../css/splash-page.css";
import SplashPageImages from "../components/SplashPageImages";
import GovernmentBanner from "../images/sig-spl.svg";
import GovernmentFooter from "../images/wmms-spl.svg";

const getRandomSplashImage = imagesList => {
  const min = 0;
  const max = imagesList.length - 1;
  const randIndex = min + Math.round(Math.random() * (max - min));

  return imagesList[parseInt(randIndex, 10)];
};

// Convention: fixed styles for a particular page only are made as a local JS object
// Shared CSS or CSS that can change depending on screen size (via media queries) are placed in app-theme.css
const styles = {
  bannerSpacer: {
    height: "1.5em"
  },
  bannerBox: {
    margin: "1.5em 0.5em 0em 0em"
  },
  splashBox: {
    backgroundColor: "#FFFFFF",
    padding: 0,
    border: "0px solid #00565e",
    borderRadius: 0
  },
  splashBoxBody: {
    padding: 20
  },
  splashBoxFooter: {
    backgroundColor: "#e1e4e7",
    margin: "0px 0px",
    padding: "30px",
    border: "0px solid #00565e",
    borderRadius: 0,
    justifyContent: "center"
  },
  splash: {
    width: "calc(100vw)",
    height: "calc(100vh)",
    backgroundImage: `url(${getRandomSplashImage(SplashPageImages)})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  }
};

export class SplashPage extends React.PureComponent {
  /**
   * Sets the appropriate language & resets the Redux state
   */
  chooseLanguage = lang => {
    const { setLanguage, resetRedux } = this.props;
    resetRedux();
    setLanguage(lang);
  };

  render() {
    const { currentLanguage } = this.props;

    return (
      <div className="splashPage">
        <Helmet>
          <title>{LOCALIZE.splashPage.title}</title>
          <html lang={currentLanguage} />
        </Helmet>
        <main style={styles.splash}>
          <div className="splash-spacer" />
          <Container className="splash-box" style={styles.splashBox}>
            <div className="card-body" style={styles.splashBoxBody}>
              <h1 property="name" className="wb-inv">
                Canada.ca
              </h1>
              <Row>
                <Col xs={{ span: 12 }} sm={{ span: 11 }} style={styles.bannerBox}>
                  <object
                    className=""
                    type="image/svg+xml"
                    tabIndex="-1"
                    role="img"
                    data={GovernmentBanner}
                    aria-label="Government of Canada / Gouvernement du Canada"
                  />
                </Col>
              </Row>

              <Row style={styles.bannerSpacer} />

              <div className="is-center splash-button-container">
                <Row>
                  {/* Using the native col classes here since it breaks IE for some reason */}
                  <div className="col-xs-6">
                    <Link
                      to={PATH.home}
                      className="btn btn-primary splash-button"
                      id="englishLanguageButton"
                      onClick={() => this.chooseLanguage("en")}
                    >
                      {LOCALIZE.splashPage.langSelectEnglish}
                    </Link>
                  </div>
                  <div className="col-xs-6">
                    <Link
                      to={PATH.home}
                      className="btn btn-primary splash-button"
                      id="frenchLanguageButton"
                      onClick={() => this.chooseLanguage("fr")}
                    >
                      {LOCALIZE.splashPage.langSelectFrench}
                    </Link>
                  </div>
                </Row>
              </div>
            </div>

            <div className="card-footer" style={styles.splashBoxFooter}>
              <Row className="align-items-center">
                <Col xs={5} sm={6} className="is-center">
                  <a className="splash-url" href={LOCALIZE.splashPage.termsLinkEnglish} lang="en">
                    {LOCALIZE.splashPage.termsEnglish}
                  </a>
                  {LOCALIZE.formatString("\t")}
                  <span className="glyphicon glyphicon-asterisk" />
                  {LOCALIZE.formatString("\t")}
                  <a className="splash-url" href={LOCALIZE.splashPage.termsLinkFrench} lang="fr">
                    {LOCALIZE.splashPage.termsFrench}
                  </a>
                </Col>
                <Col xs={{ span: 5, offset: 2 }} sm={{ span: 4, offset: 2 }} className="is-center">
                  <object
                    className="splash-footer-img"
                    type="image/svg+xml"
                    tabIndex="-1"
                    role="img"
                    data={GovernmentFooter}
                    aria-label="Symbol of the Government of Canada / Symbole du gouvernement du Canada"
                  />
                </Col>
              </Row>
            </div>
          </Container>
        </main>
      </div>
    );
  }
}

SplashPage.propTypes = {
  currentLanguage: PropTypes.string,
  resetRedux: PropTypes.func,
  setLanguage: PropTypes.func
};

SplashPage.defaultProps = {
  currentLanguage: "en",
  resetRedux: () => {},
  setLanguage: () => {}
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetRedux,
      setLanguage
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SplashPage);
