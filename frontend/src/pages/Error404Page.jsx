import React from "react";

// Constants
import LOCALIZE from "../resources/textResources";

// Components
import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";

export const Error404Page = () => {
  return (
    <PageTemplate title={LOCALIZE.error404page.title}>
      <FocusHeader>{LOCALIZE.error404page.title}</FocusHeader>
      <h3>{LOCALIZE.error404page.subtitle}</h3>
      <h4>{LOCALIZE.error404page.subtitle2}</h4>
      <p className="spacer">{LOCALIZE.error404page.content1}</p>
      <p>{LOCALIZE.error404page.content2}</p>
    </PageTemplate>
  );
};

export default Error404Page;
