import React from "react";
import { PropTypes } from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import getCheckboxTransformScale from "../helpers/checkboxTransformScale";
import validateName, {
  validateEmail,
  validatePRI,
  valdidatePrNbr,
  validateMilitaryNbr
} from "../helpers/regexValidator";

// Constants
import { PATH } from "../resources/constants";
import LOCALIZE from "../resources/textResources";
import { setAuthAndHasAccount, updatePageHasErrorState } from "../redux/loginActions";

// Components
import GenericButton from "../components/commons/GenericButton";
import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";
import ContentContainer from "../components/commons/ContentContainer";
import DatePicker from "../components/commons/DatePicker";
import CustomButton, { THEME } from "../components/commons/CustomButton";
import PrivacyNoticeStatement from "../components/commons/PrivacyNoticeStatement";

export const styles = {
  appPadding: {
    padding: "15px"
  },
  createAccountContent: {
    padding: "12px 32px 0 32px",
    border: "1px solid #cdcdcd"
  },
  inputContainer: {
    position: "relative"
  },
  inputContainerForPassword: {
    position: "relative",
    display: "table-cell",
    width: "100%"
  },
  inputTitle: {
    padding: "12px 0 6px 0",
    fontWeight: "bold"
  },
  passwordLabel: {
    padding: "5px 0 0 0",
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  inputForNames: {
    width: 240,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  inputForPasswords: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: "4px 0 0 4px"
  },
  validInputPadding: {
    padding: "3px 75px 3px 6px"
  },
  dobContainer: {
    display: "inline-block"
  },
  monthAndDayField: {
    width: 77,
    marginRight: 36,
    float: "left"
  },
  yearField: {
    width: 97,
    marginRight: 36,
    float: "left"
  },
  dobLabel: {
    padding: "3px 0 0 4px",
    margin: 0
  },
  checkMark: {
    marginRight: 6
  },
  validIconForInputField: {
    color: "#278400",
    position: "absolute",
    right: 0,
    top: "12%"
  },
  validIconMarginForFirstName: {
    marginRight: 20
  },
  validIconMarginForOtherField: {
    marginRight: 9
  },
  loginBtn: {
    display: "block",
    margin: "24px auto"
  },
  passwordRequirementsError: {
    color: "#923534",
    marginTop: 6,
    fontWeight: "bold"
  },
  passwordRequirementsForScreenReader: {
    display: "none"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  privacyNoticeZone: {
    marginTop: 24
  },
  checkboxContainer: {
    textAlign: "center"
  },
  checkbox: {
    verticalAlign: "middle"
  },
  privacyNoticeLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  tooltipIcon: {
    color: "#00565e"
  },
  tooltipButton: {
    padding: "0 6px"
  },
  passwordVisilibityContainer: {
    display: "table-cell"
  },
  passwordVisilibity: {
    color: "#00565e",
    border: "1px solid #00565e",
    borderRadius: "0 4px 4px 0",
    background: "white",
    borderLeft: "none",
    borderWidth: 1,
    borderColor: "#00565e",
    minWidth: "inherite",
    padding: "3px 6px"
  },
  passwordVisilibityInvalid: {
    borderColor: "#923534",
    borderWidth: 3
  }
};

export class RegistrationForm extends React.Component {
  constructor(props) {
    super(props);
    this.dateOfBirthDayFieldRef = React.createRef();
  }

  state = {
    // Ensures no errors are shown on page load
    isFirstLoad: true, // TODO

    // Field Content States
    firstNameContent: "", // TODO
    lastNameContent: "", // TODO
    emailContent: "", // TODO
    priContent: "", // TODO
    prNbrContent: "", // TODO
    snContent: "", // TODO

    // Field Validation States
    isValidFirstName: false, // TODO
    isValidLastName: false, // TODO
    triggerDateOfBirthValidation: false, // TODO
    isValidEmail: false, // TODO
    isValidPri: false, // TODO
    isValidPrNbr: false, // TODO
    isValidSN: false, // TODO
    isCheckboxChecked: false, // TODO
    isValidPrivacyNotice: false, // TODO

    // PopupBox States
    showPrivacyNoticeDialog: false, // TODO

    // API Errors Handler States
    accountExistsError: false // TODO
  };

  getFirstNameContent = event => {
    const firstNameContent = event.target.value;
    // allow maximum of 30 chars
    const regexExpression = /^(.{0,30})$/;
    if (regexExpression.test(firstNameContent)) {
      this.setState({
        firstNameContent: firstNameContent
      });
    }
  };

  getLastNameContent = event => {
    const lastNameContent = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(lastNameContent)) {
      this.setState({
        lastNameContent: lastNameContent
      });
    }
  };

  getEmailContent = event => {
    const emailContent = event.target.value;
    this.setState({ emailContent: emailContent });
  };

  getPriContent = event => {
    const priContent = event.target.value;
    this.setState({ priContent: priContent });
  };

  getPrnContent = event => {
    const prNbrContent = event.target.value;
    this.setState({ prNbrContent: prNbrContent });
  };

  getSnContent = event => {
    const snContent = event.target.value;
    this.setState({ snContent: snContent });
  };

  showPrivacyNoticePopup = event => {
    this.setState({ showPrivacyNoticeDialog: true });
    event.preventDefault();
  };

  closePrivacyNoticePopup = () => {
    this.setState({ showPrivacyNoticeDialog: false });
  };

  changeCheckboxStatus = () => {
    this.setState({ isCheckboxChecked: !this.state.isCheckboxChecked });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.hasAccount !== this.props.hasAccount) {
      this.checkProfileAfterLogin(); // check where to redirect
    }
  };

  checkProfileAfterLogin = () => {
    // TODO once externally authorized; use this function to determine if they have a profile or not

    // only check if hasAccount was updated; cannot even be on this page if state.authorized is false
    if (this.props.hasAccount) {
      // if there already is an account; then go to the dashboard
      window.location.href = PATH.dashboard;
    }
  };

  // checks if all fields are valid
  isFormValid = () => {
    return (
      this.state.isValidFirstName &&
      this.state.isValidLastName &&
      this.props.completeDateValidState &&
      this.state.isValidEmail &&
      this.state.isValidPri &&
      this.state.isValidPrNbr &&
      this.state.isValidSN &&
      this.state.isCheckboxChecked &&
      this.state.isValidPrivacyNotice
    );
  };

  handleSubmit = event => {
    const validForm = this.isFormValid();
    // if all fields are valid, execute API errors validation
    if (validForm) {
      // TODO execute api error validation
      // handle account/email already exists errors
      // TODO create account
      this.props.setAuthAndHasAccount(true, true);
    } else {
      this.props.updatePageHasErrorState(true);
      this.focusOnHighestErrorField();
    }
    event.preventDefault();
  };

  validateForm = () => {
    const isValidFirstName = validateName(this.state.firstNameContent);
    const isValidLastName = validateName(this.state.lastNameContent);
    const isValidEmail = validateEmail(this.state.emailContent);
    const isValidPri = validatePRI(this.state.priContent);
    const isValidPrNbr = valdidatePrNbr(this.state.prNbrContent);
    const isValidSN = validateMilitaryNbr(this.state.snContent);
    const isValidPrivacyNotice = this.state.isCheckboxChecked;
    // TODO the DoB is NOT checked
    // TODO the email is not checked against the DB for a conflict
    this.setState({
      triggerDateOfBirthValidation: !this.state.triggerDateOfBirthValidation,
      isFirstLoad: false,
      accountExistsError: false,
      isValidFirstName: isValidFirstName,
      isValidLastName: isValidLastName,
      isValidEmail: isValidEmail,
      isValidPri: isValidPri,
      isValidPrNbr: isValidPrNbr,
      isValidSN: isValidSN,
      isValidPrivacyNotice: isValidPrivacyNotice
    });
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidFirstName) {
      document.getElementById("first-name-field").focus();
    } else if (!this.state.isValidLastName) {
      document.getElementById("last-name-field").focus();
    } else if (!this.props.completeDateValidState) {
      this.dateOfBirthDayFieldRef.current.focus();
    } else if (!this.state.isValidEmail) {
      document.getElementById("email-address-field").focus();
    } else if (!this.state.isValidPri) {
      document.getElementById("pri-field").focus();
    } else if (!this.state.isValidPrNbr) {
      document.getElementById("pr-nbr-field").focus();
    } else if (!this.state.isValidSN) {
      document.getElementById("military-nbr-field").focus();
    } else if (!this.state.isValidPrivacyNotice) {
      document.getElementById("privacy-notice-checkbox").focus();
    }
  };

  render() {
    const {
      isFirstLoad,
      firstNameContent,
      lastNameContent,
      triggerDateOfBirthValidation,
      emailContent,
      priContent,
      prNbrContent,
      snContent,
      isValidFirstName,
      isValidLastName,
      isValidEmail,
      accountExistsError,
      isValidPri,
      isValidPrNbr,
      isValidSN,
      isCheckboxChecked,
      isValidPrivacyNotice
    } = this.state;

    const validFieldClass = "valid-field";
    const invalidFieldClass = "invalid-field";

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    const accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    return (
      <div className="app" style={styles.appPadding}>
        <ContentContainer>
          <PageTemplate title={LOCALIZE.registrationForm.title}>
            <h1 className="green-divider">{LOCALIZE.registrationForm.content.title}</h1>
            <span>{LOCALIZE.registrationForm.content.description}</span>
            <form onSubmit={this.handleSubmit}>
              <div>
                <div style={styles.inputTitle}>
                  <label id="first-name-title">
                    {LOCALIZE.registrationForm.content.inputs.firstNameTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidFirstName && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForFirstName
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.registrationForm.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      isValidFirstName || isFirstLoad ? validFieldClass : invalidFieldClass
                    }
                    aria-labelledby={"first-name-title first-name-error"}
                    aria-invalid={!this.state.isValidFirstName && !isFirstLoad}
                    aria-required={"true"}
                    id="first-name-field"
                    type="text"
                    value={firstNameContent}
                    style={
                      isValidFirstName
                        ? {
                            ...styles.inputs,
                            ...styles.validInputPadding,
                            ...accommodationsStyle
                          }
                        : { ...styles.inputs, ...accommodationsStyle }
                    }
                    onChange={this.getFirstNameContent}
                  />
                </div>
                {!isValidFirstName && !isFirstLoad && (
                  <label id="first-name-error" style={styles.errorMessage} className="notranslate">
                    {LOCALIZE.registrationForm.content.inputs.firstNameError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label id="last-name-title">
                    {LOCALIZE.registrationForm.content.inputs.lastNameTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidLastName && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.registrationForm.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={isValidLastName || isFirstLoad ? validFieldClass : invalidFieldClass}
                    aria-labelledby={"last-name-title last-name-error"}
                    aria-invalid={!this.state.isValidLastName && !isFirstLoad}
                    aria-required={"true"}
                    id="last-name-field"
                    type="text"
                    value={lastNameContent}
                    style={
                      isValidLastName
                        ? {
                            ...styles.inputs,
                            ...styles.validInputPadding,
                            ...accommodationsStyle
                          }
                        : { ...styles.inputs, ...accommodationsStyle }
                    }
                    onChange={this.getLastNameContent}
                  />
                </div>
                {!isValidLastName && !isFirstLoad && (
                  <label id="last-name-error" style={styles.errorMessage} className="notranslate">
                    {LOCALIZE.registrationForm.content.inputs.lastNameError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label id="dob-title">{LOCALIZE.registrationForm.content.inputs.dobTitle}</label>
                </div>
                <DatePicker
                  dateDayFieldRef={this.dateOfBirthDayFieldRef}
                  dateLabelId={"dob-title"}
                  triggerValidation={triggerDateOfBirthValidation}
                />
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label id="email-address-title">
                    {LOCALIZE.registrationForm.content.inputs.emailTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidEmail && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.registrationForm.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={
                      (isValidEmail && !accountExistsError) || isFirstLoad
                        ? validFieldClass
                        : invalidFieldClass
                    }
                    aria-labelledby={
                      "email-address-title email-address-error email-address-account-exists-error"
                    }
                    aria-invalid={!this.state.isValidEmail && !isFirstLoad}
                    aria-required={"true"}
                    id="email-address-field"
                    type="text"
                    value={emailContent}
                    style={
                      isValidEmail
                        ? { ...styles.inputs, ...styles.validInputPadding, ...accommodationsStyle }
                        : { ...styles.inputs, ...accommodationsStyle }
                    }
                    onChange={this.getEmailContent}
                  />
                </div>
                {!isValidEmail && !isFirstLoad && !accountExistsError && (
                  <label
                    id="email-address-error"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.registrationForm.content.inputs.emailError}
                  </label>
                )}
              </div>
              {accountExistsError && (
                <label
                  id="email-address-account-exists-error"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.registrationForm.accountAlreadyExistsError}
                </label>
              )}
              <h3>{LOCALIZE.registrationForm.content.inputs.optionalTitle}</h3>
              <div>
                <div style={styles.inputTitle}>
                  <label htmlFor={"pri-field"}>
                    {LOCALIZE.registrationForm.content.inputs.priTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidPri && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.registrationForm.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={isValidPri || isFirstLoad ? validFieldClass : invalidFieldClass}
                    aria-invalid={!this.state.isValidPri && !isFirstLoad}
                    aria-required={"false"}
                    id="pri-field"
                    type="text"
                    value={priContent}
                    style={{ ...styles.inputs, ...accommodationsStyle }}
                    onChange={this.getPriContent}
                  />
                </div>
                {!isValidPri && !isFirstLoad && (
                  <label htmlFor={"pri-field"} style={styles.errorMessage} className="notranslate">
                    {LOCALIZE.registrationForm.content.inputs.priError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label htmlFor={"pr-nbr-field"}>
                    {LOCALIZE.registrationForm.content.inputs.prNbrTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidPrNbr && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.registrationForm.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={isValidPrNbr || isFirstLoad ? validFieldClass : invalidFieldClass}
                    aria-invalid={!this.state.isValidPrNbr && !isFirstLoad}
                    aria-required={"false"}
                    id="pr-nbr-field"
                    type="text"
                    value={prNbrContent}
                    style={{ ...styles.inputs, ...accommodationsStyle }}
                    onChange={this.getPrnContent}
                  />
                </div>
                {!isValidPri && !isFirstLoad && (
                  <label
                    htmlFor={"pr-nbr-field"}
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.registrationForm.content.inputs.prNbrError}
                  </label>
                )}
              </div>
              <div>
                <div style={styles.inputTitle}>
                  <label htmlFor={"military-nbr-field"}>
                    {LOCALIZE.registrationForm.content.inputs.serviceTitle}
                  </label>
                </div>
                <div style={styles.inputContainer}>
                  {isValidSN && (
                    <div
                      style={{
                        ...styles.validIconForInputField,
                        ...styles.validIconMarginForOtherField
                      }}
                    >
                      <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                      {LOCALIZE.registrationForm.content.inputs.valid}
                    </div>
                  )}
                  <input
                    className={isValidSN || isFirstLoad ? validFieldClass : invalidFieldClass}
                    aria-invalid={!this.state.isValidSN && !isFirstLoad}
                    aria-required={"false"}
                    id="military-nbr-field"
                    type="text"
                    value={snContent}
                    style={{ ...styles.inputs, ...accommodationsStyle }}
                    onChange={this.getSnContent}
                  />
                </div>
                {!isValidSN && !isFirstLoad && (
                  <label
                    htmlFor={"military-nbr-field"}
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.registrationForm.content.inputs.serviceError}
                  </label>
                )}
              </div>
              <div className="privacy-notice-grid" style={styles.privacyNoticeZone}>
                <div className="privacy-notice-grid-checkbox" style={styles.checkboxContainer}>
                  <input
                    aria-invalid={!isValidPrivacyNotice && !isFirstLoad}
                    aria-labelledby={"privacy-notice-error privacy-notice-description"}
                    id="privacy-notice-checkbox"
                    type="checkbox"
                    style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                    onChange={this.changeCheckboxStatus}
                  />
                </div>
                <div className="privacy-notice-grid-description">
                  <label id="privacy-notice-description" htmlFor="privacy-notice-checkbox">
                    {LOCALIZE.formatString(
                      LOCALIZE.registrationForm.privacyNotice,
                      <button
                        aria-label={LOCALIZE.registrationForm.privacyNoticeLink}
                        tabIndex="0"
                        onClick={this.showPrivacyNoticePopup}
                        style={styles.privacyNoticeLink}
                      >
                        {LOCALIZE.registrationForm.privacyNoticeLink}
                      </button>
                    )}
                  </label>
                </div>
              </div>
              {!isValidPrivacyNotice && !isFirstLoad && (
                <label
                  id="privacy-notice-error"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.registrationForm.privacyNoticeError}
                </label>
              )}
              <CustomButton
                label={LOCALIZE.registrationForm.button}
                action={this.validateForm}
                customStyle={styles.loginBtn}
                type={"submit"}
                buttonTheme={THEME.PRIMARY}
              />
            </form>
            <PrivacyNoticeStatement
              showPopup={this.state.showPrivacyNoticeDialog}
              handleClose={this.closePrivacyNoticePopup}
            />
          </PageTemplate>
        </ContentContainer>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.loginReducer.authenticated,
    hasAccount: state.loginReducer.hasAccount,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setAuthAndHasAccount, updatePageHasErrorState }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm);
