import React from "react";

// Constants
import { PATH } from "../resources/constants";
import LOCALIZE from "../resources/textResources";

// Components
import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";
import GenericButton from "../components/commons/GenericButton";
import LoginForm from "../components/commons/LoginForm";

export const LoginPage = () => {
  return (
    <PageTemplate title={LOCALIZE.loginPage.title}>
      <div className="page-header-no-underline">
        <FocusHeader>{LOCALIZE.loginPage.title}</FocusHeader>
      </div>
      <div className="page-content">
        <div className="content">
          <LoginForm />
        </div>
        <div className="button-bar">
          <GenericButton
            linkPath={PATH.home}
            text={LOCALIZE.navigation.previousButton}
            ariaLabel={LOCALIZE.navigation.previousButtonLabel}
          />
        </div>
      </div>
    </PageTemplate>
  );
};

export default LoginPage;
