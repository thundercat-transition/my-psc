import React from "react";
import { shallow } from "enzyme";

// Components
import Error404Page from "./Error404Page";

describe("Error 404 Page component", () => {
  it("should render", () => {
    const wrapper = shallow(<Error404Page />);
    expect(wrapper.exists()).toBe(true);
  });
});
