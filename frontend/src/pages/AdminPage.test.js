import React from "react";
import { shallow } from "enzyme";

// Components
import AdminPage from "./AdminPage";

describe("AdminPage component", () => {
  it("should render", () => {
    const wrapper = shallow(<AdminPage />);
    expect(wrapper.exists()).toBe(true);
  });
});
