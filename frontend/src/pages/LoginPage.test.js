import React from "react";
import { shallow } from "enzyme";

// Components
import LoginPage from "./LoginPage";

describe("LoginPage component", () => {
  it("should render", () => {
    const wrapper = shallow(<LoginPage />);
    expect(wrapper.exists()).toBe(true);
  });
});
