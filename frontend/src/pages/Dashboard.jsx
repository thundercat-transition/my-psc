import React from "react";
import { PropTypes } from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import { Row, Col } from "react-bootstrap";
import PSCAppBox from "../components/PSCAppBox";

// Constants
import { PATH } from "../resources/constants";
import LOCALIZE from "../resources/textResources";
import { setAuthAndHasAccount, setAuthenticationState, setHasAccount } from "../redux/loginActions";

// Components
import GenericButton from "../components/commons/GenericButton";
import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";
import ContentContainer from "../components/commons/ContentContainer";
import CustomButton, { THEME } from "../components/commons/CustomButton";
import MyTestsTable from "../components/MyTestsTable";

export const styles = {
  appPadding: {
    padding: "15px"
  },
  paragraph: {
    paddingBottom: "15px"
  }
};

export class Dashboard extends React.Component {
  state = {
    currentlyLoading: true,
    rowsDefinition: {},
    selectedTestDetails: {},
    triggerPopup: false,
    firstName: "Me", // TODO get from Props
    lastName: "Too" // TODO get from Props
  };

  componentDidMount = () => {
    this.populateRowsDefinition();
  };

  handleViewResults = data => {
    this.setState({ selectedTestDetails: data }, () => {
      this.setState({ triggerPopup: !this.state.triggerPopup });
    });
  };

  populateRowsDefinition = () => {
    // TODO get actual data from the DB
    const data = [];
    data.push({
      column_1: "E369A1 - SLE UIT Reading English",
      column_2: "2021-01-12",
      column_3: "C", // LOCALIZE.commons.status.invalid
      column_4: (
        <CustomButton
          buttonId={`results-button-row-1`}
          label={
            <>
              <FontAwesomeIcon icon={faBinoculars} />
              <span style={styles.buttonLabel}>
                {LOCALIZE.dashboard.myTests.table.viewResultsButton}
              </span>
            </>
          }
          action={() => {
            this.handleViewResults({ todo: "add me" });
          }}
          customStyle={styles.viewEditDetailsBtn}
          buttonTheme={THEME.PRIMARY}
          ariaLabel={LOCALIZE.dashboard.myTests.table.viewResultsButtonAriaLabel}
        />
      )
    });

    data.push({
      column_1: "E369A1 - SLE UIT Reading English",
      column_2: "2020-01-12",
      column_3: LOCALIZE.commons.status.invalid,
      column_4: (
        <CustomButton
          buttonId={`results-button-row-2`}
          label={
            <>
              <FontAwesomeIcon icon={faBinoculars} />
              <span style={styles.buttonLabel}>
                {LOCALIZE.dashboard.myTests.table.viewResultsButton}
              </span>
            </>
          }
          action={() => {
            this.handleViewResults({ todo: "add me too" });
          }}
          customStyle={styles.viewEditDetailsBtn}
          buttonTheme={THEME.PRIMARY}
          ariaLabel={LOCALIZE.dashboard.myTests.table.viewResultsButtonAriaLabel}
        />
      )
    });

    const rowsDefinition = {
      column_1_style: styles.firstRowStyle,
      column_2_style: styles.basicRowStyle,
      column_3_style: styles.basicRowStyle,
      column_4_style: styles.basicRowStyle,
      data: data
    };

    // saving results in state
    this.setState({
      rowsDefinition: rowsDefinition
    });
    // this goes into the then
    this.setState({ currentlyLoading: false });
  };

  render() {
    // TODO add alerts? Needed?
    // TODO get these from the props
    const { firstName, lastName } = this.state;
    return (
      <div className="app" style={styles.appPadding}>
        <ContentContainer>
          <PageTemplate title={LOCALIZE.homePage.title}>
            <div id="main-content" role="main">
              <div
                id="user-welcome-message-div"
                aria-labelledby="user-welcome-message"
                className="notranslate"
              >
                <h1 id="user-welcome-message">
                  {LOCALIZE.formatString(LOCALIZE.dashboard.title, firstName, lastName)}
                </h1>
              </div>
              <div>
                <h2 className="green-divider">{LOCALIZE.dashboard.myTools.title}</h2>
                <p style={styles.paragraph}>{LOCALIZE.dashboard.myTools.description}</p>
                <Row>
                  <Col>
                    <PSCAppBox
                      image={"CAT"}
                      appName={LOCALIZE.dashboard.myTools.cat.title}
                      buttonAction={() => {
                        console.log("Triggered CAT");
                      }}
                      detailText={LOCALIZE.dashboard.myTools.cat.description}
                    />
                  </Col>
                  <Col>
                    <PSCAppBox
                      image={"PSRS"}
                      appName={LOCALIZE.dashboard.myTools.psrs.title}
                      buttonAction={() => {
                        console.log("Triggered PSRS");
                      }}
                      detailText={LOCALIZE.dashboard.myTools.psrs.description}
                    />
                  </Col>
                  <Col>
                    <PSCAppBox
                      image={"PIMS"}
                      appName={LOCALIZE.dashboard.myTools.pims.title}
                      buttonAction={() => {
                        console.log("Triggered PIMS");
                      }}
                      detailText={LOCALIZE.dashboard.myTools.pims.description}
                    />
                  </Col>
                </Row>
              </div>

              <div>
                <h2 className="green-divider">{LOCALIZE.dashboard.myTests.title}</h2>
                <p style={styles.paragraph}>{LOCALIZE.dashboard.myTests.description}</p>
                <MyTestsTable
                  currentlyLoading={this.state.currentlyLoading}
                  rowsDefinition={this.state.rowsDefinition}
                  selectedTestDetails={this.state.selectedTestDetails}
                  triggerPopup={this.state.triggerPopup}
                />
              </div>
            </div>
          </PageTemplate>
        </ContentContainer>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.loginReducer.authenticated,
    hasAccount: state.loginReducer.hasAccount
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setAuthAndHasAccount, setAuthenticationState, setHasAccount }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
