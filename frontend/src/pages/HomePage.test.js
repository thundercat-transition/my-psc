import React from "react";
import { shallow } from "enzyme";

// Constants
import LOCALIZE from "../resources/textResources";

// Components
import { HomePage } from "./HomePage";

describe("Home page tests", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<HomePage />);
  });

  it("should render", () => {
    expect(wrapper.exists()).toBe(true);
  });

  it("should have the correct title mentioned on the page", () => {
    expect(wrapper.contains(LOCALIZE.homePage.title)).toEqual(true);
  });
});
