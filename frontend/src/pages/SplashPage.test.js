import React from "react";
import { shallow } from "enzyme";
import { Link } from "react-router-dom";

// Constants
import LOCALIZE from "../resources/textResources";

// Components
import { SplashPage } from "./SplashPage";

describe("splash page tests", () => {
  const mockSetLanguage = jest.fn();

  it("english terms link should be present", () => {
    expect(
      shallow(<SplashPage />).contains(
        <a className="splash-url" href={LOCALIZE.splashPage.termsLinkEnglish} lang="en">
          {LOCALIZE.splashPage.termsEnglish}
        </a>
      )
    ).toEqual(true);
  });

  it("french terms link should be present", () => {
    expect(
      shallow(<SplashPage />).contains(
        <a className="splash-url" href={LOCALIZE.splashPage.termsLinkFrench} lang="fr">
          {LOCALIZE.splashPage.termsFrench}
        </a>
      )
    ).toEqual(true);
  });

  it("english language button exists and calls setLanguage with 'en'", () => {
    shallow(<SplashPage setLanguage={mockSetLanguage} />)
      .find(Link)
      .at(0)
      .simulate("click");
    expect(mockSetLanguage).toHaveBeenCalledWith("en");
  });

  it("french language button exists and calls setLanguage with 'fr'", () => {
    shallow(<SplashPage setLanguage={mockSetLanguage} />)
      .find(Link)
      .at(1)
      .simulate("click");
    expect(mockSetLanguage).toHaveBeenCalledWith("fr");
  });
});
