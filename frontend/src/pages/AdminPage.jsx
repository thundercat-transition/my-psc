import React from "react";

import { PATH } from "../resources/constants";
import LOCALIZE from "../resources/textResources";

import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";
import GenericButton from "../components/commons/GenericButton";

export const AdminPage = () => {
  return (
    <PageTemplate title={LOCALIZE.adminPage.title}>
      <div className="page-header-no-underline">
        <FocusHeader>{LOCALIZE.adminPage.title}</FocusHeader>
      </div>
      <div className="page-content">
        <div className="content">
          <p>{LOCALIZE.adminPage.content1}</p>
        </div>
        <div className="button-bar">
          <GenericButton
            linkPath={PATH.home}
            text={LOCALIZE.navigation.previousButton}
            ariaLabel={LOCALIZE.navigation.previousButtonLabel}
          />
        </div>
      </div>
    </PageTemplate>
  );
};

export default AdminPage;
