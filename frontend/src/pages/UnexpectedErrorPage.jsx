import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Constants
import LOCALIZE from "../resources/textResources";

// Components
import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";

export const UnexpectedErrorPage = () => {
  return (
    <PageTemplate title={LOCALIZE.unexpectedErrorPage.title}>
      <FocusHeader>{LOCALIZE.unexpectedErrorPage.title}</FocusHeader>
      <h3>{LOCALIZE.unexpectedErrorPage.subtitle}</h3>
      <h4>{LOCALIZE.unexpectedErrorPage.subtitle2}</h4>
      <p className="spacer">{LOCALIZE.unexpectedErrorPage.content1}</p>
      <p>{LOCALIZE.unexpectedErrorPage.content2}</p>
    </PageTemplate>
  );
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UnexpectedErrorPage);
