import React from "react";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";

// Constants
import { PATH } from "../resources/constants";
import LOCALIZE from "../resources/textResources";
import { setAuthAndHasAccount, setAuthenticationState, setHasAccount } from "../redux/loginActions";

// Helpers
import { sendDataToSampleEndpoint } from "../helpers/apiUtil";

// Components
import GenericButton from "../components/commons/GenericButton";
import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";
import ContentContainer from "../components/commons/ContentContainer";
import AlertError from "../components/commons/AlertError";
import LoginOption from "../components/LoginOption";

export const styles = {
  appPadding: {
    padding: "15px"
  },
  paragraph: {
    paddingBottom: "15px"
  }
};

export class HomePage extends React.Component {
  state = {
    showError: false,
    focusAlert: false
  };

  alertRef = React.createRef();

  componentDidUpdate = (prevProps, prevState) => {
    this.focusAlert();
    if (prevProps.authenticated !== this.props.authenticated) {
      this.checkProfileAfterLogin(); // once authed, the source of Verfied.me or MyGCKey is irrelevant
    }
  };

  /**
   * Sets focus to the alert
   */
  focusAlert = () => {
    const { focusAlert } = this.state;

    if (focusAlert) {
      if (this.alertRef.current != null) {
        this.alertRef.current.focus();
      }
    }
  };

  verfiedLogin = () => {
    // TODO trigger Verfied.me login
    console.log("FIRE");
    // TODO remove; for demo purposed simulates an authenticated user who has not made a profile yet (first time logging in)
    this.props.setAuthAndHasAccount(true, false);
    // end remove
    // NOTE: this will automatically trigger componentDidUpdate with a change to the state of authenticated if it was successful
  };

  gcKeyLogin = () => {
    // TODO trigger GC Key login
    console.log("FIRE 2");
    // TODO remove; for demo purposed simulates an authenticated user with an already existing profile
    this.props.setAuthAndHasAccount(true, true);
    // end remove
    // NOTE: this will automatically trigger componentDidUpdate with a change to the state of authenticated if it was successful
  };

  checkProfileAfterLogin = () => {
    // TODO once externally authorized; use this function to determine if they have a profile or not

    // Set values and redirect accordingly
    // use the following: setAuthenticationState, setHasAccount
    if (this.props.authenticated) {
      if (this.props.hasAccount) {
        // if there already is an account; then go to the dashboard
        window.location.href = PATH.dashboard;
      } else {
        // if not, then go to create profile
        window.location.href = PATH.registrationForm;
      }
    } else {
      // TODO if not authenticated, then throw error
    }
  };

  verfiedMoreInfo = () => {
    // TODO more info pop up OR redirect
    console.log("I DID IT");
  };

  gcKeyMoreInfo = () => {
    // TODO more info pop up OR redirect
    console.log("I DID IT 2");
  };

  render() {
    const { currentLanguage } = this.props;
    const { showError } = this.state;

    return (
      <div className="app" style={styles.appPadding}>
        <ContentContainer>
          <PageTemplate title={LOCALIZE.homePage.title}>
            <h1 className="green-divider">{LOCALIZE.homePage.title}</h1>
            <p style={styles.paragraph}>{LOCALIZE.homePage.paragraph1}</p>
            <p style={styles.paragraph}>{LOCALIZE.homePage.paragraph2}</p>
            <Row>
              <Col>
                <LoginOption
                  buttonText={LOCALIZE.homePage.verfiedMe.button}
                  buttonAction={this.verfiedLogin}
                  detailText={LOCALIZE.homePage.verfiedMe.explaination}
                  moreInfoAction={this.verfiedMoreInfo}
                />
              </Col>
              <Col>
                <LoginOption
                  buttonText={LOCALIZE.homePage.gcKey.button}
                  buttonAction={this.gcKeyLogin}
                  detailText={LOCALIZE.homePage.gcKey.explaination}
                  moreInfoAction={this.gcKeyMoreInfo}
                />
              </Col>
            </Row>
          </PageTemplate>
        </ContentContainer>
      </div>
    );
  }
}

HomePage.propTypes = {
  currentLanguage: PropTypes.string
};

HomePage.defaultProps = {
  currentLanguage: "en"
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.loginReducer.authenticated,
    hasAccount: state.loginReducer.hasAccount
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setAuthAndHasAccount, setAuthenticationState, setHasAccount }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
