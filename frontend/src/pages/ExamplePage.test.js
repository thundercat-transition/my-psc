import React from "react";
import { shallow } from "enzyme";

// Components
import ExamplePage from "./ExamplePage";

describe("Home page tests", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ExamplePage />);
  });

  it("should render", () => {
    expect(wrapper.exists()).toBe(true);
  });
});
