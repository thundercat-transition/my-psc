import React, { Component } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "./commons/CustomButton";

import { Row, Col } from "react-bootstrap";
import LOCALIZE from "../resources/textResources";

const styles = {
  box: {
    border: "1px solid green",
    borderRadius: "5px",
    padding: 25
  },
  btn: {
    display: "block",
    margin: "24px auto"
  },
  infoLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  paragraph: {
    paddingBottom: "15px",
    overflow: "wrap"
  }
};

class LoginOption extends Component {
  static propTypes = {
    buttonText: PropTypes.string.isRequired,
    buttonAction: PropTypes.func.isRequired,
    detailText: PropTypes.string.isRequired,
    moreInfoAction: PropTypes.func.isRequired
  };

  state = {
    currentlyLoading: false
  };

  render() {
    const { buttonText, buttonAction, detailText, moreInfoAction } = this.props;

    return (
      <div role="form">
        <div style={styles.box}>
          <Col>
            <Row>
              <CustomButton
                label={
                  this.state.currentlyLoading ? (
                    // eslint-disable-next-line jsx-a11y/label-has-associated-control
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  ) : (
                    buttonText
                  )
                }
                type={"submit"}
                customStyle={styles.btn}
                buttonTheme={THEME.PRIMARY}
                disabled={this.state.currentlyLoading}
                action={buttonAction}
              />
            </Row>
            <Row>
              <p style={styles.paragraph}>{detailText}</p>
            </Row>
            <Row>
              <button
                aria-label={LOCALIZE.homePage.moreInfo}
                tabIndex="0"
                onClick={moreInfoAction}
                style={styles.infoLink}
              >
                {LOCALIZE.homePage.moreInfo}
              </button>
            </Row>
          </Col>
        </div>
      </div>
    );
  }
}

export default LoginOption;
