import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";

// Components
import Header from "./header/Header";
import PageDetails from "./footer/PageDetails";
import FooterLinks from "./footer/FooterLinks";
import SharePanel from "./footer/SharePanel";

/*
  Generic component that has header and footer components at top/bottom of every page

  "report", and "share" components can be toggled off by using `report=false` or `share=false`

  If a title is passed in, the page title will be rendered using helmet.
  To prevent this functionality, do not pass in a title.
*/

export class PageTemplate extends React.Component {
  state = {
    share: false,
    focusShare: false
  };

  closeShare = () => {
    this.setState({ share: false, focusShare: true });

    const shareButton = document.getElementById("wb-auto-2");
    if (shareButton !== null) {
      shareButton.focus();
    }
  };

  openShare = () => {
    this.setState(prevState => ({ ...prevState, share: true }));
  };

  render() {
    const { title, dateChanged, currentLanguage, children } = this.props;
    const { share, focusShare } = this.state;

    let helmetTitle = "";
    if (title !== null) {
      helmetTitle = title;
    }

    return (
      <>
        {share && <SharePanel onclick={this.closeShare} />}
        {!share && (
          <>
            <Header />
            <main className="container" role="main">
              <Helmet>
                <title>{helmetTitle}</title>
                <html lang={currentLanguage} />
              </Helmet>
              {children}
              <p />
              <PageDetails
                dateChanged={dateChanged}
                shareOnClick={this.openShare}
                focusShare={focusShare}
              />
            </main>
          </>
        )}
      </>
    );
  }
}

PageTemplate.propTypes = {
  dateChanged: PropTypes.string,
  title: PropTypes.string,
  currentLanguage: PropTypes.string,
  children: PropTypes.node
};

PageTemplate.defaultProps = {
  dateChanged: "2020-03-31",
  title: null,
  currentLanguage: "",
  children: React.Fragment
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PageTemplate);
