import React from "react";
import { shallow } from "enzyme";

// Components
import ReportIssue from "./ReportIssue";

describe("ReportIssue component", () => {
  it("should render", () => {
    const wrapper = shallow(<ReportIssue />);
    expect(wrapper.exists()).toBe(true);
  });
});
