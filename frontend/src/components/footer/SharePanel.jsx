import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { bindActionCreators } from "redux";

// Components
import LOCALIZE from "../../resources/textResources";

const styles = {
  overflowX: "hidden",
  overflowY: "auto"
};

export class SharePanel extends React.Component {
  sharePanelRef = React.createRef();

  componentDidMount() {
    this.focusDiv();
    window.addEventListener("keydown", this.onKeyDown);
  }

  componentDidUpdate() {
    this.focusDiv();
  }

  componentWillUnmount() {
    window.removeEventListener("keydown", this.onKeyDown);
  }

  focusDiv = () => {
    if (this.sharePanelRef) {
      this.sharePanelRef.current.focus();
    }
  };

  onKeyDown = e => {
    const { onclick } = this.props;
    // if 'esc' is hit, close menu
    if (e.keyCode === 27) {
      onclick();
    }
  };

  render() {
    const { pagelink, pageTitle, onclick, currentLanguage } = this.props;
    // since 'Reddit', 'digg', etc are the same across languages, they are hard-coded
    // stuff like 'Mail' is not

    return (
      <main className="container">
        <Helmet>
          <html lang={currentLanguage} />
        </Helmet>
        <div className="mfp-bg mfp-ready" />
        <div
          className="mfp-wrap mfp-close-btn-in mfp-auto-cursor mfp-ready"
          tabIndex="-1"
          style={styles}
        >
          <div className="mfp-container mfp-s-ready mfp-inline-holder">
            <div
              className="mfp-content"
              aria-labelledby="lbx-title"
              role="document"
              data-pgtitle={pagelink}
            >
              <section id="shr-pg0" className="shr-pg modal-dialog modal-content overlay-def">
                <header className="modal-header">
                  <h1 className="modal-title" id="lbx-title">
                    {LOCALIZE.footer.sharePanel.header}
                  </h1>
                </header>
                <div className="modal-body">
                  <ul className="list-unstyled colcount-xs-2">
                    <li>
                      <a
                        href={`https://bitly.com/a/bitmarklet?${pagelink}`}
                        className="shr-lnk bitly btn btn-default"
                        target="_blank"
                        ref={this.sharePanelRef}
                        rel="noreferrer noopener"
                      >
                        bitly
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://www.blogger.com/blog_this.pyra?t=&amp;u=${pagelink}&n=${pageTitle}`}
                        className="shr-lnk blogger btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Blogger
                      </a>
                    </li>
                    <li>
                      <a
                        href={`http://digg.com/submit?phase=2&amp;url=${pagelink}&title=${pageTitle}`}
                        className="shr-lnk digg btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Digg
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://www.diigo.com/post?url=${pagelink}&title=${pageTitle}`}
                        className="shr-lnk diigo btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Diigo
                      </a>
                    </li>
                    <li>
                      <a
                        href={`mailto:?to=&amp;subject=${pageTitle}body=${pagelink}`}
                        className="shr-lnk email btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        {LOCALIZE.footer.sharePanel.emailLabel}
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://www.facebook.com/sharer.php?u=${pagelink}&t=${pageTitle}`}
                        className="shr-lnk facebook btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Facebook
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://mail.google.com/mail/?view=cm&amp;fs=1&amp;tf=1&amp;to=&amp;su=${pageTitle}&amp;body=${pagelink}`}
                        className="shr-lnk gmail btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Gmail
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://www.linkedin.com/shareArticle?mini=true&amp;url=${pagelink}&amp;title=${pageTitle}&amp;ro=false&amp;summary=&amp;source=`}
                        className="shr-lnk linkedin btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        LinkedIn®
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://www.myspace.com/Modules/PostTo/Pages/?u=${pagelink}&amp;t=${pageTitle}`}
                        className="shr-lnk myspace btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        MySpace
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://www.pinterest.com/pin/create/button/?url=${pagelink}&amp;media=&amp;description=${pageTitle}`}
                        className="shr-lnk pinterest btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Pinterest
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://reddit.com/submit?url=${pagelink}&amp;title=${pageTitle}`}
                        className="shr-lnk reddit btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        reddit
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://www.tumblr.com/share/link?url=${pagelink}&amp;name=${pageTitle}&amp;description=`}
                        className="shr-lnk tumblr btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        tumblr
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://twitter.com/intent/tweet?text=${pageTitle}&amp;url=${pagelink}`}
                        className="shr-lnk twitter btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Twitter
                      </a>
                    </li>
                    <li>
                      <a
                        href={`https://compose.mail.yahoo.com/?to=&amp;subject=${pageTitle}&amp;body=${pagelink}`}
                        className="shr-lnk yahoomail btn btn-default"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        Yahoo! Mail
                      </a>
                    </li>
                  </ul>
                  <p className="col-sm-12 shr-dscl">{LOCALIZE.footer.sharePanel.bottomText}</p>
                  <div className="clearfix" />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-sm btn-primary pull-left popup-modal-dismiss"
                    title={LOCALIZE.footer.sharePanel.closePanelHover1}
                    onClick={onclick}
                  >
                    {LOCALIZE.footer.sharePanel.closePanel}
                    <span className="wb-inv">{LOCALIZE.footer.sharePanel.closePanelHover1}</span>
                  </button>
                </div>
                <button
                  title={LOCALIZE.footer.sharePanel.closePanelHover2}
                  type="button"
                  className="mfp-close"
                  onClick={onclick}
                >
                  ×<span className="wb-inv">{LOCALIZE.footer.sharePanel.closePanelHover2}</span>
                </button>
              </section>
            </div>
          </div>
          <span className="lbx-end wb-inv" />
        </div>
      </main>
    );
  }
}

SharePanel.propTypes = {
  pagelink: PropTypes.string,
  pageTitle: PropTypes.string,
  onclick: PropTypes.func.isRequired,
  currentLanguage: PropTypes.string
};

SharePanel.defaultProps = {
  pagelink: "http://localhost:3000/",
  pageTitle: "OAP-PAT",
  currentLanguage: ""
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SharePanel);
