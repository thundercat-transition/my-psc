import React from "react";
import PropTypes from "prop-types";

// Components
import LOCALIZE from "../../resources/textResources";

class ShareButton extends React.Component {
  shareButtonRef = React.createRef();

  componentDidMount() {
    const { focusShare } = this.props;

    // refocuses on the share button when the share panel is closed.
    if (focusShare) {
      this.shareButtonRef.current.focus();
    }
  }

  render() {
    const { onclick } = this.props;
    return (
      <button
        type="button"
        data-wb-share='{"pnlId": "pnl6", "lnkClass": "btn btn-default"}'
        onClick={onclick}
        className="col-sm-12 col-md-5 offset-md-2 col-lg-3 offset-lg-4 wb-share wb-init wb-share-inited shr-opn wb-lbx btn btn-default btn-block wb-lbx-inited wb-init"
        id="shareButton"
        ref={this.shareButtonRef}
      >
        <span className="glyphicon glyphicon-share" />
        {LOCALIZE.footer.shareButton}
      </button>
    );
  }
}

ShareButton.propTypes = {
  onclick: PropTypes.func,
  focusShare: PropTypes.bool
};

ShareButton.defaultProps = {
  onclick: null,
  focusShare: false
};

export default ShareButton;
