import React from "react";
import { mount } from "enzyme";

// Components
import { SharePanel } from "./SharePanel";

describe("SharePanel component", () => {
  it("should render", () => {
    const onclick = jest.fn();
    const wrapper = mount(<SharePanel onclick={onclick} />);
    expect(wrapper.exists()).toBe(true);
  });
});
