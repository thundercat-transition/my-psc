import React from "react";
import { shallow } from "enzyme";

// Components
import FooterLinks from "./FooterLinks";

describe("FooterLiks component", () => {
  it("should render", () => {
    const wrapper = shallow(<FooterLinks />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should contain an anchor tag linking to 'wb-cont' id", () => {
    const wrapper = shallow(<FooterLinks />);
    expect(wrapper.find("a").filterWhere(n => n.props().href === "#wb-cont")).toHaveLength(1);
  });
});
