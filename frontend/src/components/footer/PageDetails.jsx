import React from "react";
import PropTypes from "prop-types";

// Components
import DateModified from "./DateModified";
import ReportIssue from "./ReportIssue";
import ShareButton from "./ShareButton";

/*
  Generic component to contain the "Report a problem", "Share this page", and "Date Modified" components at the bottom of every page
  "report", and "share" components can be toggled off by using `report=false` or `share=false`
*/

export default function PageDetails({ report, share, dateChanged, shareOnClick, focusShare }) {
  return (
    <div className="pagedetails container">
      <div className="row">
        {report && <ReportIssue />}
        {share && <ShareButton onclick={shareOnClick} focusShare={focusShare} />}
      </div>
      <DateModified date={dateChanged} />
    </div>
  );
}

PageDetails.propTypes = {
  dateChanged: PropTypes.string.isRequired,
  report: PropTypes.bool,
  share: PropTypes.bool,
  shareOnClick: PropTypes.func,
  focusShare: PropTypes.bool
};

PageDetails.defaultProps = {
  report: true,
  share: true,
  shareOnClick: null,
  focusShare: false
};
