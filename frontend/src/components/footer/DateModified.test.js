import React from "react";
import { shallow } from "enzyme";

// Components
import DateModified from "./DateModified";

describe("DateModified component", () => {
  it("should render", () => {
    const wrapper = shallow(<DateModified date="2020-03-30" />);
    expect(wrapper.exists()).toBe(true);
  });
});
