import React from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { bindActionCreators } from "redux";

// Redux
import { setLanguage } from "../../redux/localizationActions";

// Constants
import LOCALIZE from "../../resources/textResources";

export class ChangeLanguage extends React.Component {
  switchLanguage() {
    const { setLanguage } = this.props;
    setLanguage(LOCALIZE.header.otherLanguageCode);
  }

  render() {
    return (
      <div>
        <button
          lang={LOCALIZE.header.otherLanguageCode}
          type="button"
          id="switchLangButton"
          className="swap-lang-button-link"
          onClick={() => this.switchLanguage()}
        >
          {LOCALIZE.header.otherLanguage}
        </button>
      </div>
    );
  }
}

ChangeLanguage.propTypes = {
  setLanguage: PropTypes.func
};

ChangeLanguage.defaultProps = {
  setLanguage: () => {}
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ChangeLanguage);
