import React from "react";

// Constants
import GovernmentSymbolEn from "./assets/sig-blk-en.svg";
import GovernmentSymbolFr from "./assets/sig-blk-fr.svg";
import LOCALIZE from "../../resources/textResources";

// Redux
import { LANGUAGES } from "../../redux/localizationActions";

// Components
import ChangeLanguage from "./ChangeLanguage";
import SkipNav from "./SkipNav";
import HeaderMenu from "./HeaderMenu";

const Header = () => {
  // Uses the localize feature to determine the language.
  // If the language is french (i.e. the other language is en), set the homepage image to the french varient
  const governmentSymbol =
    LOCALIZE.header.otherLanguageCode === LANGUAGES.english
      ? GovernmentSymbolFr
      : GovernmentSymbolEn;

  return (
    <div className="global-header">
      <SkipNav />

      <header>
        <div id="wb-bnr" className="container">
          <section id="wb-lng" className="text-right">
            <h2 className="wb-inv">{LOCALIZE.header.langSelectSection}</h2>
            <div className="row">
              <div className="col-md-12">
                <ul className="list-inline margin-bottom-none">
                  <li>
                    <ChangeLanguage />
                  </li>
                </ul>
              </div>
            </div>
          </section>
          <div className="row">
            <div
              className="col-xs-10 col-md-5"
              property="publisher"
              typeof="GovernmentOrganization"
            >
              <a href={LOCALIZE.header.canadaHomeLink} property="url">
                <img src={governmentSymbol} alt="" property="logo" />
                <span className="wb-inv" property="name">
                  {LOCALIZE.header.canadaHomeText}
                </span>
                <meta property="areaServed" typeof="Country" content="Canada" />
                <link property="logo" href={governmentSymbol} />
              </a>
            </div>
          </div>
        </div>
        <HeaderMenu />
      </header>
    </div>
  );
};
export default Header;
