import React from "react";
import { PropTypes } from "prop-types";

/**
  generic toggleable menu
  child elements are rendered only if in the expanded state


  originally based on: https://github.com/jsmanifest/modern-sidebar/
  refactored to be much simpler
 */

class ExpandableMenu extends React.Component {
  state = {
    expanded: false
  };

  componentDidMount() {
    window.addEventListener("keydown", this.onKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener("keydown", this.onKeyDown);
  }

  toggleMenu = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded
    }));
  };

  onClick = () => {
    this.toggleMenu();
  };

  onKeyDown = e => {
    // if 'esc' is hit, close menu
    if (e.keyCode === 27) {
      this.setState({ expanded: false });
    }

    // if 'down arrow' is hit
    if (e.keyCode === 40) {
      // TODO add menu navigation using arrow keys
    }

    if (e.keyCode === 38) {
      // if 'up arrow' is hit
    }
  };

  render() {
    const { children } = this.props;
    const { expanded } = this.state;
    return (
      <>
        <button type="button" aria-haspopup="true" aria-expanded={expanded} onClick={this.onClick}>
          <span className="wb-inv">Main </span>Menu
          <span className="expicon glyphicon glyphicon-chevron-down" />
        </button>
        {expanded && children}
      </>
    );
  }
}

ExpandableMenu.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.object])
};

ExpandableMenu.defaultProps = {
  children: React.Fragment
};

export default ExpandableMenu;
