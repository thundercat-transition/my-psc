import React from "react";
import { shallow } from "enzyme";

// Components
import Header from "./Header";
import ChangeLanguage from "./ChangeLanguage";

describe("Header component", () => {
  it("should render", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should have a language switch button", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find(ChangeLanguage)).toHaveLength(1);
  });
});
