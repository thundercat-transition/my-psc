import React from "react";
import { Link } from "react-router-dom";
import { shallow } from "enzyme";

// Components
import { HeaderLogin } from "./HeaderLogin";

// Constants
import LOCALIZE from "../../resources/textResources";
import { PATH } from "../../resources/constants";

describe("HeaderLogin component", () => {
  it("should render", () => {
    const wrapper = shallow(<HeaderLogin />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should display a username & logout text if you are logged in", () => {
    const wrapper = shallow(<HeaderLogin authenticated username="usernameTest" />);
    expect(wrapper.text()).toContain("usernameTest");
    expect(wrapper.find(Link).first().text()).toContain(LOCALIZE.header.loginState.logoutText);
  });

  it("should not display a username & should display login text if you are logged out", () => {
    const wrapper = shallow(<HeaderLogin username="usernameTest" />);
    expect(wrapper.text()).not.toContain("usernameTest");
    expect(wrapper.find(Link).first().text()).toContain(LOCALIZE.header.loginState.loginText);
  });

  it("should redirect you to the Home page when clicked (when logged in)", () => {
    const setAuthenticationStateFn = jest.fn();
    const wrapper = shallow(
      <HeaderLogin
        authenticated
        username="usernameTest"
        setAuthenticationState={setAuthenticationStateFn}
      />
    );

    // Function to set your state to being logged out is called when clicked
    wrapper.find("#header-login-text").first().simulate("click");
    expect(setAuthenticationStateFn.mock.calls.length).toBe(1);

    // Links to home page if you are logged in
    expect(wrapper.find(Link).filterWhere(n => n.props().to === PATH.home).length).toEqual(1);
  });

  it("should redirect you to the Login page when clicked (when logged out)", () => {
    const setAuthenticationStateFn = jest.fn();
    const wrapper = shallow(<HeaderLogin setAuthenticationState={setAuthenticationStateFn} />);

    // Links to home page if you are logged in
    expect(wrapper.find(Link).filterWhere(n => n.props().to === PATH.login).length).toEqual(1);
  });
});
