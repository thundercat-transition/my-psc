import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

// Redux/functions
import { setCurrentUserInfo, setAuthenticationState } from "../../redux/loginActions";
import { requestJWTWithCredentials } from "../../helpers/jwtUtil";

// Constants
import LOCALIZE from "../../resources/textResources";
import { PATH } from "../../resources/constants";

const styles = {
  loginContent: {
    padding: "12px 32px 0 32px",
    marginBottom: 36,
    border: "1px solid #cdcdcd",
    maxWidth: "80%"
  },
  inputTitles: {
    padding: "12px 0 6px 0",
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #577ba8",
    borderRadius: 2
  },
  loginBtn: {
    width: 175,
    display: "block",
    margin: "24px auto"
  },
  loginError: {
    marginTop: 12,
    color: "#923534",
    fontWeight: "bold"
  },
  validationError: {
    color: "red",
    paddingTop: 8
  }
};

export class LoginForm extends React.Component {
  state = {
    username: "",
    password: "",
    wrongCredentials: false
  };

  handleLoginSubmit = event => {
    const { history, authenticated, setCurrentUserInfo, setAuthenticationState } = this.props;
    const { username, password } = this.state;

    // Determine if login is successful
    requestJWTWithCredentials(username, password).then(data => {
      // Successful login
      if (data !== null) {
        this.setState(prevState => ({ ...prevState, wrongCredentials: false }));

        setAuthenticationState(!authenticated);
        const userInfo = { username };
        setCurrentUserInfo(userInfo);
        history.push(PATH.admin);
      } else {
        this.setState(prevState => ({ ...prevState, wrongCredentials: true }));

        // Focus on password field for accessibility users
        document.getElementById("password").focus();
      }
    });

    event.preventDefault();
  };

  handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  render() {
    const { username, password, wrongCredentials } = this.state;

    return (
      <div role="form">
        <div className="col-centered" style={styles.loginContent}>
          <form id="login-form" onSubmit={event => this.handleLoginSubmit(event)}>
            <div>
              <div style={styles.inputTitles}>
                <label htmlFor="username">{LOCALIZE.loginPage.usernameHeader}</label>
              </div>
              <input
                aria-required="true"
                type="text"
                id="username"
                style={styles.inputs}
                onChange={event => this.handleUsernameChange(event)}
                value={username}
                placeholder={LOCALIZE.loginPage.usernameHeader}
              />
            </div>
            <div>
              <div style={styles.inputTitles}>
                <label htmlFor="password">{LOCALIZE.loginPage.passwordHeader}</label>
              </div>
              <input
                aria-label={
                  wrongCredentials
                    ? `${LOCALIZE.loginPage.loginError} ${LOCALIZE.loginPage.passwordAriaLabel}`
                    : LOCALIZE.loginPage.passwordHeader
                }
                aria-invalid={wrongCredentials}
                aria-required="true"
                type="password"
                id="password"
                style={styles.inputs}
                onChange={event => this.handlePasswordChange(event)}
                value={password}
                placeholder={LOCALIZE.loginPage.passwordHeader}
              />
            </div>
            <div>
              {wrongCredentials && (
                <label id="wrong-credentials-error" htmlFor="password" style={styles.loginError}>
                  {LOCALIZE.loginPage.loginError}
                </label>
              )}
            </div>
            <input
              id="form-submit-btn"
              style={styles.loginBtn}
              className="btn btn-primary"
              type="submit"
              value={LOCALIZE.loginPage.loginButton}
            />
          </form>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  history: PropTypes.shape({ goBack: PropTypes.func, push: PropTypes.func }),
  authenticated: PropTypes.bool,
  setCurrentUserInfo: PropTypes.func,
  setAuthenticationState: PropTypes.func
};

LoginForm.defaultProps = {
  history: { goBack: () => {}, push: () => {} },
  authenticated: false,
  setCurrentUserInfo: () => {},
  setAuthenticationState: () => {}
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.loginReducer.authenticated
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setCurrentUserInfo, setAuthenticationState }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginForm));
