import React from "react";
import PropTypes from "prop-types";
import Modal from "react-modal";
import ReactMarkdown from "react-markdown";

const modalStyle = {
  overlay: {},
  content: {
    position: "absolute",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -80%)"
  }
};

export const PopupWindow = props => {
  const { closeButtonText, description, handleClose, isDisplayed, title } = props;

  return (
    <Modal
      isOpen={isDisplayed}
      contentLabel={title}
      className="modal-dialog modal-content overlay-def"
      style={modalStyle}
    >
      <header className="modal-header">
        <h2 className="modal-title">{title}</h2>
      </header>

      <div className="modal-body">
        <ReactMarkdown
          source={description}
          disallowedTypes={["paragraph"]}
          unwrapDisallowed
          escapeHtml={false}
        />
      </div>
      <div className="modal-footer">
        <button
          id="popup-close-btn"
          title="Close overlay (escape key)"
          type="button"
          className="btn btn-sm btn-primary pull-right popup-modal-dismiss"
          onClick={handleClose()}
        >
          {closeButtonText}
        </button>
      </div>

      {/* "X" button to close at top-right of the window */}
      <button
        id="popup-close-btn-x"
        title="Close overlay (escape key)"
        type="button"
        className="mfp-close"
        onClick={handleClose()}
      >
        ×
      </button>
    </Modal>
  );
};

PopupWindow.propTypes = {
  closeButtonText: PropTypes.string,
  description: PropTypes.string,
  handleClose: PropTypes.func,
  isDisplayed: PropTypes.bool,
  title: PropTypes.string
};

PopupWindow.defaultProps = {
  closeButtonText: "Close",
  description: "",
  handleClose: () => {},
  isDisplayed: false,
  title: ""
};

export default PopupWindow;
