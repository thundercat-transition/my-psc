/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { PropTypes } from "prop-types";
import { Route, Redirect } from "react-router-dom";

/**
 * @param {*} component: the component you want to render
 * @param {*} auth: boolean flag to check if the user is authenticated to view the page
 * @param {*} redirectTo: the path to redirect the user to, if provided
 * @param {*} rest: the remainder of the props
 */
export const PrivateRoute = ({ component: Component, auth, redirectTo, ...rest }) => (
  <Route
    {...rest}
    render={props => (auth ? <Component {...props} /> : <Redirect to={redirectTo} />)}
  />
);

PrivateRoute.propTypes = {
  auth: PropTypes.bool,
  component: PropTypes.elementType,
  redirectTo: PropTypes.string
};

PrivateRoute.defaultProps = {
  auth: false,
  component: {},
  redirectTo: ""
};

export default PrivateRoute;
