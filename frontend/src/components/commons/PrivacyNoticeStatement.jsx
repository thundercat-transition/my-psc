import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../resources/textResources";
import PopupBox, { BUTTON_TYPE } from "./PopupBox";

const styles = {
  privacyNoticeLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  }
};

class PrivacyNoticeStatement extends Component {
  static propTypes = {
    showPopup: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired
  };

  // opening provided link in another tab
  openLinkInOtherTab = link => {
    window.open(link, "_blank");
  };

  render() {
    return (
      <PopupBox
        isCloseButtonVisible={false}
        isBackdropStatic={true}
        show={this.props.showPopup}
        handleClose={() => {}}
        title={LOCALIZE.privacyNoticeStatement.title}
        description={
          <div>
            <section aria-labelledby="privacy-notice-statement">
              <h2 id="privacy-notice-statement">
                {LOCALIZE.privacyNoticeStatement.descriptionTitle}
              </h2>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.privacyNoticeStatement.paragraph1,
                  <button
                    aria-label={LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLinkTitle}
                    tabIndex="0"
                    onClick={() =>
                      this.openLinkInOtherTab(
                        LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLink
                      )
                    }
                    style={styles.privacyNoticeLink}
                  >
                    {LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLinkTitle}
                  </button>,
                  <button
                    aria-label={LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLinkTitle}
                    tabIndex="0"
                    onClick={() =>
                      this.openLinkInOtherTab(
                        LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLink
                      )
                    }
                    style={styles.privacyNoticeLink}
                  >
                    {LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLinkTitle}
                  </button>,
                  <button
                    aria-label={LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLinkTitle}
                    tabIndex="0"
                    onClick={() =>
                      this.openLinkInOtherTab(
                        LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLink
                      )
                    }
                    style={styles.privacyNoticeLink}
                  >
                    {LOCALIZE.privacyNoticeStatement.publicServiceEmploymentActLinkTitle}
                  </button>,
                  <button
                    aria-label={LOCALIZE.privacyNoticeStatement.privacyActLinkTitle}
                    tabIndex="0"
                    onClick={() =>
                      this.openLinkInOtherTab(LOCALIZE.privacyNoticeStatement.privacyActLink)
                    }
                    style={styles.privacyNoticeLink}
                  >
                    {LOCALIZE.privacyNoticeStatement.privacyActLinkTitle}
                  </button>
                )}
              </p>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.privacyNoticeStatement.paragraph2,
                  <button
                    aria-label={LOCALIZE.privacyNoticeStatement.infoSourceLinkTitle}
                    tabIndex="0"
                    onClick={() =>
                      this.openLinkInOtherTab(LOCALIZE.privacyNoticeStatement.infoSourceLink)
                    }
                    style={styles.privacyNoticeLink}
                  >
                    {LOCALIZE.privacyNoticeStatement.infoSourceLinkTitle}
                  </button>
                )}
              </p>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.privacyNoticeStatement.paragraph3,
                  <button
                    aria-label={
                      LOCALIZE.privacyNoticeStatement.privacyCommissionerOfCanadaLinkTitle
                    }
                    tabIndex="0"
                    onClick={() =>
                      this.openLinkInOtherTab(
                        LOCALIZE.privacyNoticeStatement.privacyCommissionerOfCanadaLink
                      )
                    }
                    style={styles.privacyNoticeLink}
                  >
                    {LOCALIZE.privacyNoticeStatement.privacyCommissionerOfCanadaLinkTitle}
                  </button>
                )}
              </p>
            </section>
          </div>
        }
        rightButtonType={BUTTON_TYPE.primary}
        rightButtonTitle={LOCALIZE.commons.close}
        rightButtonAction={this.props.handleClose}
      />
    );
  }
}

export { PrivacyNoticeStatement as UnconnectedPrivacyNoticeStatement };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(PrivacyNoticeStatement);
