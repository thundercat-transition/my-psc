import React from "react";
import { shallow } from "enzyme";

// Components
import { FocusHeader } from "./FocusHeader";

describe("FocusHeader component", () => {
  it("should render", () => {
    const wrapper = shallow(<FocusHeader>Test text</FocusHeader>);
    expect(wrapper.exists()).toBe(true);
  });
});
