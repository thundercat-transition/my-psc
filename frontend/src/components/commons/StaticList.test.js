import React from "react";
import { shallow } from "enzyme";

// Components
import StaticList from "./StaticList";

describe("Assessment Page component", () => {
  it("should render", () => {
    const list = ["text with no link"];
    const wrapper = shallow(<StaticList list={list} />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should render multiple elements", () => {
    const list = [
      "text with no link",
      { text: "text with {0}", link: "google.ca" },
      { text: "text with {0}", bold: "bold text" }
    ];

    const wrapper = shallow(<StaticList list={list} />);
    expect(wrapper.exists()).toBe(true);
  });
});
