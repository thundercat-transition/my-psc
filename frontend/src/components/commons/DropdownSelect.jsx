import { connect } from "react-redux";
import React from "react";
import { PropTypes } from "prop-types";
import LOCALIZE from "../../resources/textResources";
import { getLineSpacingCSS } from "../../redux/accommodationsReducer";
import Select from "react-select";

const styles = {
  option: {
    padding: "8px 12px",
    height: "38px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class DropdownSelect extends React.Component {
  // ordering options by label values
  getOrderedByLabelsOptions = options => {
    options.sort((a, b) => {
      const labelA = a.label.toUpperCase();
      const labelB = b.label.toUpperCase();
      if (labelA < labelB) {
        return -1;
      }
      if (labelA > labelB) {
        return 1;
      }

      return 0;
    });
    return options;
  };

  // ordering options by value values
  getOrderedByValuesOptions = options => {
    options.sort((a, b) => {
      const valueA = parseInt(a.value);
      const valueB = parseInt(b.value);
      if (valueA < valueB) {
        return -1;
      }
      if (valueA > valueB) {
        return 1;
      }

      return 0;
    });
    return options;
  };

  // filtering options by label only
  filterOption = (option, inputValue) => {
    // making sure that provided label is a "string" to avoid errors
    const label = option.label.toString();
    return label.toUpperCase().includes(inputValue.toUpperCase());
  };

  render() {
    const {
      defaultValue,
      onChange,
      options,
      ariaLabelledBy,
      ariaRequired,
      idPrefix,
      hasPlaceholder,
      customRef,
      isDisabled,
      customStyle,
      isValid,
      isMulti,
      customPlaceholderWording,
      orderByLabels,
      orderByValues
    } = this.props;

    let acccommodationsStyle = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      acccommodationsStyle = { ...acccommodationsStyle, ...getLineSpacingCSS() };
    } else {
      acccommodationsStyle = { ...acccommodationsStyle };
    }

    const customStyles = {
      control: (provided, state) => ({
        ...provided,
        ...acccommodationsStyle,
        ...customStyle,
        // style when focused
        ...(state.isFocused
          ? {
              border: "none",
              outline: "none",
              boxShadow: `0 0 18px ${isValid ? "#00565e" : "#923534"}`
            }
          : {})
      }),
      option: provided => ({
        ...provided,
        ...acccommodationsStyle
      }),
      menu: provided => ({
        ...provided
      }),
      // forcing value container width to respect the dropdown container width
      valueContainer: provided => ({
        ...provided,
        ...{ width: 1 }
      }),
      menuPortal: provided => ({
        ...provided,
        ...{ zIndex: 9999 }
      })
    };

    let valueLabel = LOCALIZE.dropdownSelect.pleaseSelect;
    if (typeof defaultValue.label !== "undefined") {
      valueLabel = defaultValue.label;
    }

    return (
      <div>
        {!isMulti && (
          <label id={`${idPrefix}-value-label`} style={styles.hiddenText}>
            {valueLabel}
          </label>
        )}

        <Select
          {...this.props}
          ref={customRef}
          className={
            isValid
              ? `valid-field ${idPrefix}-classname w-100`
              : `invalid-field ${idPrefix}-classname w-100`
          }
          menuPortalTarget={document.body}
          id={`${idPrefix}-react-select-dropdown`}
          onChange={(event, action) => onChange(event, action)}
          aria-labelledby={`${ariaLabelledBy} ${idPrefix}-value-label`}
          aria-required={ariaRequired ? "true" : "false"}
          styles={customStyles}
          value={defaultValue}
          isDisabled={isDisabled}
          isMulti={isMulti}
          placeholder={
            hasPlaceholder ? customPlaceholderWording || LOCALIZE.dropdownSelect.pleaseSelect : ""
          }
          noOptionsMessage={() => LOCALIZE.dropdownSelect.noOptions}
          filterOption={this.filterOption}
          options={
            orderByValues
              ? this.getOrderedByValuesOptions(options)
              : orderByLabels
              ? this.getOrderedByLabelsOptions(options)
              : options
          }
          isSearchable={true}
          menuPlacement={this.props.menuPlacement}
          maxMenuHeight={this.props.maxMenuHeight}
          overflow="visible"
        ></Select>
      </div>
    );
  }
}

DropdownSelect.propTypes = {
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.object]),
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    })
  ),
  ariaLabelledBy: PropTypes.string,
  ariaRequired: PropTypes.bool,
  idPrefix: PropTypes.string,
  hasPlaceholder: PropTypes.bool,
  customRef: PropTypes.object,
  isDisabled: PropTypes.bool,
  customStyle: PropTypes.object,
  isValid: PropTypes.bool,
  isMulti: PropTypes.bool,
  customPlaceholderWording: PropTypes.string,
  menuPlacement: PropTypes.string,
  orderByLabels: PropTypes.bool,
  orderByValues: PropTypes.bool,
  maxMenuHeight: PropTypes.string
};

DropdownSelect.defaultProps = {
  defaultValue: "",
  onChange: () => {},
  options: [],
  ariaLabelledBy: "",
  ariaRequired: false,
  idPrefix: "",
  hasPlaceholder: false,
  accommodations: {
    fontFamily: "Nunito Sans",
    fontSize: "16px",
    loadedSaved: true,
    spacing: false,
    triggerRerender: false
  },
  isDisabled: false,
  customStyle: {},
  isValid: true,
  isMulti: false,
  customPlaceholderWording: null,
  menuPlacement: "bottom",
  orderByLabels: true,
  orderByValues: false,
  maxMenuHeight: "200px"
};

// props spreading is needed to pass ref
// eslint-disable-next-line react/jsx-props-no-spreading
const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(
  React.forwardRef((props, innerRef) => <DropdownSelect ref={innerRef} {...props} />)
);
