import LOCALIZE from "../../resources/textResources";

const alternateColorsStyle = (id, height) => {
  const styles = {
    rowContainerBasicStyle: {
      width: "100%",
      height: `${height}px`,
      padding: "8px 0 8px 12px"
    },
    rowContainerDark: {
      backgroundColor: "#F3F3F3"
    },
    rowContainerLight: {
      backgroundColor: "white"
    },
    rowContainerBorder: {
      borderTop: "1px solid #CECECE"
    }
  };

  if (id === 0) {
    return { ...styles.rowContainerBasicStyle, ...styles.rowContainerLight };
  }
  if (id % 2 === 0) {
    return {
      ...styles.rowContainerBasicStyle,
      ...styles.rowContainerBorder,
      ...styles.rowContainerLight
    };
  }
  return {
    ...styles.rowContainerBasicStyle,
    ...styles.rowContainerBorder,
    ...styles.rowContainerDark
  };
};

// reference: assigned_test_status.py
const TEST_STATUS = {
  ASSIGNED: 11,
  READY: 12,
  ACTIVE: 13,
  LOCKED: 14,
  PAUSED: 15,
  QUIT: 19,
  SUBMITTED: 20,
  UNASSIGNED: 21,
  PRE_TEST: 22
};

// UIT test status
const UIT_TEST_STATUS = {
  TAKEN: 91,
  NOT_TAKEN: 92,
  IN_PROGRESS: 93,
  UNASSIGNED: 94
};

function getUitTestStatus(uitTestStatus, codeDeactivated) {
  switch (uitTestStatus) {
    case UIT_TEST_STATUS.IN_PROGRESS:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testInProgress;
    case UIT_TEST_STATUS.TAKEN:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.testTaken;
    case UIT_TEST_STATUS.UNASSIGNED:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testUnassigned;
    default:
      if (codeDeactivated) {
        return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
          .testCodeDeactivated;
      }
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testNotTaken;
  }
}
export default TEST_STATUS;

export { alternateColorsStyle, UIT_TEST_STATUS, getUitTestStatus };
