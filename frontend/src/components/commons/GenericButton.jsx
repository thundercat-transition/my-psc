import React from "react";
import { Link } from "react-router-dom";
import { PropTypes } from "prop-types";

export const GenericButton = props => {
  const { ariaLabel, data, linkPath, onClickFunc, text } = props;

  // Fixes a double-speak accessibility issue by returning a link styled as a button
  // If no link is defined, just return a regular button element
  return linkPath !== undefined ? (
    <Link
      to={linkPath}
      className="main btn btn-primary"
      onClick={() => onClickFunc(data)}
      aria-label={ariaLabel}
    >
      {text}
    </Link>
  ) : (
    <button
      type="button"
      className="main btn btn-primary"
      onClick={() => onClickFunc(data)}
      aria-label={ariaLabel}
    >
      {text}
    </button>
  );
};

GenericButton.propTypes = {
  ariaLabel: PropTypes.string,
  data: PropTypes.shape({}),
  linkPath: PropTypes.string,
  onClickFunc: PropTypes.func,
  text: PropTypes.string
};

GenericButton.defaultProps = {
  ariaLabel: "",
  data: {},
  linkPath: undefined,
  onClickFunc: () => {},
  text: ""
};

export default GenericButton;
