import React from "react";

import { PropTypes } from "prop-types";
import LOCALIZE from "../../resources/textResources";

export const AlertError = React.forwardRef(({ text }, ref) => {
  return (
    <div
      id="errors-validation"
      className="alert alert-danger"
      tabIndex="-1"
      ref={ref}
      alt={LOCALIZE.alerts.errorAlt}
    >
      <p>{text}</p>
    </div>
  );
});

AlertError.defaultProps = {
  text: ""
};

AlertError.propTypes = {
  text: PropTypes.string
};

AlertError.displayName = "AlertError";

export default AlertError;
