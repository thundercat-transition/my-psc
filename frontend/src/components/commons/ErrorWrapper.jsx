import React from "react";
import { PropTypes } from "prop-types";

// Components
import { logError } from "../../helpers/logUtil";
import UnexpectedErrorPage from "../../pages/UnexpectedErrorPage";

export class ErrorWrapper extends React.Component {
  state = {
    hasError: false
  };

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    const { enzymeTestFunc } = this.props;

    logError(error, info);
    enzymeTestFunc(error, info); // Unit testing purposes only - this will do nothing in other scenarios due to defaultProps
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (!hasError) {
      return children;
    }

    return (
      <>
        <UnexpectedErrorPage />
      </>
    );
  }
}

ErrorWrapper.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.object]),
  enzymeTestFunc: PropTypes.func
};

ErrorWrapper.defaultProps = {
  children: React.Fragment,
  enzymeTestFunc: () => {}
};

export default ErrorWrapper;
