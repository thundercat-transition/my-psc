import React, { Component } from "react";
import PropTypes from "prop-types";
import Modal from "react-bootstrap/Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import "../../css/popup-box.css";
import LOCALIZE from "../../resources/textResources";
import { connect } from "react-redux";
import CustomButton, { THEME } from "./CustomButton";
import { getLineSpacingCSS } from "../../redux/accommodationsReducer";
import { Row, Col } from "react-bootstrap";

export const BUTTON_TYPE = {
  primary: THEME.PRIMARY,
  secondary: THEME.SECONDARY,
  success: THEME.SUCCESS,
  danger: THEME.DANGER,
  // useful if you have some conditions to display or not the button
  none: "NONE"
};

export const BUTTON_STATE = {
  disabled: true,
  enabled: false
};

// default custom style
const customStyles = {
  content: {
    maxWidth: "60%",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#F5FAFB"
  },
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)"
  }
};

// style that allows drop down to overlap the modal, so the drop down is always visible, even if it exceeds the modal window
const customStylesWithVisibleOverflow = {
  content: {
    maxWidth: "60%",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#F5FAFB",
    padding: 0,
    overflow: "visible"
  },
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)"
  }
};

const styles = {
  contentPadding: {
    padding: 15
  },
  title: {
    margin: "3px 0px 3px 6px"
  },
  descriptionContainer: {
    width: "100%"
  },
  rightButton: {
    minHeight: 38,
    whiteSpace: "nowrap",
    textAlign: "center",
    // This is the equivalent in Flex of minWidth:100
    flex: "0 0 100px",
    // Remove minWidth from CustomButton
    minWidth: "auto"
  },
  leftButton: {
    minHeight: 38,
    whiteSpace: "nowrap",
    textAlign: "center",
    // This is the equivalent in Flex of minWidth:100
    flex: "0 0 100px",
    // Remove minWidth from CustomButton
    minWidth: "auto"
  },
  overflowVisible: {
    overflow: "visible"
  },
  icon: {
    verticalAlign: "middle"
  },
  buttonLabel: {
    marginLeft: 6,
    verticalAlign: "middle"
  },
  closeButton: {
    position: "absolute",
    right: 0,
    top: 0,
    fontSize: 24,
    background: "transparent",
    border: "none",
    color: "black"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  noStyle: {},
  modalBody: {
    borderBottom: "1px solid #CECECE",
    borderTop: "1px solid #CECECE",
    // maxHeight will be 60% of the viewport height
    maxHeight: "60vh"
  },
  header: {
    overflow: "auto",
    maxHeight: "20vh"
  },
  footer: {
    display: "flex",
    overflowX: "auto",
    maxHeight: "20vh",
    justifyContent: "space-between"
  },
  footerOnlyOneButton: {
    display: "flex",
    overflowX: "auto",
    maxHeight: "20vh",
    justifyContent: "flex-end"
  }
};

class PopupBox extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      show: PropTypes.bool,
      handleClose: PropTypes.func,
      customModalStyle: PropTypes.object,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      leftButtonType: PropTypes.string,
      leftButtonTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
      leftButtonIcon: PropTypes.symbol,
      leftButtonIconCustomStyle: PropTypes.object,
      leftButtonLabel: PropTypes.string,
      leftButtonState: PropTypes.string,
      leftButtonAction: PropTypes.func,
      // you should only use this props to change the button's color => leftButtonCustomStyle={{backgroundColor: "<color_code>"}}
      leftButtonCustomStyle: PropTypes.object,
      rightButtonType: PropTypes.string,
      rightButtonTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
      rightButtonIcon: PropTypes.symbol,
      rightButtonIconCustomStyle: PropTypes.object,
      rightButtonLabel: PropTypes.string,
      rightButtonAction: PropTypes.func,
      rightButtonState: PropTypes.string,
      // you should only use this props to change the button's color => leftButtonCustomStyle={{backgroundColor: "<color_code>"}}
      rightButtonCustomStyle: PropTypes.object,
      isBackdropStatic: PropTypes.bool,
      shouldCloseOnEsc: PropTypes.bool,
      onPopupOpen: PropTypes.func,
      onPopupClose: PropTypes.func,
      overflowVisible: PropTypes.bool,
      displayCloseButton: PropTypes.bool,
      closeButtonAction: PropTypes.func,
      size: PropTypes.string,
      backdropClassName: PropTypes.string
    };
    // click away or esc to close
    PopupBox.defaultProps = {
      isBackdropStatic: false,
      overflowVisible: false,
      customModalStyle: {},
      size: "xl"
    };
  }

  leftButtonCloseAndAction = () => {
    if (this.props.leftButtonAction) {
      this.props.leftButtonAction();
    }
    this.props.handleClose();
  };

  rightButtonCloseAndAction = () => {
    if (this.props.rightButtonAction) {
      this.props.rightButtonAction();
    }
    this.props.handleClose();
  };

  handleOnAfterOpen = () => {
    // focus on title on modal open
    document.getElementById("modal-heading").focus();
    this.removeScrollFromBody();
    this.props.onPopupOpen();
  };

  handleOnAfterClose = () => {
    this.putBackScrollFromBody();
    this.props.onPopupClose();
  };

  // removing scroll from body
  removeScrollFromBody = () => {
    document.body.style.overflow = "hidden";
  };

  // put back scroll from body
  putBackScrollFromBody = () => {
    document.body.removeAttribute("style");
  };

  // when closing the popupbox
  handleHide = () => {
    this.props.show = false;
    this.props.handleClose();
  };

  render() {
    const {
      show,
      handleClose,
      title,
      description,
      leftButtonType,
      leftButtonTitle,
      leftButtonIcon,
      leftButtonLabel,
      leftButtonState,
      rightButtonType,
      rightButtonTitle,
      rightButtonIcon,
      rightButtonLabel,
      rightButtonState,
      currentLanguage,
      customModalStyle,
      size,
      backdropClassName
    } = this.props;

    // If a root node exists, the app is being served, otherwise it's a unit test.
    let ariaHideApp = true;
    if (document.getElementById("#root")) {
      Modal.setAppElement("#root");
    } else {
      // Unit tests do not consider outside of the dialog.
      ariaHideApp = false;
    }

    // add accomodations
    let customStyleWithAccom = {
      ...customStyles
    };
    let customStylesWithVisibleOverflowWithAccom = {
      ...customStylesWithVisibleOverflow
    };

    customStyleWithAccom.fontFamily = this.props.accommodations.fontFamily;
    customStyleWithAccom.fontSize = this.props.accommodations.fontSize;
    customStylesWithVisibleOverflowWithAccom.fontFamily = this.props.accommodations.fontFamily;
    customStylesWithVisibleOverflowWithAccom.fontSize = this.props.accommodations.fontSize;
    if (this.props.accommodations.spacing) {
      customStyleWithAccom = {
        ...customStyleWithAccom,
        ...getLineSpacingCSS(),
        ...customModalStyle
      };
      customStylesWithVisibleOverflowWithAccom = {
        ...customStylesWithVisibleOverflowWithAccom,
        ...getLineSpacingCSS(),
        ...customModalStyle
      };
    } else {
      customStyleWithAccom = {
        ...customStyleWithAccom,
        ...{ padding: 0 },
        ...customModalStyle
      };
      customStylesWithVisibleOverflowWithAccom = {
        ...customStylesWithVisibleOverflowWithAccom,
        ...{ padding: 0 },
        ...customModalStyle
      };
    }

    return (
      <Modal
        show={this.props.show}
        onHide={this.props.handleClose}
        onEntered={
          this.props.onPopupOpen ? this.handleOnAfterOpen : () => this.removeScrollFromBody()
        }
        onExited={
          this.props.onPopupClose ? this.handleOnAfterClose : () => this.putBackScrollFromBody()
        }
        keyboard={this.props.shouldCloseOnEsc}
        backdrop={this.props.isBackdropStatic ? "static" : true}
        aria-labelledby={"modal-heading"}
        aria-describedby={"modal-description"}
        centered={true}
        size={size}
        scrollable={!this.props.overflowVisible}
        backdropClassName={backdropClassName}
        autoFocus={true}
        style={
          this.props.overflowVisible
            ? customStylesWithVisibleOverflowWithAccom
            : customStyleWithAccom
        }
      >
        <div lang={currentLanguage} id="unit-test-popup" className="notranslate">
          <Modal.Header style={styles.header}>
            <Row className="align-items-center">
              <Col className="col float-left">
                <h1 id="modal-heading" className="popup-title" style={styles.title}>
                  {title}
                </h1>
              </Col>

              {this.props.displayCloseButton && (
                <div className="float-right">
                  <button
                    className="close-button btn btn-secondary"
                    style={styles.closeButton}
                    onClick={this.props.closeButtonAction}
                  >
                    <FontAwesomeIcon icon={faTimes} />
                    <span style={styles.hiddenText}>{LOCALIZE.commons.close}</span>
                  </button>
                </div>
              )}
            </Row>
          </Modal.Header>

          <Modal.Body style={styles.modalBody}>
            <div
              id="modal-description"
              style={
                this.props.overflowVisible
                  ? { ...styles.contentPadding, ...styles.overflowVisible }
                  : styles.contentPadding
              }
            >
              <div style={styles.descriptionContainer}>{description}</div>
            </div>
          </Modal.Body>

          <Modal.Footer
            // If there isn't any Left Button, then we can assume there's only a Right Button
            style={
              leftButtonType && leftButtonType !== BUTTON_TYPE.none
                ? styles.footer
                : styles.footerOnlyOneButton
            }
          >
            {leftButtonType && leftButtonType !== BUTTON_TYPE.none && (
              <CustomButton
                buttonId="unit-test-left-btn"
                label={
                  leftButtonTitle && leftButtonIcon ? (
                    <div>
                      <FontAwesomeIcon
                        icon={leftButtonIcon}
                        style={
                          this.props.leftButtonIconCustomStyle
                            ? this.props.leftButtonIconCustomStyle
                            : styles.icon
                        }
                      />
                      <span style={styles.buttonLabel}>{leftButtonTitle}</span>
                    </div>
                  ) : leftButtonTitle && !leftButtonIcon ? (
                    leftButtonTitle
                  ) : leftButtonIcon && !leftButtonTitle ? (
                    <FontAwesomeIcon
                      icon={leftButtonIcon}
                      style={
                        this.props.leftButtonIconCustomStyle
                          ? this.props.leftButtonIconCustomStyle
                          : styles.icon
                      }
                    />
                  ) : (
                    ""
                  )
                }
                action={() => this.leftButtonCloseAndAction()}
                customStyle={
                  this.props.leftButtonCustomStyle
                    ? { ...styles.leftButton, ...this.props.leftButtonCustomStyle }
                    : styles.leftButton
                }
                buttonTheme={this.props.leftButtonType}
                ariaLabel={leftButtonIcon ? leftButtonLabel : leftButtonTitle}
                disabled={leftButtonState}
              />
            )}
            {rightButtonType && rightButtonType !== BUTTON_TYPE.none && (
              <CustomButton
                buttonId={"unit-test-right-btn"}
                label={
                  rightButtonTitle && rightButtonIcon ? (
                    <div>
                      <FontAwesomeIcon
                        icon={rightButtonIcon}
                        style={
                          this.props.rightButtonIconCustomStyle
                            ? this.props.rightButtonIconCustomStyle
                            : styles.icon
                        }
                      />
                      <span style={styles.buttonLabel}>{rightButtonTitle}</span>
                    </div>
                  ) : rightButtonTitle && !rightButtonIcon ? (
                    rightButtonTitle
                  ) : rightButtonIcon && !rightButtonTitle ? (
                    <FontAwesomeIcon
                      icon={rightButtonIcon}
                      style={
                        this.props.rightButtonIconCustomStyle
                          ? this.props.rightButtonIconCustomStyle
                          : styles.icon
                      }
                    />
                  ) : (
                    ""
                  )
                }
                action={() => this.rightButtonCloseAndAction()}
                customStyle={
                  this.props.rightButtonCustomStyle
                    ? { ...styles.rightButton, ...this.props.rightButtonCustomStyle }
                    : styles.rightButton
                }
                buttonTheme={this.props.rightButtonType}
                ariaLabel={rightButtonIcon ? rightButtonLabel : rightButtonTitle}
                disabled={rightButtonState}
              />
            )}
          </Modal.Footer>
        </div>
      </Modal>
    );
  }
}

export { PopupBox as UnconnectedPopupBox };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(PopupBox);
