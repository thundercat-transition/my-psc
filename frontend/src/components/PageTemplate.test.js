import React from "react";
import { shallow } from "enzyme";

// Components
import { PageTemplate } from "./PageTemplate";
import Header from "./header/Header";
import FooterLinks from "./footer/FooterLinks";

describe("Page Template component", () => {
  it("should render", () => {
    const wrapper = shallow(
      <PageTemplate>
        <p>Page Content</p>
      </PageTemplate>
    );
    expect(wrapper.exists()).toBe(true);
  });

  it("should have a footer links component", () => {
    const wrapper = shallow(
      <PageTemplate>
        <p>Page Content</p>
      </PageTemplate>
    );
    expect(wrapper.find(FooterLinks)).toHaveLength(1);
  });

  it("should have a header component", () => {
    const wrapper = shallow(
      <PageTemplate>
        <p>Page Content</p>
      </PageTemplate>
    );
    expect(wrapper.find(Header)).toHaveLength(1);
  });
});
