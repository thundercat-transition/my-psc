/**
 * This file individually imports each possible image to render for the Splash page
 */
import SplashImage1 from "../images/splash/sp-bg-1.jpg";
import SplashImage2 from "../images/splash/sp-bg-2.jpg";
import SplashImage3 from "../images/splash/sp-bg-3.jpg";
import SplashImage4 from "../images/splash/sp-bg-4.jpg";
import SplashImage5 from "../images/splash/sp-bg-5.jpg";
import SplashImage6 from "../images/splash/sp-bg-6.jpg";
import SplashImage7 from "../images/splash/sp-bg-7.jpg";
import SplashImage8 from "../images/splash/sp-bg-8.jpg";
import SplashImage9 from "../images/splash/sp-bg-9.jpg";
import SplashImage10 from "../images/splash/sp-bg-10.jpg";
import SplashImage11 from "../images/splash/sp-bg-11.jpg";

const SplashPageImages = [
  SplashImage1,
  SplashImage2,
  SplashImage3,
  SplashImage4,
  SplashImage5,
  SplashImage6,
  SplashImage7,
  SplashImage8,
  SplashImage9,
  SplashImage10,
  SplashImage11
];

export default SplashPageImages;
