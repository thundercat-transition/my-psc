import React, { Component } from "react";
import PropTypes from "prop-types";

import { Row, Col } from "react-bootstrap";
import LOCALIZE from "../resources/textResources";

const styles = {
  box: {
    border: "1px solid green",
    borderRadius: "5px",
    padding: 25
  },
  image: {},
  infoLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  paragraph: {
    paddingBottom: "15px",
    overflow: "wrap"
  }
};

class PSCAppBox extends Component {
  static propTypes = {
    image: PropTypes.string.isRequired, // image.isRequired
    appName: PropTypes.string.isRequired,
    buttonAction: PropTypes.func.isRequired,
    detailText: PropTypes.string.isRequired
  };

  state = {
    currentlyLoading: false
  };

  render() {
    const { image, appName, buttonAction, detailText } = this.props;

    return (
      <div role="form">
        <div style={styles.box}>
          <Col>
            <Row>
              <p style={styles.paragraph}>{image}</p>
            </Row>
            <Row>
              <button
                aria-label={LOCALIZE.homePage.moreInfo} // TODO fix this
                tabIndex="0"
                onClick={buttonAction}
                style={styles.infoLink}
              >
                {appName}
              </button>
            </Row>
            <Row>
              <p style={styles.paragraph}>{detailText}</p>
            </Row>
          </Col>
        </div>
      </div>
    );
  }
}

export default PSCAppBox;
