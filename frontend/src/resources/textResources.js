import LocalizedStrings from "react-localization";

const LOCALIZE = new LocalizedStrings({
  // ======================
  // ENGLISH
  // ======================
  en: {
    navigation: {
      nextButton: "Next ››",
      nextButtonLabel: "Next",
      previousButton: "‹‹ Previous",
      previousButtonLabel: "Previous",
      startButton: "Start",
      finishButton: "Finish",
      newActivityButton: "New Activity",
      questionnaireButton: "Questionnaire",

      a11y: {
        skipToMain: "Skip to main content",
        skipToFooter: `Skip to "About government"`
      }
    },

    titles: {
      status: "MyPSC - System Status"
    },

    language: {
      otherLanguage: "Français",
      otherLanguageCode: "fr"
    },

    alerts: {
      errorAlt: "Error",
      message: "Error message here"
    },

    timeoutPopup: {
      title: "Session Expired",
      description:
        "Your session has expired due to inactivity. You will now be redirected to the home page.",
      closeButton: "Close"
    },

    pageDetails: {
      dateModifiedText: "Date modified: "
    },

    header: {
      otherLanguage: "Français",
      otherLanguageCode: "fr",
      canadaHomeLink: "https://www.canada.ca/en.html",
      canadaHomeText: "Government of Canada",

      langSelectSection: "Language selection",

      menu: [
        ["Jobs and the workplace", "https://www.canada.ca/en/services/jobs.html"],
        [
          "Immigration and citizenship",
          "https://www.canada.ca/en/services/immigration-citizenship.html"
        ],
        ["Travel and tourism", "https://travel.gc.ca/"],
        ["Business and industry", "https://www.canada.ca/en/services/business.html"],
        ["Benefits", "https://www.canada.ca/en/services/benefits.html"],
        ["Health", "https://www.canada.ca/en/services/health.html"],
        ["Taxes", "https://www.canada.ca/en/services/taxes.html"],
        ["Environment and natural resources", "https://www.canada.ca/en/services/environment.html"],
        ["National security and defence", "https://www.canada.ca/en/services/defence.html"],
        ["Culture, history and sport", "https://www.canada.ca/en/services/culture.html"],
        ["Policing, justice and emergencies", "https://www.canada.ca/en/services/policing.html"],
        ["Transport and infrastructure", "https://www.canada.ca/en/services/transport.html"],
        ["Canada and the world", "https://international.gc.ca/world-monde/index.aspx?lang=eng"],
        ["Money and finances", "https://www.canada.ca/en/services/finance.html"],
        ["Science and innovation", "https://www.canada.ca/en/services/science.html"]
      ],

      loginState: {
        loginText: "Log in",
        logoutText: "Log out",
        usernamePrefix: "Logged in as:"
      }
    },

    footer: {
      h2: "About government",
      feedback: "Feedback",
      feedbackLink: "https://www1.canada.ca/en/contact/feedback.html",
      socialMedia: "Social media",
      socialMediaLink: "https://www.canada.ca/en/social.html",
      mobile: "Mobile applications",
      mobileLink: "https://www.canada.ca/en/mobile.html",
      about: "About Canada.ca",
      aboutLink: "https://www1.canada.ca/en/newsite.html",
      terms: "Terms and conditions",
      termsLink: "https://www.canada.ca/en/transparency/terms.html",
      privacy: "Privacy",
      privacyLink: "https://www.canada.ca/en/transparency/privacy.html",
      imageAltText: "Symbol of the Government of Canada",

      aboutSite: "About this site",
      contactLink: "https://www.canada.ca/en/contact.html",
      contact: "Contact us",
      depLink: "https://www.canada.ca/en/government/dept.html",
      dep: "Departments and agencies",
      publicServiceLink: "https://www.canada.ca/en/government/publicservice.html",
      publicService: "Public service and military",
      newsLink: "http://news.gc.ca/web/index-en.do",
      news: "News",
      lawsLink: "https://www.canada.ca/en/government/system/laws.html",
      laws: "Treaties, laws and regulations",
      reportLink: "https://www.canada.ca/en/transparency/reporting.html",
      report: "Government-wide reporting",
      primeMinisterLink: "http://pm.gc.ca/en",
      primeMinister: "Prime Minister",
      systemLink: "https://www.canada.ca/en/government/system.html",
      system: "About government",
      openGovLink: "http://open.canada.ca/en",
      openGov: "Open government",

      pageUp: "Top of page",

      reportIssue: {
        buttonLink: "https://www.canada.ca/en/report-problem.html",
        buttonText: "Report a problem or mistake on this page"
      },

      shareButton: "Share this page",

      sharePanel: {
        header: "Share this page",
        bitly: "",
        blogger: "",
        digg: "",
        diigo: "",
        email: "",
        emailLabel: "Email",
        facebook: "",
        gmail: "",
        linkedin: "",
        myspace: "",
        pinterest: "",
        reddit: "",
        tumblr: "",
        twitter: "",
        yahoomail: "",
        bottomText: "No endorsement of any products or services is expressed or implied.",
        closePanel: "Close",
        closePanelHover1: "Close overlay",
        closePanelHover2: "Close overlay (escape key)"
      }
    },

    splashPage: {
      title: "Canada.ca",
      langSelectEnglish: "English",
      langSelectFrench: "Français",
      termsEnglish: "Terms\u00A0& Conditions", // The unicode character here is a non-breaking space (&nbsp). This is needed to keep the first 2 words together
      termsFrench: "Avis",
      termsLinkEnglish: "https://www.canada.ca/en/transparency/terms.html",
      termsLinkFrench: "https://www.canada.ca/fr/transparence/avis.html"
    },

    homePage: {
      title: "Welcome to My GC Career",
      paragraph1:
        "This website is used to allow Canadian Citizens access to the Government of Canada Job's site, the Candidate Assessment Tool, and other online applications created and maintained by the Public Service Commission of Canada. We have partnered with Verfied.me and MyGCKey to Streamline your user experience across our applications.",
      paragraph2:
        "Please note that you will leave this website to access the partner login portal and return afterwards.",
      moreInfo: "Need more information?",
      verfiedMe: {
        button: "Verfied.me Login",
        explaination: "Click here to login in through your preferred financial institution."
      },
      gcKey: {
        button: "My GC Key Login",
        explaination: "Click here to login with your GCKey User ID and Password"
      }
    },

    registrationForm: {
      title: "Create an account",
      content: {
        title: "Create an account",
        description:
          "Your MyCareer account will be used to create and link all the other PSC services together.",
        inputs: {
          valid: "Valid",
          firstNameTitle: "First Name:",
          firstNameError: "Must be a valid First Name",
          lastNameTitle: "Last Name:",
          lastNameError: "Must be a valid Last Name",
          dobTitle: "Date of Birth ( DD/ MM / YYYY):",
          dobError: "Must be a valid Date",
          dayTitle: "Day",
          monthTitle: "Month",
          yearTitle: "Year",
          emailTitle: "Email Address:",
          emailError: "Must be a valid Email Address",
          optionalTitle: "Optional",
          priTitle: "Personal Record Identifier (PRI)",
          priError: "Must be a valid PRI",
          prNbrTitle: "Priority Reference Number (PRN)",
          prNbrError: "Must be a valid PRN",
          serviceTitle: "Service Number (Canadian Armed Forces)",
          serviceError: "Must be a valid Service Number"
        }
      },
      privacyNotice:
        "I have read the {0} and I accept the manner in which the Public Service Commission collects, uses and discloses personal information.",
      privacyNoticeLink: "Privacy Notice",
      privacyNoticeError: "You must accept the privacy notice by clicking on the checkbox",
      button: "Create account",
      accountAlreadyExistsError: "An account is already associated to this email address"
    },

    // Dashboard Page
    dashboard: {
      title: "Welcome, {0} {1}.",
      lastLogin: "[Last Logged In: {0}]",
      lastLoginNever: "never",
      myTools: {
        title: "My Tools",
        description: "To access your PSC tools, please click on one of the boxes below.",
        cat: {
          title: "Candidate Assessment Tool",
          description:
            "Your online portal to supervised and unsupervised tests, including the Second Language Evaluations."
        },
        psrs: {
          title: "Government of Canada Jobs",
          description: "Opportunities to work for the Canadian Federal Government"
        },
        pims: {
          title: "Priority Information Management System",
          description: "Apply for priority entitlement when seeing a position."
        }
      },
      myTests: {
        title: "My Tests",
        description:
          "Here you can see the tests you have taken, when you can take them again and your results.",
        table: {
          nameOfTest: "Test",
          testDate: "Test Date",
          score: "Result",
          results: "Action",
          viewResultsButton: "View",
          viewResultsButtonAriaLabel:
            "View results of the {0} test, completed on {1}, where a retest is possible as of {2} and the score is {3}.",
          viewResultsPopup: {
            title: "Test Result",
            testLabel: "Test:",
            testDescriptionLabel: "Test Description:",
            testDateLabel: "Test Date:",
            scoreLabel: "Result:"
          },
          noDataMessage: "No tests have been scored yet."
        }
      }
    },

    // DatePicker Component
    datePicker: {
      dayField: "Day",
      dayFieldSelected: "Day field selected",
      monthField: "Month",
      monthFieldSelected: "Month field selected",
      yearField: "Year",
      yearFieldSelected: "Year field selected",
      hourField: "Hour",
      hourFieldSelected: "Hour field selected",
      minuteField: "Minute",
      minuteFieldSelected: "Minute field selected",
      currentValue: "The current value is:",
      none: "Select",
      datePickedError: "Must be a valid Date",
      futureDatePickedError: "Must be a future date",
      futureDatePickedIncludingTodayError: "Cannot be earlier than today's date",
      comboStartEndDatesPickedError: "Start date must be before end date",
      comboFromToDatesPickedError: "From date must be before To date"
    },

    // PrivacyNoticeStatement Component
    privacyNoticeStatement: {
      title: "Privacy Notice Statement",
      descriptionTitle: "Privacy Notice Statement",
      paragraph1:
        "Personal information is used to provide assessment services to clients of the Personnel Psychology Centre. It is collected for staffing related purposes, in accordance with sections 11, 30 and 36 of the {0}. For organizations not subject to this act, personal information is collected under the authority of that organization’s enabling statute and section 35 of the {1}. Second language evaluation results will be disclosed to authorized organizational officials. Results of all other tests will be disclosed only to the requesting organization. Test results may be disclosed to the Investigations Directorate of the Public Service Commission if an investigation is conducted pursuant to section 66 or 69 of the {2}. Your information may also be used for statistical and analytical research purposes. In some cases, information may be disclosed without your consent, pursuant to section 8(2) of the {3}. Provision of your personal information is voluntary, but if you choose not to provide your information, you may not be able to receive Personnel Psychology Centre services.",
      paragraph2:
        "The information is collected and used as described in the Personnel Psychology Centre’s (PSC PCU 025) PIB, found in the Public Service Commission’s {0}.",
      paragraph3:
        "You have the right to access and correct your personal information, and to request corrections where you believe there is an error or omission. You also have the right to file a complaint to the {0} regarding the handling of your personal information.",
      publicServiceEmploymentActLinkTitle: "Public Service Employment Act",
      publicServiceEmploymentActLink: "https://laws-lois.justice.gc.ca/eng/acts/p-33.01/",
      privacyActLinkTitle: "PA",
      privacyActLink: "https://laws-lois.justice.gc.ca/eng/acts/p-21/",
      infoSourceLinkTitle: "Info Source",
      infoSourceLink:
        "https://www.canada.ca/en/public-service-commission/corporate/about-us/access-information-privacy-office/info-source-sources-federal-government-employee-information.html#122",
      privacyCommissionerOfCanadaLinkTitle: "Privacy Commissioner of Canada",
      privacyCommissionerOfCanadaLink: "https://www.priv.gc.ca/en/"
    },

    // DropdownSelect Component
    dropdownSelect: {
      pleaseSelect: "Select",
      noOptions: "No Options"
    },

    examplePage: {
      title: "MyPSC- Example",
      content1:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at ipsum vitae diam aliquet ornare. Vestibulum non diam lacinia, scelerisque augue fringilla, convallis erat. Proin congue magna sit amet nisi pulvinar, sed hendrerit leo sagittis. Cras viverra sem non tincidunt maximus. Phasellus molestie purus vel mollis gravida. Morbi lacinia quam vitae enim commodo, sit amet rutrum neque euismod. Donec nec velit tortor. Curabitur in dolor laoreet arcu vestibulum semper ac nec eros. Cras fermentum lacus in molestie gravida."
    },

    loginPage: {
      title: "MyPSC - Login",
      usernameHeader: "Username",
      passwordHeader: "Password",
      passwordAriaLabel: "Password field selected.",
      loginButton: "Login",
      loginError: "Login error, please try again."
    },

    adminPage: {
      title: "MyPSC - Admin",
      content1: "Congratulations! You are now logged in."
    },

    error404page: {
      title: "Error 404--Not Found",
      subtitle: "From RFC 2068 Hypertext Transfer Protocol -- HTTP/1.1:",
      subtitle2: "10.4.5 404 Not Found",
      content1:
        "The server has not found anything matching the Request-URI. No indication is given of whether the condition is temporary or permanent.",
      content2:
        "If the server does not wish to make this information available to the client, the statuscode 403 (Forbidden) can be used instead. The 410 (Gone) status code SHOULD be used if the server knows, through some internally configurable mechanism, that an old resource is permanently unavailable and has no forwarding address."
    },

    unexpectedErrorPage: {
      title: "Unexpected Error",
      subtitle: "We apologize for the inconvenience",
      content1: "An unexpected error has occurred, and our Administrators have been alerted.",
      content2: "Please navigate to a valid web page in the meantime."
    },
    // Status Page
    statusPage: {
      title: "MyPSC Status",
      logo: "MyPSC Logo",
      welcomeMsg:
        "Internal status page to quickly determine the status / health of the My PSC Tool.",
      versionMsg: "My PSC Tool Version: ",
      gitHubRepoBtn: "GitHub Repository",
      serviceStatusTable: {
        title: "Service Status",
        frontendDesc: "Front end application built and serving successfully",
        backendDesc: "Back end application completing API requests successfully",
        databaseDesc: "Database completing API requests sccessfully"
      },
      systemStatusTable: {
        title: "System Status",
        javaScript: "JavaScript",
        browsers: "Chrome, Firefox, Edge",
        screenResolution: "Screen resolution minimum of 1024 x 768"
      },
      additionalRequirements:
        "Additionally, the following requirements must be met to use this application in a test centre.",
      secureSockets: "Secure Socket Layer (SSL) encryption enabled",
      fullScreen: "Full-screen mode enabled",
      copyPaste: "Copy + paste functionality enabled"
    },

    // Commons
    commons: {
      enabled: "Enabled",
      disabled: "Disabled",
      close: "Close",
      status: {
        checkedIn: "Checked-in",
        ready: "Ready",
        preTest: "Pre-Test",
        active: "Testing",
        locked: "Locked",
        paused: "Paused",
        neverStarted: "Not Started Yet",
        inactivity: "Inactivity",
        timedOut: "Timed Out",
        quit: "Quit",
        submitted: "Submitted",
        unassigned: "Unassigned",
        invalid: "Invalid"
      }
    }
  },

  // ======================
  // FRENCH
  // ======================
  fr: {
    navigation: {
      nextButton: "Suivant ››",
      nextButtonLabel: "Suivant",
      previousButton: "‹‹ Précédent",
      previousButtonLabel: "Précédent",
      startButton: "Débutez",
      finishButton: "Terminer",
      newActivityButton: "Nouvelle activité",
      questionnaireButton: "Questionnaire",

      a11y: {
        skipToMain: "Passer au contenu principal",
        skipToFooter: "Passer à « Au sujet du gouvernement »"
      }
    },

    titles: {
      status: "Mon CFP - System Status"
    },

    language: {
      otherLanguage: "English",
      otherLanguageCode: "en"
    },

    alerts: {
      errorAlt: "Erreur",
      message: "Nom d'erreur ici"
    },

    timeoutPopup: {
      title: "Session Expirée",
      description:
        "Votre session a expiré en raison de l'inactivité. Vous allez maintenant être redirigé à la page d'accueil.",
      closeButton: "Fermer"
    },

    pageDetails: {
      dateModifiedText: "Date de modification : "
    },

    header: {
      otherLanguage: "English",
      otherLanguageCode: "en",
      canadaHomeLink: "https://www.canada.ca/fr.html",
      canadaHomeText: "Gouvernement du Canada",

      langSelectSection: "Sélection de la langue",

      menu: [
        ["Emplois et milieu de travail", "https://www.canada.ca/fr/services/emplois.html"],
        [
          "Immigration et citoyenneté",
          "https://www.canada.ca/fr/services/immigration-citoyennete.html"
        ],
        ["Voyage et tourisme", "https://voyage.gc.ca/"],
        ["Entreprises et industrie", "https://www.canada.ca/fr/services/entreprises.html"],
        ["Prestations", "https://www.canada.ca/fr/services/prestations.html"],
        ["Santé", "https://www.canada.ca/fr/services/sante.html"],
        ["Impôts", "https://www.canada.ca/fr/services/impots.html"],
        [
          "Environnement et ressources naturelles",
          "https://www.canada.ca/fr/services/environnement.html"
        ],
        ["Sécurité nationale et défense", "https://www.canada.ca/fr/services/defense.html"],
        ["Culture, histoire et sport", "https://www.canada.ca/fr/services/culture.html"],
        [
          "Services de police, justice et urgences",
          "https://www.canada.ca/fr/services/police.html"
        ],
        ["Transport et infrastructure", "https://www.canada.ca/fr/services/transport.html"],
        ["Canada et le monde", "https://international.gc.ca/world-monde/index.aspx?lang=fra"],
        ["Argent et finances", "https://www.canada.ca/fr/services/finance.html"],
        ["Science et innovation", "https://www.canada.ca/fr/services/science.html"]
      ],

      loginState: {
        loginText: "Connexion",
        logoutText: "Déconnexion",
        usernamePrefix: "Connecté en tant que :"
      }
    },

    footer: {
      h2: "Au sujet du gouvernement",
      feedback: "Rétroaction",
      feedbackLink: "https://www1.canada.ca/fr/contact/retroaction.html",
      socialMedia: "Médias sociaux",
      socialMediaLink: "https://www.canada.ca/fr/sociaux.html",
      mobile: "Applications mobiles",
      mobileLink: "https://www.canada.ca/fr/mobile.html",
      about: "À propos de Canada.ca",
      aboutLink: "https://www1.canada.ca/fr/nouveausite.html",
      terms: "Avis",
      termsLink: "https://www.canada.ca/fr/transparence/avis.html",
      privacy: "Confidentialité",
      privacyLink: "https://www.canada.ca/fr/transparence/confidentialite.html",
      imageAltText: "Symbole du gouvernement du Canada",

      aboutSite: "À propos de ce site",
      contactLink: "https://www.canada.ca/fr/contact.html",
      contact: "Contactez-nous",
      depLink: "https://www.canada.ca/fr/gouvernement/min.html",
      dep: "Ministères et organismes",
      publicServiceLink: "https://www.canada.ca/fr/gouvernement/fonctionpublique.html",
      publicService: "Fonction publique et force militaire",
      newsLink: "https://www.canada.ca/fr/nouvelles.html",
      news: "Nouvelles",
      lawsLink: "https://www.canada.ca/fr/gouvernement/systeme/lois.html",
      laws: "Traités, lois et règlements",
      reportLink: "https://www.canada.ca/fr/transparence/rapports.html",
      report: "Rapports à l'échelle du gouvernement",
      primeMinisterLink: "http://pm.gc.ca/fr",
      primeMinister: "Premier ministre",
      systemLink: "https://www.canada.ca/fr/gouvernement/systeme.html",
      system: "À propos du gouvernement",
      openGovLink: "http://ouvert.canada.ca/",
      openGov: "Gouvernement ouvert",

      pageUp: "Haut de la page",

      reportIssue: {
        buttonLink: "https://www.canada.ca/fr/signaler-probleme.html",
        buttonText: "Signaler un problème ou une erreur sur cette page"
      },

      shareButton: "Partagez cette page",

      sharePanel: {
        header: "Partagez cette page",
        bitly: "",
        blogger: "",
        digg: "",
        diigo: "",
        email: "",
        emailLabel: "Courriel",
        facebook: "",
        gmail: "",
        linkedin: "",
        myspace: "",
        pinterest: "",
        reddit: "",
        tumblr: "",
        twitter: "",
        yahoomail: "",
        bottomText:
          "Aucun appui n’est accordé, soit de façon expresse ou tacite, à aucun produit ou service.",
        closePanel: "Fermer",
        closePanelHover1: "Fermer la fenêtre superposée",
        closePanelHover2: "Fermer la fenêtre superposée (touche d'échappement)"
      }
    },

    splashPage: {
      title: "Canada.ca",
      langSelectEnglish: "English",
      langSelectFrench: "Français",
      termsEnglish: "Terms\u00A0& Conditions", // The unicode character here is a non-breaking space (&nbsp). This is needed to keep the first 2 words together
      termsFrench: "Avis",
      termsLinkEnglish: "https://www.canada.ca/en/transparency/terms.html",
      termsLinkFrench: "https://www.canada.ca/fr/transparence/avis.html"
    },

    homePage: {
      title: "FR Bienvenue a Ma GC Carrière",
      paragraph1:
        "FR This website is used to allow Canadian Citizens access to the Government of Canada Job's site, the Candidate Assessment Tool, and other online applications created and maintained by the Public Service Commission of Canada. We have partnered with Verfied.me and MyGCKey to Streamline your user experience across our applications.",
      paragraph2:
        "FR Please note that you will leave this website to access the partner login portal and return afterwards.",
      moreInfo: "FR Need more information?",
      verfiedMe: {
        button: "FR Verfied.me Login",
        explaination: "FR Click here to login in through your preferred financial institution."
      },
      gcKey: {
        button: "FR My GC Key Login",
        explaination: "FR Click here to login with your GCKey User ID and Password"
      }
    },

    registrationForm: {
      title: "FR Create an account",
      content: {
        title: "FR Create an account",
        description:
          "FR Your MyCareer account will be used to create and link all the other PSC services together.",
        inputs: {
          valid: "FR Valid",
          firstNameTitle: "FR First Name:",
          firstNameError: "FR Must be a valid First Name",
          lastNameTitle: "FR Last Name:",
          lastNameError: "FR Must be a valid Last Name",
          dobTitle: "FR Date of Birth ( DD/ MM / YYYY):",
          dobError: "FR Must be a valid Date",
          dayTitle: "FR Day",
          monthTitle: "FR Month",
          yearTitle: "FR Year",
          emailTitle: "FR Email Address:",
          emailError: "FR Must be a valid Email Address",
          optionalTitle: "FR Optional",
          priTitle: "FR Personal Record Identifier (PRI)",
          priError: "FR Must be a valid PRI",
          prNbrTitle: "FR Priority Reference Number (PRN)",
          prNbrError: "FR Must be a valid PRN",
          serviceTitle: "SFR ervice Number (Canadian Armed Forces)",
          serviceError: "FR Must be a valid Service Number"
        }
      },

      // Dashboard Page
      dashboard: {
        title: "Bienvenue {0} {1}.",
        lastLogin: "[Date de la dernière ouverture de session\u00A0: {0}]",
        lastLoginNever: "jamais",
        myTools: {
          title: "FR My Tools",
          description: "FR To access your PSC tools, please click on one of the boxes below.",
          cat: {
            title: "FR Candidate Assessment Tool",
            description:
              "FR Your online portal to supervised and unsupervised tests, including the Second Language Evaluations."
          },
          psrs: {
            title: "FR Government of Canada Jobs",
            description: "FR Opportunities to work for the Canadian Federal Government"
          },
          pims: {
            title: "FR riority Information Management System",
            description: "FR Apply for priority entitlement when seeing a position."
          }
        },
        myTests: {
          title: "Bienvenue {0} {1}.",
          alertCatTestsOnly1:
            "Vous trouverez ci-après vos résultats à tous les tests que vous avez passés en utilisant l’Outil d’évaluation des candidats. Vos résultats à d’autres types de tests ne sont pas inclus.",
          alertCatTestsOnly2:
            "Ces résultats sont valides lorsque les conditions ont été respectées, notamment le respect du délai d’attente avant de reprendre un test.",
          alertCatTestsOnly3:
            "Vos résultats aux tests en ligne non supervisés pour les examens de compréhension de l’écrit et d’expression écrite passés depuis le 1er\u00A0septembre\u00A02022 sont valides pendant 5\u00A0ans et sont transférables à tous les ministères et organismes fédéraux.",
          table: {
            nameOfTest: "Test",
            testDate: "Date du test",
            score: "Résultat",
            results: "Action",
            viewResultsButton: "Afficher",
            viewResultsButtonAriaLabel:
              "Afficher les résultats du test {0}, passé le {1}, reprise possible à compter du {2}, et votre résultat est {3}.",
            viewResultsPopup: {
              title: "Résultat du test",
              testLabel: "Test\u00A0:",
              testDescriptionLabel: "Description du test\u00A0:",
              testDateLabel: "Date du test\u00A0:",
              scoreLabel: "Résultat\u00A0:"
            },
            noDataMessage: "Aucun test n'a encore été corrigé."
          }
        }
      },

      // DatePicker Component
      datePicker: {
        dayField: "Jour",
        dayFieldSelected: "Champ Jour sélectionné",
        monthField: "Mois",
        monthFieldSelected: "Champ Mois sélectionné",
        yearField: "Année",
        yearFieldSelected: "Champ Année sélectionné",
        hourField: "Heure",
        hourFieldSelected: "Champ Heure sélectionné",
        minuteField: "Minute",
        minuteFieldSelected: "Champ Minute sélectionné",
        currentValue: "La valeur actuelle est :",
        none: "Sélectionnez",
        datePickedError: "Doit être une date valide",
        futureDatePickedError: "Doit être une date ultérieure",
        futureDatePickedIncludingTodayError: "Ne peut être une date antérieure à aujourd'hui",
        comboStartEndDatesPickedError: "La date de début doit être antérieure à la date de fin",
        comboFromToDatesPickedError: "La date de début doit être antérieure à la date de fin"
      },

      // PrivacyNoticeStatement Component
      privacyNoticeStatement: {
        title: "Énoncé de confidentialité",
        descriptionTitle: "Énoncé de confidentialité",
        paragraph1:
          "Les renseignements personnels servent à fournir des services d’évaluation aux clients du Centre de psychologie du personnel. Ils sont recueillis à des fins de dotation, conformément aux articles 11, 30 et 36 de la {0}. Dans le cas des organisations qui ne sont pas assujetties à cette loi, les renseignements personnels sont recueillis en vertu de la loi habilitante de l’organisation concernée ainsi que de l’article 35 de la {1}. Les résultats aux tests d’évaluation de langue seconde seront divulgués aux responsables de l’organisation autorisés. Les résultats obtenus à tous les autres examens seront communiqués exclusivement à l’organisation qui en a fait la demande. Les résultats obtenus aux examens pourraient être communiqués à la Direction des enquêtes de la Commission de la fonction publique si une enquête a lieu en vertu des articles 66 ou 69 de la {2}. Vos renseignements pourraient également être utilisés à des fins de recherche statistique et analytique. En vertu du paragraphe 8(2) de la {3}, des renseignements pourraient, dans certains cas, être divulgués sans votre autorisation. La communication de vos renseignements personnels est volontaire, mais si vous choisissez de ne pas fournir ces renseignements, il se peut que vous ne puissiez pas recevoir les services du Centre de psychologie du personnel.",
        paragraph2:
          "Les renseignements sont recueillis et utilisés de la façon décrite dans le fichier de renseignements personnels du Centre de psychologie du personnel (CFP PCU 025), qui se trouve dans {0} de la Commission de la fonction publique.",
        paragraph3:
          "Vous avez le droit d’accéder à vos renseignements personnels et de les corriger, ainsi que de demander qu’ils soient modifiés si vous croyez qu’ils sont erronés ou incomplets. Vous avez également le droit de porter plainte auprès du {0} au sujet du traitement de vos renseignements personnels.",
        publicServiceEmploymentActLinkTitle: "Loi sur l’emploi dans la fonction publique",
        publicServiceEmploymentActLink: "https://laws-lois.justice.gc.ca/fra/lois/p-33.01/",
        privacyActLinkTitle: "Loi sur la protection des renseignements personnels",
        privacyActLink: "https://laws-lois.justice.gc.ca/fra/lois/p-21/",
        infoSourceLinkTitle: "l’Info Source",
        infoSourceLink:
          "https://www.canada.ca/fr/commission-fonction-publique/organisation/propos-nous/bureau-acces-information-protection-renseignements-personnels/info-source.html#122",
        privacyCommissionerOfCanadaLinkTitle:
          "Commissaire à la protection de la vie privée du Canada",
        privacyCommissionerOfCanadaLink: "https://www.priv.gc.ca/fr/"
      },

      // DropdownSelect Component
      dropdownSelect: {
        pleaseSelect: "Sélectionnez",
        noOptions: "Aucune option"
      },

      privacyNotice:
        "I have read the {0} and I accept the manner in which the Public Service Commission collects, uses and discloses personal information.",
      privacyNoticeLink: "Privacy Notice",
      privacyNoticeError: "You must accept the privacy notice by clicking on the checkbox",
      button: "Create account",
      accountAlreadyExistsError: "An account is already associated to this email address"
    },

    examplePage: {
      title: "Mon CFP - Exemple",
      content1:
        "Praesent semper nibh eget libero dapibus, id blandit ligula dictum. Suspendisse condimentum, massa ut blandit malesuada, elit tellus sagittis diam, accumsan porta tellus quam vitae lorem. Suspendisse magna massa, lacinia sit amet sagittis in, elementum vel felis. Aenean varius mollis libero. Praesent pharetra nisi id enim varius pharetra. Etiam dignissim, magna in sollicitudin viverra, libero dolor blandit metus, molestie posuere urna elit posuere lectus."
    },

    loginPage: {
      title: "Mon CFP - Login",
      usernameHeader: "Nom d'utilisateur",
      passwordHeader: "Mot de passe",
      passwordAriaLabel: "Champ de mot de passe sélectionné.",
      loginButton: "Connexion",
      loginError: "Erreur de connexion, veuillez réessayer."
    },

    adminPage: {
      title: "Mon CFP - Admin",
      content1: "Félicitations! Vous êtes maintenant connecté."
    },

    error404page: {
      title: "Erreur 404 - Page non trouvée",
      subtitle: "De RFC 2068 Hypertext Transfer Protocol -- HTTP/1.1:",
      subtitle2: "10.4.5 404 introuvable",
      content1:
        "Le serveur n'a rien trouvé correspondant à l'URI demandée. Aucune indication n'est donnée quant à savoir si la condition est temporaire ou permanente.",
      content2:
        "Si le serveur ne souhaite pas rendre cette information disponible au client, le code d'état 403 (Interdit) peut être utilisé à la place. Le code d'état 410 (Parti) DEVRAIT être utilisé si le serveur sait, via un mécanisme interne configurable, qu'une ancienne ressource est indisponible en permanence et qu'il n'y a pas d'adresse de redirection."
    },

    unexpectedErrorPage: {
      title: "Erreur Inattendue",
      subtitle: "Nous nous excusons pour tout inconvénient",
      content1: "Une erreur inattendue est survenue et nos Administrateurs ont été alertés.",
      content2: "Veuillez s'il vous plaît vous rendre à une page web valide entre-temps."
    }
  },
  // Status Page
  statusPage: {
    title: "Statut de OÉC",
    logo: "Logo Mon CFP",
    welcomeMsg:
      "Page de statut interne afin de déterminer rapidement l'état / la santé de l'outil de mon PCFP.",
    versionMsg: "Version de l'Outil de mon CFP: ",
    gitHubRepoBtn: "Répertoire GitHub",
    serviceStatusTable: {
      title: "Statut des services",
      frontendDesc: "La Face avant de l'application est construite et utilisée avec succès",
      backendDesc: "La Face arrière de l'application réussit les demandes API avec succès",
      databaseDesc: "La Base de données réussit les demandes API avec succès"
    },
    systemStatusTable: {
      title: "Statut du système",
      javaScript: "JavaScript",
      browsers: "Chrome, Firefox, Edge",
      screenResolution: "Résolution d'écran minimum de 1024 x 768"
    },
    additionalRequirements:
      "De plus, les exigences suivantes doivent être respectées pour l’utilisation de cette application dans un centre de test.",
    secureSockets: "Chiffrement Secure Socket Layer (SSL) activé",
    fullScreen: "Mode plein écran activé",
    copyPaste: "Fonction copier-coller activée"
  },

  // Commons
  commons: {
    enabled: "Activé",
    disabled: "Désactivé",
    close: "Fermer",
    status: {
      checkedIn: "Enregistré",
      ready: "Prêt",
      preTest: "Pré-test",
      active: "En test",
      locked: "Verrouillé",
      paused: "En pause",
      neverStarted: "Non commencé",
      inactivity: "Inactivité",
      timedOut: "Temps écoulé",
      quit: "Quitté",
      submitted: "Soumis",
      unassigned: "Non attribué",
      invalid: "Invalide"
    }
  }
});

export default LOCALIZE;
