/**
 * This file contains the important & centralized constants used throughout the application
 */

/**
 * Navigation paths/URLs
 */
// eslint-disable-next-line import/prefer-default-export
export const PATH = {
  splash: "/",
  home: "/home",
  registrationForm: "/registration-form",
  dashboard: "/dashboard",
  editProfile: "/profile",
  // TODO check and remove the ones we don't need
  example: "/ex",
  login: "/login",
  admin: "/admin",
  status: "/status"
};
