// ========================================================================================
// This file contains helper functions to handle JWTs
// ========================================================================================

// Reference: SimpleJWT framework endpoints in djoser: https://djoser.readthedocs.io/en/latest/jwt_endpoints.html

/**
 * Requests the user's JWT from the API and stores it in sessionStorage
 * Returns the token if the request is successful
 * Returns null if the request is unsuccessful
 */
export const requestJWTnoAuth = async () => {
  const response = await fetch("/api/auth/jwt/create_token_no_auth/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      app_name: "my_psc_gov"
    })
  });

  // Async passes & obtains a response
  if (response.ok) {
    const data = await response.json();
    sessionStorage.setItem("auth_token_noAuth", JSON.stringify(data));
    return data;
  }
  return null;
};

/**
 * Using a user's login credentials, attempts to request a JWT from the API and stores it in sessionStorage
 * Returns the token if the request is successful
 * Returns null otherwise
 */
export const requestJWTWithCredentials = async (username, password) => {
  const response = await fetch("/api/auth/jwt/create_token/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      username,
      password
    })
  });

  // Async passes & obtains a response
  if (response.ok) {
    const data = await response.json();
    sessionStorage.setItem("auth_token", JSON.stringify(data));
    return data;
  }
  return null;
};

/**
 * Verifies if the JWT is still valid
 * Receives a storageTokenKey as a parameter
 * Returns true when receiving a successful API response, false otherwise
 */
export const verifyJWT = async storageTokenKey => {
  const authToken = JSON.parse(sessionStorage.getItem(storageTokenKey));
  const response = await fetch("/api/auth/jwt/verify_token/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ token: authToken.access })
  });

  return response.ok;
};

/**
 * Refreshes the JWT
 * Receives a storageTokenKey as a parameter
 * Returns the token when receiving a successful API response
 * Returns null otherwise
 */
export const refreshJWT = async storageTokenKey => {
  const authToken = JSON.parse(sessionStorage.getItem(storageTokenKey));
  const response = await fetch("/api/auth/jwt/refresh_token/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ refresh: authToken.refresh })
  });

  // Set sessionStorage authToken's new authToken
  if (response.ok) {
    const data = await response.json();
    sessionStorage.setItem(storageTokenKey, JSON.stringify(data));
    return data;
  }
  return null;
};

/**
 * Returns the properly formatted JWT token
 */
export const getFormattedJWTnoAuth = async () => {
  const storageTokenKey = "auth_token_noAuth";
  // Get a token if it does not already exist
  if (sessionStorage.getItem(storageTokenKey) === null) {
    await requestJWTnoAuth(storageTokenKey);
  } else {
    // If token is invalid, refresh the token
    const isValidToken = await verifyJWT(storageTokenKey);
    if (!isValidToken) {
      await refreshJWT(storageTokenKey);
    }
  }

  // Afterwards, retrieve the token
  const authToken = JSON.parse(sessionStorage.getItem(storageTokenKey));
  const formattedJWT = `Bearer ${authToken.access}`;

  // Returns the proper JWT token to be accepted
  return formattedJWT;
};

/**
 * Returns the properly formatted JWT token (assuming credentials were used)
 * Returns null if unsuccessful
 */
export const getFormattedJWT = async () => {
  const storageTokenKey = "auth_token";

  // Get a token if it does not already exist
  if (sessionStorage.getItem(storageTokenKey) === null) {
    await requestJWTWithCredentials();
  } else {
    // If token is invalid, refresh the token
    const isValidToken = await verifyJWT(storageTokenKey);
    if (!isValidToken) {
      const refreshToken = await refreshJWT(storageTokenKey);

      // Handle invalid token action by returning null & letting App.js handle it through Redux
      if (refreshToken === null) {
        return null;
      }
    }
  }

  // Afterwards, retrieve the token
  const authToken = JSON.parse(sessionStorage.getItem(storageTokenKey));
  const formattedJWT = `Bearer ${authToken.access}`;

  // Returns the proper JWT token to be accepted
  return formattedJWT;
};

export default getFormattedJWTnoAuth;
