// transforming checkboxes based on provided font size
function getCheckboxTransformScale(fontSize) {
  // converting current font size in Int
  const fontSizeInt = parseInt(fontSize.substring(0, 2));
  // converting checkbox scale based on font size
  switch (true) {
    case fontSizeInt <= 10:
      return "scale(0.75)";
    case fontSizeInt > 10 && fontSizeInt <= 14:
      return "scale(1)";
    case fontSizeInt > 14 && fontSizeInt <= 20:
      return "scale(1.4)";
    case fontSizeInt > 20 && fontSizeInt <= 30:
      return "scale(2)";
    case fontSizeInt > 30 && fontSizeInt <= 40:
      return "scale(2.5)";
    case fontSizeInt > 40:
      return "scale(2.7)";
    default:
      return "scale(1.4)";
  }
}

export default getCheckboxTransformScale;
