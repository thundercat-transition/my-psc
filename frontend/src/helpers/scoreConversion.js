import TEST_STATUS from "../components/commons/Constants";
import LOCALIZE from "../resources/textResources";

// reference: ...\backend\cms\views\utils.py
export const CONVERTED_SCORE = {
  INVALID_CONVERSION: "INVALID CONVERSION",
  PASS: "PASS",
  FAIL: "FAIL",
  NONE: "NONE"
};

function getConvertedScore(convertedScoreValue, testStatus) {
  // test status is SUBMITTED
  if (testStatus === TEST_STATUS.SUBMITTED) {
    // pass
    if (convertedScoreValue === CONVERTED_SCORE.PASS) {
      return LOCALIZE.commons.convertedScore.pass;
      // fail
    }
    if (convertedScoreValue === CONVERTED_SCORE.FAIL) {
      return LOCALIZE.commons.convertedScore.fail;
      // none
    }
    if (convertedScoreValue === CONVERTED_SCORE.NONE) {
      return LOCALIZE.commons.convertedScore.none;
      // invalid converted score
    }
    if (convertedScoreValue === null || convertedScoreValue === CONVERTED_SCORE.INVALID_CONVERSION)
      return LOCALIZE.commons.convertedScore.invalidScoreConversion;
    return convertedScoreValue;
  }
  // test status is not SUBMITTED
  return LOCALIZE.commons.convertedScore.notScored;
}

export default getConvertedScore;
