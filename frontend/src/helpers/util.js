/**
 * Miscellaneous util functions
 * @param {} inputObject
 */

/**
 * Returns if an object is empty or not
 * Reference: https://stackoverflow.com/a/59984732
 * @param {*} inputObject
 */
export const isEmpty = inputObject => {
  return Object.keys(inputObject).length === 0;
};

export default isEmpty;
