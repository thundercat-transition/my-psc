/* eslint-disable import/no-extraneous-dependencies */
import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import log from "loglevel";
import remote from "loglevel-plugin-remote";
import { remoteOptions } from "./helpers/logUtil";

// Components
import store from "./store-index";
import App from "./App";
import ErrorWrapper from "./components/commons/ErrorWrapper";

function renderApp() {
  ReactDOM.render(
    <Provider store={store}>
      <ErrorWrapper>
        <App />
      </ErrorWrapper>
    </Provider>,
    document.getElementById("root")
  );
}

if (process.env.NODE_ENV !== "production") {
  // eslint-disable-next-line global-require
  log.setDefaultLevel("warn");
} else {
  log.setDefaultLevel("error");
}

/**
 * Remote Logging
 */
remote.apply(log, remoteOptions);

renderApp();
