import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Router, Route, Switch } from "react-router-dom";

import { createBrowserHistory } from "history";
import Modal from "react-modal";

import { Helmet } from "react-helmet";

import SessionStorage, { ACTION, ITEM } from "./SessionStorage";

// CSS imports
import "bootstrap/dist/css/bootstrap.min.css";
// import "./css/theme.css";
import "./css/lib/aurora.min.css";
import "./css/my-psc-theme.css";
import "./css/create-react-app.css";

// Function/Redux imports
import { getFormattedJWT } from "./helpers/jwtUtil";
import { resetRedux } from "./redux/otherActions";

// Component imports
import RoutesList from "./RoutesList";
import PopupWindow from "./components/commons/PopupWindow";

// Constants imports
import { PATH } from "./resources/constants";
import LOCALIZE from "./resources/textResources";

const history = createBrowserHistory();

export class App extends React.Component {
  state = {
    isShowingTimeoutPopup: false,
    isLanguageSelected: SessionStorage(ACTION.GET, ITEM.MY_PSC_LANGUAGE)
  };

  componentDidMount = () => {
    Modal.setAppElement("body"); // Fixes a JavaScript error for Modal based on DOM rendering times
  };

  /**
   * Spawn the timeout popup window if the user's auth token expires
   */
  handleCurrentAuthState = async () => {
    const { authenticated } = this.props;
    if (authenticated) {
      // TODO fix this when linked properly with oauth/verfied.me/myGCKey
      // const authToken = await getFormattedJWT();
      // // Token is invalid, so log the user out by resetting Redux & display the timeout popup window
      // if (authToken === null) {
      //   this.setState({ isShowingTimeoutPopup: true });
      //   const { resetRedux } = this.props;
      //   resetRedux();
      // }
    }
  };

  /**
   * Functions to run when the timeout popup window is closed
   * Resets the Redux state when executed & redirects to Splash Page afterwards
   */
  closeTimeoutPopup = () => {
    this.setState({ isShowingTimeoutPopup: false });
    history.push(PATH.home);
  };

  render() {
    const accommodationStyles = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    const { isShowingTimeoutPopup } = this.state;
    return (
      <>
        <div onBlur={this.handleCurrentAuthState}>
          <PopupWindow
            isDisplayed={isShowingTimeoutPopup}
            handleClose={() => this.closeTimeoutPopup}
            title={LOCALIZE.timeoutPopup.title}
            description={LOCALIZE.timeoutPopup.description}
            closeButtonText={LOCALIZE.timeoutPopup.closeButton}
          />
          <Router history={history}>
            <RoutesList />
          </Router>
        </div>
      </>
    );
  }
}

App.propTypes = {
  authenticated: PropTypes.bool,
  resetRedux: PropTypes.func
};

App.defaultProps = {
  authenticated: false,
  resetRedux: () => {}
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.loginReducer.authenticated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ resetRedux }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
