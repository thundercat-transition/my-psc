from django.urls import re_path
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from django.contrib import admin
from rest_framework import routers

# ========
# Views
# ========

from user_management.views import user_personal_info
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

from backend.views import (
    views,
    sample_view,
    database_check_view,
    logs_view,
    jwt_custom_view,
)

router = routers.DefaultRouter()
router.register(r"api/database-check", database_check_view.DatabaseViewSet)

urlpatterns = [
    path(r"api/backend-status", views.index, name="index"),
    path("", include(router.urls)),
    re_path(r"^api/admin/", admin.site.urls),
    re_path(r"^api/auth/", include("djoser.urls")),
    re_path(r"^api/auth/", include("djoser.urls.authtoken")),
    re_path(r"^api/auth/", include("rest_framework.urls", namespace="rest_framework")),
    re_path(
        r"^api/auth/jwt/create_token_no_auth/",
        jwt_custom_view.CreateJWTtoken.as_view(),
        name="token_obtain_pair_noAuth",
    ),
    re_path(
        r"^api/auth/jwt/create_token/",
        TokenObtainPairView.as_view(),
        name="token_obtain_pair",
    ),
    re_path(
        r"^api/auth/jwt/refresh_token/",
        TokenRefreshView.as_view(),
        name="token_refresh",
    ),
    re_path(
        r"^api/auth/jwt/verify_token/", TokenVerifyView.as_view(), name="token_verify"
    ),
    re_path(
        r"^api/update-user-personal-info",
        user_personal_info.UpdateUserPersonalInfo.as_view(),
    ),
    re_path(r"^api/sample-endpoint/", sample_view.AddSample.as_view()),
    re_path(r"^api/add-log/", logs_view.AddLogs.as_view()),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
