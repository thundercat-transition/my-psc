import os

SETTINGS_MODULE = "config.settings.local"

if os.environ.get("ENVIRONMENT") == "dev":
    SETTINGS_MODULE = "config.settings.dev"

if os.environ.get("ENVIRONMENT") == "ua":
    SETTINGS_MODULE = "config.settings.ua"
