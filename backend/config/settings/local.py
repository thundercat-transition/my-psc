from .base import *
import os

DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "mssql",
        "NAME": "master",
        "USER": "SA",
        "PASSWORD": "someSecurePassword10!",
        "HOST": "db",  # set in docker-compose.yml
        "PORT": 1433,  # ms sql port,
        "OPTIONS": {"driver": "ODBC Driver 17 for SQL Server"},  # odbc driver installed
    }
}

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static/")

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")
