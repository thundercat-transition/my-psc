"""
Django settings for backend project.

Generated by 'django-admin startproject' using Django 2.1.4.
Since the start of the project, we have upgraded the version to 2.1.7

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os
from datetime import timedelta

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "%qviowlmp*kitbai+y!%1y=jdl_o3_#7+_ud6l9uwn$$=bxt1y"

ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True

# Application definition
INSTALLED_APPS = [
    # Personal,
    "backend.custom_models",
    "user_management.user_management_models",
    # BASE
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # Requirements
    "corsheaders",
    "rest_framework",
    "djoser",
    "rest_framework.authtoken",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "corsheaders.middleware.CorsMiddleware",
]

STATIC_ROOT = os.path.join(BASE_DIR, "static")
ROOT_URLCONF = "config.urls"
WSGI_APPLICATION = "config.wsgi.application"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# Permission classes is empty because this application does not need an authenticated user for sending POST Requests to the API
REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": (
        "rest_framework.permissions.IsAuthenticatedOrReadOnly",
    ),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.BasicAuthentication",
    ),
}

LANGUAGE_CODE = "en-us"
TIME_ZONE = "Canada/Eastern"
USE_I18N = True
USE_L10N = True
USE_TZ = True

CORS_ALLOW_HEADERS = [
    "accept",
    "accept-encoding",
    "authorization",
    "content-disposition",
    "content-type",
    "origin",
    "user-agent",
    "x-csrftoken",
    "x-requested-with",
]

# Allow the user to log in by email or username
AUTH_USER_MODEL = "user_management_models.User"

# JWT settings for authentication
JWT_AUTH = {
    "JWT_EXPIRATION_DELTA": timedelta(hours=1),
    "JWT_ALLOW_REFRESH": True,
}

# JWT Settings list: https://django-rest-framework-simplejwt.readthedocs.io/en/latest/settings.html
SIMPLE_JWT = {
    "AUTH_HEADER_TYPES": ("Bearer"),
    "ACCESS_TOKEN_LIFETIME": timedelta(minutes=5),
    "REFRESH_TOKEN_LIFETIME": timedelta(days=1),
    "ROTATE_REFRESH_TOKENS": True,
}

# Swagger settings for documentation
SWAGGER_SETTINGS = {
    "LOGIN_URL": "rest_framework:login",
    "LOGOUT_URL": "rest_framework:logout",
}

# Djoser settings for Rest Login (https://djoser.readthedocs.io/en/latest/settings.html)
DJOSER = {
    "SET_PASSWORD_RETYPE": True,
    "SEND_ACTIVATION_EMAIL": False,
    "PERMISSIONS": {
        # Admin Only
        "activation": ["rest_framework.permissions.IsAdminUser"],  #
        "set_username": ["rest_framework.permissions.IsAdminUser"],
        "user_delete": ["rest_framework.permissions.IsAdminUser"],
        "user_list": ["rest_framework.permissions.IsAdminUser"],
        "password_reset": ["rest_framework.permissions.IsAdminUser"],
        # Authenticated
        "token_destroy": ["rest_framework.permissions.IsAuthenticated"],
        # Current User or Admin
        "user": ["djoser.permissions.CurrentUserOrAdmin"],
        "set_password": ["djoser.permissions.CurrentUserOrAdmin"],
        # Any
        "password_reset_confirm": ["rest_framework.permissions.AllowAny"],
        "user_create": ["rest_framework.permissions.AllowAny"],
        "token_create": ["rest_framework.permissions.AllowAny"],
    },
}

# Logging settings (https://docs.djangoproject.com/en/3.0/topics/logging/)
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "[{levelname}] [{asctime}] [{module}] {message}",
            "style": "{",
        },
    },
    "handlers": {
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": "/backend/logs/debug.log",
            "formatter": "simple",
        }
    },
    "loggers": {
        "django": {"handlers": ["file"], "level": "ERROR", "propagate": True},
        "project.sample_view": {"handlers": ["file"], "level": "ERROR"},
    },
}

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
