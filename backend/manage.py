#!/usr/bin/env python
import os
import sys
from config.environment import SETTINGS_MODULE

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", SETTINGS_MODULE)

    # PTVSD setup: https://gist.github.com/veuncent/1e7fcfe891883dfc52516443a008cfcb
    if (SETTINGS_MODULE == "config.settings.local"):
        if os.environ.get('RUN_MAIN') or os.environ.get('WERKZEUG_RUN_MAIN'):
            import ptvsd
            # Attach to port where debugger is connected to
            ptvsd.enable_attach(address=('0.0.0.0', 5678))
            print("Attached remote debugger for Django")

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
