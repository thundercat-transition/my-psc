from django.db import models


class Sample_Model(models.Model):
    # id = models.AutoField(primary_key=True)# Added by default, not required explicitly
    name = models.CharField(blank=False, null=False, max_length=50)
    test_id = models.IntegerField(blank=False, null=False)
    content = models.CharField(blank=False, null=False, max_length=500)

    def __str__(self):
        return '%s %s %s' % (self.name, self.test_id, self.content)
