from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.tokens import RefreshToken
from user_management.user_management_models.user_models import User
from backend.views.utils import is_undefined

"""
Reference for manually creating a new token: https://jpadilla.github.io/django-rest-framework-jwt/#creating-a-new-token-manually
Repurposed to simple-jwt: https://django-rest-framework-simplejwt.readthedocs.io/en/latest/creating_tokens_manually.html
"""


def generate_JWT_token(request):  # Generates a JWT token to send back to the frontend
    app_name = request.data.get("app_name", None)

    # Error checking: make sure that we have the needed parameters
    if is_undefined(app_name) or app_name != "my_psc_gov":
        return Response(
            {"error": "invalid body parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # Dummy adminUser account info
    adminUser = {
        "username": "my_psc_admin",
        "first_name": "my_psc",
        "last_name": "admin",
        "birth_date": "1975-01-01",
        "pri_or_military_nbr": "",
        "email": "my_psc_admin@canada.ca",
        "password": "testPassCATStackAdmin",
    }

    # Get user info if dummy account exists, otherwise create it
    if User.objects.filter(username=adminUser["username"]).exists():
        user = User.objects.get(username=adminUser["username"])
    else:
        user = User.objects.create_user(
            username=adminUser["username"],
            first_name=adminUser["first_name"],
            last_name=adminUser["last_name"],
            birth_date=adminUser["birth_date"],
            pri_or_military_nbr=adminUser["pri_or_military_nbr"],
            email=adminUser["email"],
            password=adminUser["password"],
        )

    # Manually create token for this dummy user and return the token
    refresh = RefreshToken.for_user(user)

    return Response(
        {
            "refresh": str(refresh),
            "access": str(refresh.access_token),
        },
        status=status.HTTP_200_OK,
    )


class CreateJWTtoken(APIView):
    permission_classes = [AllowAny]

    # API call
    def post(self, request):
        return generate_JWT_token(request)
