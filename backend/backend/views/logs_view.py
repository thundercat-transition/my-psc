from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import is_undefined


def save_logs(request):
    raw_logs = request.data.get("logs", None)
    logs = raw_logs[0]

    timestamp = logs.get("timestamp", None)
    severity = logs.get("severity", None)
    message = logs.get("message", None)
    stacktrace = logs.get("stacktrace", None)

    # Error checking: make sure that we have the needed parameters
    if is_undefined(timestamp):
        return Response(
            {"error": "no 'timestamp' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(severity):
        return Response(
            {"error": "no 'severity' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(message):
        return Response(
            {"error": "no 'message' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(stacktrace):
        return Response(
            {"error": "no 'stacktrace' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    return Response(status=status.HTTP_200_OK)


class AddLogs(APIView):
    # API call
    def post(self, request):
        return save_logs(request)
