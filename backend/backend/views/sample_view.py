import datetime
import logging
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from backend.custom_models.sample_model import Sample_Model
from backend.views.utils import is_undefined

logger = logging.getLogger('project.sample_view')


def save_sample(request):
    name = request.data.get("name", None)
    test_id = request.data.get("test_id", None)
    content = request.data.get("content", None)

    # Error checking: make sure that we have the needed parameters
    if is_undefined(name):
        logger.error("no name in request")
        return Response(
            {"error": "no 'name' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_id):
        logger.error("no test_id in request")
        return Response(
            {"error": "no 'test_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(content):
        logger.error("no content in request")
        return Response(
            {"error": "no 'content' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # Prepare new entry for DB by instantiating the model
    sample = Sample_Model(
        name=name, test_id=test_id, content=content)

    # Save the entry to the DB
    sample.save()

    return Response({}, status=status.HTTP_200_OK)


class AddSample(APIView):
    # API call
    def post(self, request):
        return save_sample(request)
