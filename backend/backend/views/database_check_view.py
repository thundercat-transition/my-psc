from rest_framework import viewsets
from backend.custom_models.database_models import DatabaseCheckModel
from backend.serializers.database_check_serializer import DatabaseCheckSerializer


class DatabaseViewSet(viewsets.ModelViewSet):
    # same as 'SELECT * FROM backend_databasecheckmodel;'
    queryset = DatabaseCheckModel.objects.all()
    serializer_class = DatabaseCheckSerializer
    # allows only GET requests
    http_method_names = ["get"]
