import datetime
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from user_management.user_management_models.user_models import User
from user_management.views.utils import is_undefined


def update_user_personal_info(request):
    first_name = request.query_params.get("first_name", None)
    last_name = request.query_params.get("last_name", None)
    username = request.query_params.get("username", None)
    primary_email = request.query_params.get("email", None)
    secondary_email = request.query_params.get("secondary_email", None)
    dob = request.query_params.get("birth_date", None)
    pri_or_military_nbr = request.query_params.get("pri_or_military_nbr", None)
    # making sure that we have the needed parameters
    if is_undefined(first_name):
        return {"error": "no 'first_name' parameter"}
    if is_undefined(last_name):
        return {"error": "no 'last_name' parameter"}
    if is_undefined(username):
        return {"error": "no 'username' parameter"}
    if is_undefined(primary_email):
        return {"error": "no 'email' parameter"}
    if is_undefined(dob):
        return {"error": "no 'birth_date' parameter"}
    # secondary email validation (useful only for manual entry in the DB, not for the Personal Info page, since there is frontend validation first)
    try:
        # if secondary_email string is not empty, validate it
        if secondary_email != "":
            validate_email(secondary_email)
    except ValidationError:
        return {"error": "secondary email is invalid"}
    # getting current user
    current_user = User.objects.filter(username=username)
    # updating user's personal info
    current_user.update(
        first_name=first_name,
        last_name=last_name,
        username=username,
        email=primary_email,
        secondary_email=secondary_email,
        birth_date=dob,
        pri_or_military_nbr=pri_or_military_nbr,
    )
    return status.HTTP_200_OK


class UpdateUserPersonalInfo(APIView):
    def get(self, request):
        return Response(update_user_personal_info(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def update_last_password_change_time(request):
    username = request.query_params.get("username", None)
    # making sure that we have the needed parameters
    if is_undefined(username):
        return {"error": "no 'username' parameter"}
    # getting current user
    current_user = User.objects.filter(username=username)
    # updating user's last password change time
    current_user.update(last_password_change=datetime.date.today())
    return status.HTTP_200_OK


class UpdateUserLastPasswordChangeTime(APIView):
    def get(self, request):
        return Response(update_last_password_change_time(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
