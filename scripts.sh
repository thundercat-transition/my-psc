#!/bin/bash

# Author(s): William Lam

# ======
# Info
# ======
# This script file maps potentially long commands to a simple parameter when running the script from the shell.

# ===============
# Instructions
# ===============
# - Powershell: run "sh ./scripts.sh <parameter>"
# - Bash: run "./scripts.sh <parameter>"

if [[ $# -ne 1 ]]; then
    echo "Please only enter 1 parameter for running this script."
    echo "For more info on existing commands, run this script with the \"help\" parameter."
    exit 2
fi

# ===============
# Command list
# ===============
case $1 in

    "startall")
        docker-compose up
        ;;
    "buildall")
        docker-compose up
        ;;
    "restartall")
        docker-compose down
        docker-compose up
        ;;
    "rebuildall")
        docker-compose down
        docker system prune
        rm -r frontend/node_modules
        rm /frontend/yarn.lock
        docker-compose build
        docker-compose up
        ;;
    "startfront")
        docker-compose -f docker-compose-dev-frontend.yml up
        ;;

    "bashfront")
        docker exec -it my-psc_frontend_1 bash
        ;;

    "test")
        docker exec -it my-psc_frontend_1 yarn run test
        ;;

    "stopall")
        docker stop $(docker ps -a -q)
        ;;

    "help")
        help_text
        ;;

    "sql")
        docker exec -it my-psc_mssql bash -c "/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P \"someSecurePassword10!\""
        ;;

    *)
        echo "Unknown command."
        help_text
        ;;
esac


# ===============
# Functions
# ===============

# Prints the help dialog
function help_text()
{
    echo "
        startall: start all containers
        buildall: build and run all containers
        restartall: shut down and start up the containers
        rebuildall: destory the containers and dependency tree, rebuild all
        startfront: runs the frontend container only
        bashfront: logs into the frontend container via docker exec
        test: runs Jest in the pre-existing frontend container
        stopall: stops all running Docker containers on the system
        sql: logs into the MS SQL Docker container
    "
}