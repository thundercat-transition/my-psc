# Setup Dev Environment

## Software Center requirements

Install the following software via the software center:

- Node.Js
- Docker Desktop

Other applications on the technology stack are installed via the Dockerfile images

## Firefox extensions to install

- [Redux Devtools](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools)
- [React Developer Tools](https://addons.mozilla.org/en-CA/firefox/addon/react-devtools/)
- [a11y-outline (accessibility)](https://addons.mozilla.org/en-CA/firefox/addon/a11y-outline/)
- [Katalon Recorder (automated functional testing)](https://addons.mozilla.org/en-US/firefox/addon/katalon-automation-record/)

## Visual Studio Code - Configuration

We use Visual Studio Code as our IDE.

- Install Visual Studio Code, which is found [here](https://code.visualstudio.com/)

## VSCode Extensions to install

Press Ctrl+Shift+X for the hotkey to select the tab on the left-hand side of the IDE, then install the following VSCode extensions:

- Bracket Pair Colorizer 2, by CoenraadS
- CSS Peek, by Pranay Prakash
- Docker, by Microsoft
- Django, by Baptiste Darthenay
- ESLint, by Dirk Baeumer
- Git History Diff, by Hui Zhou
- Git Graph, by mhutchie
- Path Intellisense, by Christian Kohler
- Peacock, by John Papa
- PostgreSQL, by Chris Kolkman
- Prettier - Code formatter, by Esben Petersen
- Python, by Microsoft
- React Redux ES6 Snippets, by Timothy McLane

## Configuring VSCode Extensions

We use VS Code, and specific linting setup when developing the application. This ensures consistency in our code style.

Linting and autoformatting settings are present in the repository, but the .vscode/settings.json file for your local project still needs to be configured properly.

### SQL Server Extension

- Go to SQL Server extension on the side of VSCode and click the `+` sign on the SQL Server Explorer title. Then add the following:
- `hostname`: localhost
- `Database name`: master
- `Authentication Type`: SQL Login
- `Username`: SA
- `Password`: someSecurePassword10!
- `Save Password`: Yes
- `Profile Name`: optional (name it whatever you want, or just press "Enter" to continue)

### Linting Configuration

If you have not done so already, open the project in VSCode by right-clicking on the folder in File Explorer and select "Open with Code".

- ESLint:
  - The file `.vscode/settings.json` should already exist & be in use as a workspace settings.json file. If not, feel free to port the eslint-specific settings to your own user settings.json file.
  - To ensure that ESLint works, open the Terminal window within VSCode by pressing `` Ctrl+Shift+` ``, then click on the "Output" tab and select "ESLint" from the dropdown menu. There should be no errors shown in the Output window if ESLint is working correctly when a .js, .jsx or .json file is open in another VSCode tab.
