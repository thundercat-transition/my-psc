# Starting a new project based on CAT Stack

## Importing this as a foundation for a new project

When making a new project, **do not make an empty repo!** Instead, perform the following steps:

- Go to this link: https://devops.psc-cfp.gc.ca/DDOREY/my-psc/edit
- Go to "Export Project" -> click on the "Export Project" button. Download the exported project.
- When creating a New Project, click on the "Import" tab: ![alt_text](docs/images/gitlab-export.png)
- Afterwards, import the Exported Gitlab files that were just downloaded: ![alt_text](docs/images/gitlab-export-2.png)

## Variable renaming before starting a new project

- In `docker-compose.yml`, go to the `container_name` name field of the db service. Substitute `your-project-name` with the name of your project repo.
- In `scripts.sh`, replace `my-psc` with the name of your project repo.
  - Docker containers are automatically named with your project repo name as a prefix, so it is important to be consistent.
- In `frontend/package.json`, change the `name` field's value to be the name of your project repo
- In `frontend/public/manifest.json`, change the `short_name` field's value to be the name of your project repo
- Rename `backend/tests/postman/My-PSC.postman_collection.json` to have the prefix of your project repo instead of `My-PSC`.
  - Do the same thing for `backend/tests/postman/environments/local/My-PSC.local.postman_environment.json`, and also change the "name" field's value to use your project repo's name instead.
  - Actually open the Postman files and replace the variable names in the request URL, the tests, the request body & the pre-request scripts properly
- In `backend/README.md`, Find and Replace `my-psc` with the name of your project repo
- In `docs/Security Assessments.md`, Find and Replace `my-psc` with the name of your project repo

In general, just Ctrl+F -> Find in Files for `my-psc` and replace with the name of your project repo where appropriate.

---

# CAT Stack

Docker, React, Redux, Django, Node.js, NGINX, PostgreSQL

## Other Technologies Used

- Visual Studio Code (IDE)
- Jest (testing)
- Enzyme (testing)
- GCWEB v6 (theme)

## Development Environment Setup

- See [SETUP.md](./SETUP.md) to set up your development environment properly.

## Installing the App

- After setting up your development environment, `git clone https://devops.psc-cfp.gc.ca/DDOREY/my-psc`
- In the root directory of this project, run `docker-compose build` to build the Docker image.

## Starting the app

- Run `docker-compose up` to create and run a Docker container that will run the application.
- Go to http://localhost:81/ to view the frontend of the application.
- To stop the Docker container, just press `Ctrl+C` in the terminal.

## scripts.sh file

This .sh file maps certain long/tedious commands to remember/type to a simple command.

- Powershell: run `sh ./scripts.sh help` to see the available commands.
- Bash: run `./scripts.sh help` to see the available commands.

## TODO

Update as needed. When we get more information on other parts of the system (Backend, DB, Nginx), this can be updated.
