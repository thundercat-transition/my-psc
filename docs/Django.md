# Django

Django is a powerful Python framework. It is used as this project's backend system and connects the frontend to the PostgreSQL Database.

## Models & Views

In short, Django's setup to interact with the database (DB) follows the Model-View-Controller (MVC) design pattern. The Model represents a DB table, and the View acts as the View + Controller for the DB system.

References:

- https://docs.djangoproject.com/en/3.0/intro/tutorial02/
- https://docs.djangoproject.com/en/3.0/intro/tutorial03/

## Setting up an API Endpoint

1. Create your view file in `backend/views`.

- This view file should contain a class that takes in an APIView & calls a REST action (get, post, put, delete, etc.), and a function that handles the request:

```python
class AddSurveyStats(APIView):
    def post(self, request):
        return function_to_handle_request(request)
```

Additional resources for requests:

- https://www.django-rest-framework.org/tutorial/2-requests-and-responses/
- https://www.django-rest-framework.org/api-guide/requests/

2. Create your model file in `backend/custom_models`

This step is only needed if you plan to store any data to the PostgreSQL database.

- The model sets the parameters of a particular table in PostgreSQL. Example:

```python
from django.db import models

class Survey_Stats(models.Model):
    # id = models.AutoField(primary_key=True)# Added by default, not required explicitly
    question_id = models.IntegerField(blank=False, null=False)
    answer_id = models.IntegerField(blank=False, null=False)
    answer_text = models.CharField(
        blank=True, null=True, max_length=100)  # Optional field

    def __str__(self):
        return '%s %s %s' % (self.question_id, self.answer_id, self.answer_text)
```

Additional resources for models:

- List of model fields: https://docs.djangoproject.com/en/3.0/ref/forms/fields/
- Models: https://docs.djangoproject.com/en/3.0/topics/db/models/

3. Configure view to save to to the corresponding model

In the function that handles the requests, parse the data from the request & then manipulate the data in the model as you desire.

Example:

```python
def save_survey_stats(request):
    question_id = request.data.get("question_id", None)
    answer_id = request.data.get("answer_id", None)
    answer_text = request.data.get("answer_text", None)

   # Prepare new entry for DB by instantiating the model
   survey_stats = Survey_Stats(
      question_id=question_id, answer_id=answer_id, answer_text=answer_text)

   # Save the entry to the PostgreSQL DB
   survey_stats.save()
```

Additional resources for DB queries:

- https://docs.djangoproject.com/en/3.0/topics/db/queries/

4.  Go to `backend/config/urls.py` to configure your API endpoint URL.

Example:

```python
url(r"^api/insert-endpoint-url-here/", survey_stats_view.AddSurveyStats.as_view()),
```

- Remember to also import your view!

```python
from backend.views import survey_stats_view
```

# Swagger & APIs

Swagger is an open source tool whose purpose is to visualize and interact with the API’s resources without having any of the implementation logic in place. It’s automatically generated from your OpenAPI (formerly known as Swagger) Specification, with the visual documentation making it easy for back end implementation and client side consumption.

Click [here](https://swagger.io/tools/swagger-ui/) for more details about Swagger UI tool.

## How Swagger UI has been setup in this project

### Requirements

The _django-rest-swagger_ package has been added in the [requirements.txt](../backend/requirements.txt) file. We are currently using [version 2.2.0](https://pypi.org/project/django-rest-swagger/).

## How to use Swagger UI

### Accessing the interface

In order to access Swagger UI tool and use all its functionalities, make sure that all your containers are running and working well. Once done, you'll be able to access it with the following link:

http://localhost:8000/

### Unprotected APIs

By default, you will not be connected, so you will only be able to see and manipulate the unprotected APIs.

![swagger screenshot](/docs/images/swagger-view-unprotected-apis.png)

### Protected APIs

However, if you login using your super user credentials, you'll be able to see and manipulate all the APIs.

![swagger screenshot](/docs/images/swagger-view-protected-apis.png)

![swagger screenshot](/docs/images/swagger-view-protected-apis2.png)

![swagger screenshot](/docs/images/swagger-view-protected-apis3.png)

Also, if you login using basic user credentials, you'll only be able to see and manipulate the APIs that you have permission.

### How to manipulate the APIs

The manipulation of the APIs with Swagger UI is very simple and straight forward. Here is how to do so:

1. Choose the API that you want to use and click on "Try it out":

   ![swagger screenshot](/docs/images/swagger-manipulate-apis.png)

2. Fill in the required information (if needed) and click on "Execute":

   ![swagger screenshot](/docs/images/swagger-manipulate-apis2.png)

3. You have now your response:

   ![swagger screenshot](/docs/images/swagger-manipulate-apis3.png)

**For more information about this tool, visit https://swagger.io/.**

# Permissions

- Even when logging in as Admin, you may still view the APIs as an anonymous user. For a workaround, change this setting in base.py ([reference](https://stackoverflow.com/questions/27085219/how-can-i-disable-authentication-in-django-rest-framework)):

```python
REST_FRAMEWORK = {
    ...

    "DEFAULT_PERMISSION_CLASSES": (

    ),

    ...
}
```

In `base.py`, `DEFAULT_PERMISSION_CLASSES` sets the default permissions for each view. To override a permission on a per-view basis, simply redefine `permission_classes = []` in the View's class. A list of default permissions provided by the Django Rest Framework (DRF) API can be found [here](https://www.django-rest-framework.org/api-guide/permissions/#api-reference).

[In general, DRF documentation regarding permissions can be found here.](https://www.django-rest-framework.org/api-guide/permissions/)

## JWT (JSON Web Token) Authentication

This web application currently has no login system & database with user/login information. Despite this, there is still a desire to protect our endpoints from unnecessary attacks and mitigate how easy it is for attackers to figure out the mechanism of doing so.

To accomplish this, we assign a JWT to the user's browser when an API event is called, and it is temporarily stored in the browser's sessionStorage. Simply put, this is another layer of authentication that is created to deter attackers from spamming our endpoints and potentially obtaining undesired access/creating undesired entries in our databases.

In Django, JWTs **must be associated with a User in the application's User model**. Because this web application has no formal login system, a dummy admin account with fixed credentials is used & created in the PostgreSQL database via code in the backend.

Reference documentation:

- https://jwt.io/introduction/
- https://simpleisbetterthancomplex.com/tutorial/2018/12/19/how-to-use-jwt-authentication-with-django-rest-framework.html

## Djoser & JWT

Djoser is a library that provides a set of Django Rest Framework views to handle basic operations involving user accounts. It also contains some endpoints to handle JWT Authentication.

The Djoser JWT Endpoints & the required payload in the request to create, refresh & verify JWTs can be found [here](https://djoser.readthedocs.io/en/latest/jwt_endpoints.html). For setting up the actual URLs though in `urls.py`, I followed the setup instructions [here](https://django-rest-framework-simplejwt.readthedocs.io/en/latest/getting_started.html) and imported the appropriate views from the DRF-SimpleJWT framework.

In general, Djoser documentation can be found here: https://djoser.readthedocs.io/en/latest/introduction.html

## Other notes

- Creating an admin account in the backend container for Django:

```shell
docker exec -it my-psc-backend-1 bash
python manage.py createsuperuser
```

- Swagger GUI may not show API parameters. Todo: make the parameters selectable in Swagger GUI: https://stackoverflow.com/a/41144277
