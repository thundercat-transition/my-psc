# Header

The header is composed of 3 sections

- Accessibility section (the `<nav>` tag) which allows users to skip to content using the keyboard
- The language selection row, which has a button that allows the user to switch the language
  ~~- Search bar section, with 2 subsections~~
  ~~- government of canada image that links back to homepage~~
  ~~- search bar, which searches canada<span>.ca~~
  ~~- Menu with links to other Canada<span>.ca locations~~
  ~~- Breadcrumb Trail~~

## A11Y section

- not implemented (TODO)

## Language Selection

In prod, the language toggle is done using a link to either /en or /fr. Since we already have a language setting action and have used it in a button before (splash page), that is what we are going to do.

Since using an anchor tag with an empty ref is bad practice, we will instead use a button, styled to look like a link:

- rule `.swap-lang-button-link` in `css/app-theme.css`

The button is linked to the setLanguage action, and toggles the language to the opposite one.

The button element must also be connected to the `localizationReducer`, to ensure that the localized text (English / Français) will swap when the button is selected.

This implementation works for most pages, but ones that have been connected to the redux store do not update on button press (`InfoPage.jsx`).

This occurs since the `connect` in App.js is overriden by the child component. To solve this, we can either

- connect the page back to the `localizationReducer`, or
- move the logic that requires a connection to redux to a separate component

We decided on the second option, as it promotes loose coupling, and extracted `StartAssessmentButton.jsx` from the InfoPage.

## Search Bar Section

### Homepage Image Link

Loads 2 `.svg` files for french and english. Uses the state returned by the localizationReducer to show the correct image.

### Search

Simple to implement, following the same style as prod. The search section remains visible in mobile view, but needed a style change to unsure the search button stayed on the same line is the input box.

- see `styles` in `components/SearchBar.jsx`

The search bar is localized, so searching for text while the language is french will bring you to `canada.ca/fr`. The same applies to english.

## Canada<span>.ca Navbar

- not implemented (TODO)

PA Tool gets its menu items from the following sources:

- http://www2.cfp-psc.gc.ca/pat-oap/appIncludes/mega-menu-fra.inc
- http://www2.cfp-psc.gc.ca/pat-oap/appIncludes/mega-menu-eng.inc

The Canada.ca website gets its menu items from the following sources:

- https://www.canada.ca/content/dam/canada/sitemenu/sitemenu-v2-en.html
- https://www.canada.ca/content/dam/canada/sitemenu/sitemenu-v2-fr.html
