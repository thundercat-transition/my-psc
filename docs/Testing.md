# Unit Testing

## Creating a test file:

- Make sure the file is in the tests folder & follows the general file name syntax of: `<name>.test.js`. This will allow Jest to detect this.

## Writing test cases

Some rules of thumb:

- Always try using Enzyme's shallow rendering before using any other type of rendering. Shallow rendering isolates a React component more than other types of rendering for testing (hence the term "unit testing").
- Using `console.log(wrapper.debug())` helps you see which HTML page is rendering.
- Using `console.log(wrapper.props())` or appending `.props()` to different wrapper functions in general really help with the debug process.
- For handling cases with `wrapper.find()` where more than 1 element can be found, consider using `wrapper.contains()` or the `wrapper.find().filterWhere()` predicate function to narrow down the search results
- If searching by string content, always refer to the textResources.js file as that contains the React-Localization strings
- To find if some text is found within a component, use `.text()` combined with `.toContain("string")`
- In general, if all else fails, tagging an element by ID & using `wrapper.find(#id)` will work.
- `.dive()` is used to go 1 level deeper in a React component. For non-React component, use `.shallow()` instead. [Reference](https://github.com/enzymejs/enzyme/issues/1798)
- You can also spy on React class/component methods directly! Simply use the `jest.spyOn()` function & reference the class function by text: https://remarkablemark.org/blog/2018/06/13/spyon-react-class-method/

References:

- Enzyme's API: https://airbnb.io/enzyme/docs/api/
- Enzyme's shallow vs. mount rendering: https://gist.github.com/fokusferit/e4558d384e4e9cab95d04e5f35d4f913

## React Router Testing

- Export the list of routes into its own React component (in this project it is `RoutesList.js`).

- In the unit tests, you can import RoutesList & then use Enzyme's `mount()` function with `<MemoryRouter initialEntries={["/urlName"]}/>` to emulate routing.

Reference articles:

- https://medium.com/@antonybudianto/react-router-testing-with-jest-and-enzyme-17294fefd303
- https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/testing.md

- Testing routing with Redux: https://medium.com/@bugzeeeeee/react-redux-react-router-how-to-test-routing-1111c214e964

## React/Redux unit testing:

If your component connects to a redux store and you get `Invariant Violation: Could not find "store"`,
you'll want to export the component directly:

```js
class MyComponent extends React.Component{
  ...
}
export default connect(mapStateToProps, mapDispatchToProps)(MyComponent);
```

to

```js
export class MyComponent extends React.Component{
  ...
}
export default connect(mapStateToProps, mapDispatchToProps)(MyComponent);
```

Then in the testing component, import the unconnected variant of the component (for unit tests), i.e. instead of:

```jsx
`import MyComponent from "./MyComponent`;
```

, use this instead:

```jsx
import { MyComponent } from "./MyComponent";
```

- When shallowly rendering, pass the props and functions required to the component.
  - If you want to test if functions are being called, mock them.

```
  describe("my component tests", ()=>{
    let mockFunction = jest.fn();

    it("should call someFunction", (){
      shallow (<MyComponent someFunction={mockFunction}>)
        .find("buttonA")
        .simulate("click");

      expect(mockFunction.mock.calls.length).toBe(1);
    });
  });
```

For actually testing reducers, here is a basic primer to follow: https://willowtreeapps.com/ideas/best-practices-for-unit-testing-with-a-react-redux-approach

# Postman testing (for APIs)

## Backend tests

The backend Django application has a set of tests for its views, and uses Postman to manage API tests.

To run the unit tests, you'll need to enter the backend Docker container.

In powershell:

```shell
docker exec -it my-psc-backend-1 /bin/bash

./manage.py test tests
```

To run Postman tests, you'll need to launch Postman desktop app and setup your environment.

1. Import My-PSC local environment:

   ![postman screenshot](/docs/images/import-postman-env.png)

   ![postman screenshot](/docs/images/import-postman-env2.png)

   ![postman screenshot](/docs/images/import-postman-env3.png)

   Then select the following file:
   `backend\tests\postman\environments\local\My-PSC.local.postman_environment.json_`

   You can then put the name of your choice for this new imported environment (e.g. _"My-PSC - Local"_).

2. Make sure that you new environment is selected:

   ![postman screenshot](/docs/images/postman-selected-env.png)

3. Import My-PSC collection:

   ![postman screenshot](/docs/images/import-postman-collection.png)

   ![postman screenshot](/docs/images/import-postman-collection2.png)

   Then select the following file: `backend\tests\postman\My-PSC.postman_collection.json_`

   You have now your new My-PSC collection:

   ![postman screenshot](/docs/images/import-postman-collection3.png)

4. You can either run individual tests or run the whole collection:

   Individual test:

   ![postman screenshot](/docs/images/postman-run-individual-test.png)

   Whole collection:

   ![postman screenshot](/docs/images/postman-run-whole-collection.png)

   ![postman screenshot](/docs/images/postman-run-whole-collection2.png)

   **Note that if you run the whole collection, you may need to delete some data, since the tests are creating new accounts and you also may need to add authorization for protected APIs.**

   ![postman screenshot](/docs/images/postman-authorization-for-protected-apis.png)

## Postman Pre-Request Scripts & Authorization

Postman can run Javascript prior to an individual request (e.g. to obtain an appropriate authentication token). This authentication token can then be set in the environment variables via the following commands (example here is for JWTs in PA Tool):

```js
pm.environment.set("refresh_token", responseJson.refresh);
pm.environment.set("access_token", responseJson.access);
```

These environment variables can then be used in the "Authorization" header. For PA Tool's JWTs, simply select "Bearer" and then reference the appropriate environment variable in the Token field, e.g. `{{access_token}}`.

# References

## Redux testing:

- https://medium.com/netscape/testing-a-react-redux-app-using-jest-and-enzyme-b349324803a9
- https://hackernoon.com/unit-testing-redux-connected-components-692fa3c4441c

# Archives

# Enzyme Setup (for new projects that aren't based off this tech repo)

This project uses Jest & Enzyme for testing.

1. In the project root directory, run `npm install enzyme`
2. Run `npm install --save-dev enzyme-adapter-react-16`

- There is a specific error you'll run into if you do not do this: https://github.com/airbnb/enzyme/issues/1265

3. Ensure that setupTests.js is in the src directory

4. Run `npm test`
