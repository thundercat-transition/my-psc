# Security Assessments

This document will go over some security assessment tools and procedures used for our application.

# Free Security Assessment Tools (Implemented in our project)

## React & JS (frontend)

### yarn's built-in tools

For closed source code (private repos), use `yarn audit`.

- Use the [Selective dependency resolutions method](https://www.rockyourcode.com/fix-npm-vulnerabilities-with-yarn/) to resolve vulnerabilities
  - For each vulnerability, fix it by setting it to the latest version of the vulnerable dependency in the `resolutions:` field of package.json
  - The latest version of a vulnerable dependency can be searched for on the [official npm website](https://www.npmjs.com/)

### ESLint

- Use the ESLint plugin combined with [eslint-plugin-react and the rules to use](https://github.com/yannickcr/eslint-plugin-react)
- Also, use in conjunction with [eslint-plugin-security](https://github.com/nodesecurity/eslint-plugin-security)

#### Common ESLint Errors & how to address them

Generic Object Injection Sink / Variable Assigned to Object Injection Sink:

- This error occurs if you use arrays use the `[]` brackets to access/modify an element in the array. To fix this, use `parseInt(variable, 10)` function inside the square brackets. Example: `array[parseInt(index, 10)]`

## Django & Python (backend)

### Django's built-in tools

For closed source code, consult the [deployment checklist](https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/)

- [Alternate article (more up-to-date)](https://dev.to/coderasha/django-web-security-checklist-before-deployment-secure-your-django-app-4jb8)

Steps to take:

- While the Docker containers are running, log into the backend Docker container and go into the bash shell via `docker exec -it my-psc-backend-1 bash`
- Run `python manage.py check --deploy` to run a system check
- Fix the vulnerability errors as appropriate for the context of the application
- Afterwards, proceed through the rest of the checklist

### Bandit

For closed source code, use [Bandit](https://pypi.org/project/bandit/)

- While the Docker containers are running, log into the backend Docker container and go into the bash shell via `docker exec -it my-psc-backend-1 bash`
- Run `bandit -r ./` to scan through all Python files. The results will be output to the terminal.

## Nginx (server)

- [Nginx Web Server Hardening Guide](https://geekflare.com/nginx-webserver-security-hardening-guide/)
- [Dreamhost's Nginx Security Tips](https://help.dreamhost.com/hc/en-us/articles/222784068-The-most-important-steps-to-take-to-make-an-nginx-server-more-secure)

---

# Paid Security Assessment Tools (consideration for the future)

- [Burp Suite & Burp Static Scan](https://medium.com/@michael.plazek91/learn-how-to-statically-scan-your-react-app-for-security-flaws-738d36064988)
- [Checkmarx CxSAST (for Python & Node.js - CI/CD)](https://www.checkmarx.com/)
- [Deepscan (for React/JS and Node.js) - CI/CD](https://deepscan.io/)
- [Fortify Static Code Analyzer (SCA) (for React/JS and Python, CI/CD)](https://www.microfocus.com/en-us/products/static-code-analysis-sast/overview)
- [Sonarqube (for Python & JS) - CI/CD](https://www.sonarqube.org/)

---

# Archives

## Snyk

For open source code (public repos), use Snyk instead, as the scans are more thorough.

[Comparison between npm audit & Snyk (details)](https://www.nearform.com/blog/comparing-npm-audit-with-snyk/)

## npm audit (note that we are using yarn in this repo now)

For closed source code (private repos), consult the [npm audit documentation](https://docs.npmjs.com/auditing-package-dependencies-for-security-vulnerabilities).

Some notes:

- Requirement is for npm to be at least version 6 or higher.
- Run `npm audit`
- If there are any vulnerabilities, run `npm audit fix` to fix the vulnerabilities

---

# References

- [List of Static Application Security Testing (SAST) Tools](https://github.com/mre/awesome-static-analysis)
- [List of Gitlab CI/CD SAST Tools](https://docs.gitlab.com/ee/user/application_security/sast/)
- [List of Gitlab CI/CD DAST Tools](https://docs.gitlab.com/ee/user/application_security/dast/)
