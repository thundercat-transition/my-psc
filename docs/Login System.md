# Frontend Components

## HeaderLogin component

The `HeaderLogin` component houses the link to Log In/perform the Log Out action. This component is part of the `Header` component, and is therefore shown in the header of every single page to allow for easy access to logging in and logging out. This component will be rendered on every page except the `SplashPage` component (since it does not use the Header component) and the `LoginPage` component.

When the user is not logged in, `HeaderLogin` will render a link that redirects them to the `LoginPage` component.

When the user is logged in, `HeaderLogin` will render the user's username and a link to log the user out and redirect them to the `HomePage` component.

## LoginForm component

This component contains a form with Username and Password fields, and contains a button to submit a login request to the API endpoint. If the login request is successful, then a JSON Web Token (JWT) is retrieved from Django's API endpoint and stored in `sessionStorage` in the `auth_token` field. Redux's loginReducer's `authenticated` and `userInfo { username }` fields are stored when this occurs. The user is then redirected to the `AdminPage` route.

If the login fails, then the user remains on the same page & the `LoginForm`'s `wrongCredentials` state is updated so that the appropriate login error text is rendered, telling the user to try logging in again with the correct credentials. The password field then gains focus.

# Token authentication

## App.js

Once the user is logged in/authenticated and the JWT is stored in sessionStorage, the parent React component in the application `App.js` checks `onBlur` (i.e. when an object in App.js loses focus) to see if the user is still authenticated by sending a JWT to the API endpoint that verifies a JWT's integrity & timestamp.

- If the user is still properly authenticated (i.e. the JWT's access lifetime has not expired yet), then nothing happens.
- If the JWT's access lifetime has expired but the refresh token lifetime has not expired, then the JWT will be refreshed by calling the refresh API endpoint and the new JWT is stored in `sessionStorage`.
- If the JWT's access lifetime and refresh lifetime have both expired, then the application will clear the JWT from `sessionStorage` and reset the Redux state entirely.

## Testing token authentication

- By default, the refresh lifetime of a JWT is 1 day. To test this, go to `backend/config/settings/base.py` and find the `ACCESS_TOKEN_LIFETIME` and `REFRESH_TOKEN_LIFETIME` fields in the `SIMPLE_JWT` dictionary. Change those to a small value temporarily for testing (e.g. 5 seconds and 10 seconds) and then save the changes to `base.py` until the Docker terminal outputs a success message from the backend container.

- Afterwards, create an admin account in the backend container for Django (if you have not already):

```shell
docker exec -it my-psc-backend-1 bash
python manage.py createsuperuser
```

- Remember your username & password. Using those credentials, go to the login page (http://localhost:81/login) and then log in.
- Do nothing for a short amount of time (until `REFRESH_TOKEN_LIFETIME`'s temporary time value has been exceeded), then click somewhere else on the page. You should receive a popup message telling you that your session has expired & that you have been logged out.
