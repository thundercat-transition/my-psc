# Debugging in Django (Python) in Docker containers

The current tech stack is run in Docker, making it initially difficult to have a proper debugging environment for the Django/Python part of the tech stack. Proper debugging allows developers to diagnose more complex bugs if they occur, rather than relying solely on print statements.

## Setup

There are a series of steps we can take to enable & use the debugger for Django:

1. In `backend/requirements.txt`, install the Python VSCode Debugger dependency: `ptvsd == 4.3.2`. This dependency uses VSCode's native debugger library.
2. In `backend/manage.py`, add these lines of code:

```python
if (SETTINGS_MODULE == "config.settings.local"):
        if os.environ.get('RUN_MAIN') or os.environ.get('WERKZEUG_RUN_MAIN'):
            import ptvsd
            # Attach to port where debugger is connected to
            ptvsd.enable_attach(address=('0.0.0.0', 5678))
            print("Attached remote debugger for Django")
```

The first line in particular ensures that the debugger is only attached while developping locally (and not in other environments).

3. In `docker-compose.yml`, open port 5678 in the backend container:

```yml
backend:
  build: ./backend
  volumes:
    - ./backend/:/backend # maps host directory to internal container directory
  working_dir: /backend
  depends_on:
    - db
  ports:
    - 8000:8000
    - 5678:5678
```

4. Configure `.vscode/launch.json`. The port should connect to the debugger's occupied port (5678), and the localRoot & remoteRoot should be that of the backend container:

```json
{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Python: Remote Attach",
      "type": "python",
      "request": "attach",
      "connect": {
        "host": "localhost",
        "port": 5678
      },
      "pathMappings": [
        {
          "localRoot": "${workspaceFolder}/backend",
          "remoteRoot": "/backend"
        }
      ]
    }
  ]
}
```

5. To use this properly, the VSCode Python extension should be installed (install this if you have not done so already): https://marketplace.visualstudio.com/items?itemName=ms-python.python

## Usage

To use the debugger, simply press `F5` to start the debugger.

- If VSCode prompts you on which debugger to run, select `Python Remote Attach`.

Afterwards, enable at least 1 breakpoint where you know Python code will be executed if you do some action in the frontend container. When said action is triggered, the debugger will stop at the breakpoint you enabled. You should be able to see the standard debugging information on the debug bar on the left side of the VSCode window.

Example screenshot: ![alt_text](images/python-debugging.png "image_tooltip")

You can exit the Remote Debugger at any time by pressing the "Disconnect" red-plug button in the top bar.

## References:

- https://www.docker.com/blog/containerized-python-development-part-3/
- https://gist.github.com/veuncent/1e7fcfe891883dfc52516443a008cfcb
