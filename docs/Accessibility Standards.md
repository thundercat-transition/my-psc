# Accessibility Standards

- [React Accessibility (A11y) Documentation](https://reactjs.org/docs/accessibility.html)

- [ARIA 1.1 Authoring practices documentation](https://www.w3.org/TR/wai-aria-practices/)

  - [Menus](https://www.w3.org/TR/wai-aria-practices/#menubutton)

  - [Tab controls](https://www.w3.org/TR/wai-aria-practices/#tabpanel)

  - [Reducing verbosity of tables](https://www.w3.org/TR/wai-aria-practices/#presentation_role)

  - [Button aria-labelledby_attribute](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-labelledby_attribute)

# Articles from David Farough

- [Accessibility implementations for the Gatsby.js framework](https://www.gatsbyjs.org/blog/2020-02-10-accessible-client-side-routing-improvements/)
- [Inclusive Design 24 presentation](https://www.youtube.com/watch?v=Tr21FqQQv-U)
- [Testing organization that evaluated the Gatsby application](https://www.makeitfable.com/)

# React Helmet library

React Helmet is a really simple library to use. Simply import it via:

`import { Helmet } from "react-helmet";`

Helmet is used to manipulate the elements in the document's `<head />`. The ones we are most interested in modifying are `<title />` and `<meta />`, as those will be picked up by a screen reader.

Example usage:

```jsx
<Helmet>
  <title>New HTML Doc Title</title>
</Helmet>
```

- [React Helmet npm documentation](https://www.npmjs.com/package/react-helmet/v/6.0.0-beta.2)

# ESLint Plugins

The project is already using ESLint. Use [eslint-plugin-jsx-a11y](https://github.com/evcohen/eslint-plugin-jsx-a11y) to further enforce accessibility rules on JSX Elements.

# Accessibility Testing - NVDA

## NVDA Screen Reader (Windows)

- [Software Download](https://www.nvaccess.org/download/)
- [Testing & Usage Guide](https://webaim.org/articles/nvda/)
- [Keyboard Shortcuts Reference](https://dequeuniversity.com/screenreaders/nvda-keyboard-shortcuts)

## Accessibility auditing libraries

- [eslint-plugin-jsx-a11y usage](https://web.dev/accessibility-auditing-react/)

## Web browser plugins

- [a11y-outline (Firefox)](https://addons.mozilla.org/en-US/firefox/addon/a11y-outline/)
  - Brings up a list of landmarks, headings, and links without the need for a screen reader

# Issues and how they were solved:

## JAWS reading the ">>" and "<<" on the Next and Previous buttons

This change was relatively simple, as any button with the next simply needed an aria-label added with the non-bracketed text.

## Button / Link Consistency

All link elements are styled as buttons to solve a double-speak issue when screen readers navigate to the button.

## HTML formatting tags being read by screen readers

Certain HTML formatting tags like `<em />`, `<i />`, `<strong />`, `<b />`, and `<br />` are read by screen readers. To circumvent this issue, the following actions have been taken in this application:

- `<i />` and `<em />` tags are now controlled by the CSS class `.emphasisFormat`. To format text inline (without starting a new HTML element block), simply wrap the text with `<span className="emphasisFormat></span>`.

- `<b />` and `<strong />` tags are now controlled by the CSS class `.boldFormat`. To format text inline (without starting a new HTML element block), simply wrap the text with `<span className="boldFormat></span>`.

- `<br />` tags are now controlled by the CSS classes `.spacer` and `no-spacer`. To obtain formatting similar to a line break, wrap text in a `<p />` element and use these classes to dictate whether there should be any margins above and/or below the HTML element. Other HTML block elements like `<div />` could also be used.
