# Katalon Test Cases

## Setup

To get started with creating and running Katalon test cases, you will need to have the Katalon Automation Recorder add-on installed
for either [Firefox](https://addons.mozilla.org/en-CA/firefox/addon/katalon-automation-record/)
or [Google Chrome](https://chrome.google.com/webstore/detail/katalon-recorder-selenium/ljdobmomdgdljniojadhoplhkpialdid).

Once you have this installed, you can open the test suite .html file and run the tests.

## Loading and Saving Test Suites

A testing suite is a collection of test cases.

- To load a test suite, press Ctrl+O and select the test suite .html file.

- To save a test suite, press Ctrl+S to save the test suite to a target destination file.

## Some Important Katalon Operations

#### Record

This operation starts recording most actions performed in the current active tab of your web browser. Useful in general as a macro recording operation.

### Play

Plays the currently selected test case from the very beginning.

## Some Important Katalon Commands

#### open

This command opens a URL, specified in the _target_ field.

#### click

This command clicks on the appropriate element, specified in the _target_ field.

#### storeText

This command stores the text from the element in the _target_ field, into a variable specified by the _value_.

#### verifyText

This command compares the text from the _target_ field to the one in the _value_ field.
The syntax for variables is as follows: `${variableName}`  
You can compare against variables by placing the previous code into the _value_ field.

#### assertText

This command works identically to `verifyText` but with one key difference:
The tests will stop on a failed assertText, but will continue if a failed verifyText occurs.

## Other neat tips & tricks for Katalon

- When Katalon is running, you can select some text and then right-click to bring up the Katalon context menu.

  - In the right-click context menu, there will be a set of Katalon commands (verifyText, storeTitle, assertValue, ...) that you can select.
  - _Important!:_ This menu is visible when not recording, but no commands will be stored unless the record option is selected.

- Adding a new command in the editor will place the new command below the currently selected command.
- You can select multiple Katalon commands in the editor by holding down Shift or Ctrl.
- You can use the standard Copy and Paste hotkey combinations (Ctrl-C and Ctrl-V) to copy and paste Katalon commands.
- When running a Katalon test, it starts the test on the currently active tab. Switching to a new active tab does not stop the Katalon test, and it will keep running in the background!
- Right-clicking a Katalon command in the editor will bring up an option menu. One of the actions is to start running the Test Case from the currently selected command.
  - Useful if you don't want to restart the test case from the very beginning
- If you need a `false` condition to act as a passed test, use the "VerifyNot" version of the command. Example: `VerifyNotText` will check two strings & pass the test if they are different from one another.

# References

- Selenese (Selenium IDE command list): https://docs.katalon.com/katalon-recorder/docs/selenese-selenium-ide-commands-reference.html
