# Refs and Focus Change

If you have a pure component and you want to set focus to an element within it, use a forwardRef and have the parent element set focus.

Converting a pure component to a forwardRef requires some changes

- changing the function signature to add forwardRef
- adding the displayName of the component
- move the export from the function signature

```js
export default function ReferencedHeader({ headerContent }) {
  //render code here
}
```

to

```js
const ReferencedHeader = React.forwardRef(({ headerContent }, ref) => {
  //render
});

ReferencedHeader.displayName = "ReferencedHeader";
export default ReferencedHeader;
```

If you have prop validition, it can remain unchanged.

To pass the ref to the component, in your parent class, create a ref using `React.createRef()`, and pass that into the child component

```js
headerRef = React.createRef()

render(){
  <ReferencedHeader headerContent="Hello World" ref={this.headerRef}>
}
```

Now you can focus that element by using the created ref (remember to use the `.current` property of the ref)

```js
this.headerRef.current.focus();
```

## AlertError component

Conditionally render this component using a React class' state. This component will be rendered & focused based on the conditions of the class' state.

# In the context of our application

A simpler method to do this was used in the `FocusHeader.jsx` component. `FocusHeader` is just a React component that renders its children within an `<h1 />` tag with the proper styling, while containing the base logic for focusing on the `<h1 />` element. This approach improves modularity and reduces repetitive code for each page of the application that uses the HTML element focus feature.
