# React Bootstrap Usage

- WET Template general reference: https://wet-boew.github.io/wet-boew/index-en.html
- WET Template Grid Documentation: https://wet-boew.github.io/wet-boew-styleguide/design/grids-en.html

React Bootstrap Grid Examples:

- https://medium.com/@julianajlk/grids-rows-and-columns-in-react-bootstrap-c36a703c3c45
- https://reactstrap.github.io/components/layout/
- https://react-bootstrap.github.io/layout/grid/

In short, leverage the React Bootstrap grid system by using `<Container />, <Row />` and `<Col />`. Each React row is split up into 12 different columns. There are many ways to customize how an element will be distributed in the grid. The main reason why the grid system is used is to handle content on different screen sizes (e.g. mobile, tablet and desktop screens) without having to worry about too many of the responsive design nuances.

- **Special note**: because we are using the WET Template CSS files & not Aurora, we have to use class names specific to the WET Template instead of the React Bootstrap specific syntax.

- You do still have to view all web pages on different media using Google Chrome's/Firefox's responsive mode debugger to see if there is any unusual behaviour on certain screen resolutions.

The "span" attribute for `<Col />` tells you how many columns the column will actually span (similar to the span attribute in HTML/CSS)

SVG Icon Usage in React (import as a React component): https://blog.lftechnology.com/using-svg-icons-components-in-react-44fbe8e5f91

# CSS Usage

Styling that is shared across all pages, or styling that can change depending on screen size (via media queries) are placed in app-theme.css. These override the default styling for Aurora's and React Bootstrap's CSS files.

If there are page-specific CSS that do not change via media queries, use a local JS object & leverage React's inline styling instead. If media queries are required for that 1 page, separate the page-specific CSS into its own file.

- Example of using a local JS object style with React (styling an anchor tag):

```javascript
const styles = {
  urls: {
    color: "#335075",
    fontSize: "1.25em",
  },
};

// Other code

return (
  <a href="http://www.google.ca" styles={styles.urls}>
    Redirect text
  </a>
);
```
