# Replicating Page Styling

## Adding GCWeb 6.0

The GCWeb theme can be located [here](https://github.com/wet-boew/GCWeb/releases):
github.com/wet-boew/GCWeb/releases

Download the [6.0 distribution](https://github.com/wet-boew/GCWeb/releases/download/v6.0/themes-dist-6.0-gcweb.zip)

Unzip it, and create a folder to hold the theme (`GCWeb-6.0\` was used). Copy:

- `\themes-dist-6.0-gcweb\GCWeb` folder into your project
- `\themes-dist-6.0-gcweb\wet-boew` folder into your project

The theme will not work out the box, as there is a missing asset. To fix it:

- Go to `GCWeb-6.0\GCWeb\assets\`.
- copy `bkg-home-yourgov.jpg` and rename it `your-gov2.jpg`

Go to your App.js, and add the following line:  
`import "./GCWeb-6.0\GCWeb\css\theme.min.css";`

This line adds the GCWeb 6.0 style to your react app.

You are now ready to start styling!

## Matching Styling

Assuming you've used plain `<div>`, `<h1>`, and other HTML elements with no classes, installing the theme will automatically style some elements.

![Intro page with no styling](/docs/images/intro-plain.png)  
![Intro page with after theme install](/docs/images/intro-after-install.png)

Your next step is to match the page you intend. This step assumes you already have the text content present.  
![comparison between Intro pages](/docs/images/comparison-1.png)

Go to the page you intend to match, and turn on developer tools (`F12` in Google Chrome, Internet Explorer, Firefox)

Select an element you want to copy, and Right-Click > Inspect Element (Firefox) or Right-Click > Inspect (Google Chrome)

At this point, the developer console will open (if it is not open already), and you will be able to see what attributes that element has.

You will want to start by matching the class each element has.

> Remember! In React, you use `className="yourClassHere"` and not `class="yourClassHere"` (due to potential keyword ambiguity)

As an example, let's match the "<< Previous", and "Next" buttons

If we inspect element on those buttons, we can see they are of class `btn btn-primary`  
![inspecting button](/docs/images/button-inspect.png)

Let's change our version to match.

BEFORE:

```html
<div>
  <Link to="/intro">{LOCALIZE.navigation.previousButton}</Link>
  {LOCALIZE.formatString("\t")} {/*Spacing between buttons*/}
  <Link to="/assessment">{LOCALIZE.navigation.startButton}</Link>
</div>
```

AFTER:

```html
<div>
  <Link className="btn btn-primary" to="/intro">
    {LOCALIZE.navigation.previousButton}
  </Link>
  {LOCALIZE.formatString("\t")} {/*Spacing between buttons*/}
  <Link className="btn btn-primary" to="/assessment">
    {LOCALIZE.navigation.startButton}
  </Link>
</div>
```

You will notice that the buttons now look very similar to what we want.  
![after class change](/docs/images/button-class1.png)

However, they do not quite match. Why is that?

This is because the parent elements do not have the same style as the child.
You can try to identify which element is missing by looking at the rules that are applied to each element.

![RULES 1](/docs/images/local-rules.png)
![RULES 2](/docs/images/prod-rules.png)

Notice how our button is missing a `.main` class. If you take a look at the actual page, there is a `<main>` element that our buttons are children of.
![inspecting button](/docs/images/prod-main.png)

Lets add that to our Intro page:

```html
<main className="container" role="main" property="mainContentOfPage">
  ...
  <div>
    <Link className="btn btn-primary" to="/intro">
      {LOCALIZE.navigation.previousButton}
    </Link>
    {LOCALIZE.formatString("\t")} {/*Spacing between buttons*/}
    <Link className="btn btn-primary" to="/assessment">
      {LOCALIZE.navigation.startButton}
    </Link>
  </div>
  ...
</main>
```

Our buttons are the correct colour. In addition, all the page content is centered!  
![inspecting button](/docs/images/done.png)

Repeat the above process until all of your pages are styled correctly.

## Adding a Favicon to the application

- [Add Favicon to your React App](https://serverless-stack.com/chapters/add-app-favicons.html)

# React Modal

react-modal is a dependency that is primarily used to display a popup window in React while delivering the appropriate accessibility to the users.

## Documentation

- Modals with GCWeb: https://wet-boew.github.io/themes-dist/GCWeb/demos/overlay/overlay-en.html
- React Modal documentation: https://reactcommunity.org/react-modal/
