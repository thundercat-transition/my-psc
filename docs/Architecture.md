<!----- Conversion time: 1.424 seconds.


Using this Markdown file:

1. Cut and paste this output into your source file.
2. See the notes and action items below regarding this conversion run.
3. Check the rendered output (headings, lists, code blocks, tables) for proper
   formatting and use a linkchecker before you publish this page.

Conversion notes:

* Docs to Markdown version 1.0β18
* Thu Feb 20 2020 09:49:11 GMT-0800 (PST)
* Source doc: https://docs.google.com/open?id=1FaKadenLfVW-flGzDs0lG06SA1XZ2oKEkxntI8pgMVM
* This document has images: check for >>>>>  gd2md-html alert:  inline image link in generated source and store images to your server.
----->

## My-PSC C&C Architecture

Date created: February 6, 2020

Author: Clayton Perroni

## General Information

This document is a combined structure of component and connectors and allocation structures. This following documentation serves to illustrate and detail the architecture decisions made during the My-PSC Java framework conversion project during the end of the fiscal year 2019.

## Primary Presentation

![alt_text](images/arc-My-PSC.png "image_tooltip")

## Element Catalog

Details the properties, relations, behaviors and available interfaces of the elements shown in the primary presentation.

The architecture described above consists of 3 main elements, nginx, nodejs, django. Each of these elements contains sub elements that will be described in other views.

### Elements and their Properties

The following elements are all solution building blocks that can be reused from the CAT application. There is a repository containing a base image starting point for new applications interested in using this stack.

#### NGINX

##### Responsibility

- Maintaining optimal performance when traffic increases to the node server
- Reverse proxy for Node JS and Django
  - Mitigate DoS attacks
  - Lower application privileges because NodeJS and Django do not need to bind to port 80

##### Visibility of Interfaces

- Port 80 to the user (web browser) for serving all content for the application

##### Implementation

- Configuration file for the server
- By extension implements
  - React
  - Redux
  - Router

##### Mapping to Source Code

- Nginx directory in the root repository

##### Test Information

- Requires tests that when a request is made to Node JS we expect to see specific content from Node JS and the same is true for Django

##### Management Information

- Set it and forget it
- Only is modified when protocols for the application change

##### Implementation Constraints

- Containerized using docker

#### Browser

This component is the implementation information of the Backend:Frontend component since the frontend module only serves the the components listed in :Browser

##### Responsibility

- Serving React content
- Core application logic

##### Visibility of Interfaces

The following are exposed through nginx

- Urls as defined by the &lt;Route> components

##### Implementation

- Implemented using the frameworks:
  - React JS
  - Redux
  - React Router
- Look at the Node JS module architecture document for more information

##### Mapping to Source Code

- Located in the frontend directory at the repository root

##### Test Information

- There are Selenium tests that test the functionality of the application on the repository under the selenium folder. These are functional tests and validate the application as a whole but in doing so greatly overlap with this module element
- There are unit tests in the frontend/src/tests folder that are run in the CI/CD pipeline as well as being available in the container at runtime.

##### Management Information

- Any changes to the visual or functional elements of this application will most likely occur in this element.
- The brunt of work effort is performed in this module and as such requires most of the budget.

##### Implementation Constraints

- Containerized using docker
- Specific implementation for the modules of this element can be found in the Node JS module architecture document.

#### Django

##### Responsibility

- ORM to store the candidate responses to the statistical information questions in the political activities questionnaire.
- Frontend to serve the React components to the Browser clients.

##### Visibility of Interfaces

- Exposes urls in the urls.py file in backend directory at the root of the repository

##### Implementation

- Implements the following frameworks:
  - Django
  - Django REST framework
  - Pyunittest
- Look at the Django module architecture document for more information

##### Mapping to Source Code

- Located in the backend directory at the repository root

##### Test Information

- Unit tests are located in the backend directory under the tests folder using Pyunittest
- API tests are located in the postman folder in the root of the repository

##### Management Information

- This element controls the storing of user input in the database. Any changes to the database require changes to this element.

##### Implementation Constraints

- Containerized using docker
- Specific implementation for the modules of this element can be found in the Django module architecture document.

#### Postgres

##### Responsibility

- Data representation of the statistical information questions in the political activities questionnaire.

##### Visibility of Interfaces

- Exposes port 5432

##### Implementation

- Docker image of Postgres alpine 10.1 (Localhost)
- Postgres 10.x as PaaS from Azure

##### Mapping to Source Code

- No source code

##### Test Information

- N/A

##### Management Information

- N/A

##### Implementation Constraints

- N/A

### Relations and their Properties

Sufficiently documented in the primary presentation.

### Element Interfaces

#### NGINX Interfaces

- Port 80 to access the web application
- Accessible from entering the docker container using a bash execution from host machine

#### Django Interfaces

- Port 8000 is exposed for the Django server
- URLs in urls.py

#### Postgres Interfaces

- Port 5432

### Element Behavior

See the Primary Presentation.

## Context Diagram

See the Primary Presentation.

## Variability Guide

#### NGINX Variability

- No Variability

#### Browser Variability

- Variability in the exposed paths
- Variability in the number of Questionnaire pages/questions

#### Django Variability

- Variability in the exposed urls
- Variability in the ORM depends on the statistical information desired to be collected.

#### Postgres Variability

- Variability in the data stored depends on the statistical information desired to be collected.

## Rationale

The presented architecture was selected for the reuse of solution building blocks available from the CAT application. The architecture uses a monolith approach but still utilises a divided frontend and backend.

Using the loosely coupled architecture presented the potential for growth is not hindered by the monolith approach. Future improvements could be made to entirely decouple the frontend from the backend but that would need to be justified with proof of a lack of scalability from the application.

Given the rationale above, the architecture proposed in this document is sound and is aligned with department architecture roadmaps and goals.

## Referential Material

#### Project Information

This document was created for the java framework upgrade Project and My-PSC application.

<!-- Docs to Markdown version 1.0β18 -->
