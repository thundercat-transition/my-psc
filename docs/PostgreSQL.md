# PostgreSQL

Open the VSCode Extension known as `PostgreSQL Explorer` (elephant icon on the left bar) by clicking on it.

Go to the model you wish to look at: a model in Django represents a table in PostgreSQL. Right-click it and you can execute PostgreSQL queries on the table.

Checking the PostgreSQL Database contents is important to ensure API calls function correctly.

## Some useful queries:

```sql
SELECT * FROM "table_name" -- Displays all rows of the table

DELETE FROM "table_name"; -- Clears the table contents for testing purposes
```
