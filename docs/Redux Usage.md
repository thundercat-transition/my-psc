# Redux patterns & architecture in our application

Redux consists of a few main components that we actively maintain:

- Actions (found in an action file)
- actionTypes.js (central list of all available actions ; each action imports an action type from this file)
- Reducers (found in a reducer file)

All of these are found in the `frontend/src/redux` folder in our application. Generally speaking, each action file corresponds to a reducer file, as most actions are generally used just for 1 reducer.

For more universal actions that are used by multiple reducers, they are generally placed in the `otherActions.js` action file.

### Reducers

The reducers are functions that take in the parameters of an action as an input & then modify + return the data. Often times, they are the objects you import into a component to work with the state/data stored in the Redux Store.

#### Rules of Reducers

- Must return any value besides undefined
- Produces state, or data to be used inside of your app using only the previous state & the action (i.e. reducers are pure)
- Must not reach "out of itself" to decide what value to return
- Must not mutate its input "state" argument

#### Other points:

- Always takes in 2 parameters: the previous (old) state, and the action. Typically shown as `(state, action)`
  - Handle the initial condition of the state in case it does not exist. Either put it as an empty array, null, or some kind of initial state defined elsewhere (like in the action)
- Check the action type with a switch/if statement.

  - If the action matches, then return something
  - If the action does not match, then simply return the previous state

- If you return just the state without modifying it, then the application state will not update (since it is receiving the same state). This is a good thing as it prevents unnecessary page reloads!

- State mutation rules: ![State mutation rules](/docs/images/ReduxStateMutations.png)

  - Reference: S. Grider, Modern React with Redux, January 1, 2020. Accessed on: February 14, 2020. [https://www.udemy.com/course/react-redux/learn/lecture/12586898]. Available: Udemy database.

- In the reducer, when updating the state & making changes to it, make sure that the state is wrapped as a JS object (and not as an array).

### .js & ,jsx files that connect to the Redux Store

- These types of files always contains the following elements:

```jsx
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// This function gets the state from the Redux store & passes the desired parameters to this JS/JSX object's properties (this.props)
// This is also run each time the state gets updated, i.e. by one of the actions specified in the connect() function
const mapStateToProps = (state) => {
  return {
    sections: state.answerReducer,
    // i.e. Anything that you want this JS file to have access to from a reducer in the store.
    // The key name can be changed to anything you want (if you wanted to name the key differently, for example)
  };
};

// This function binds the action creators to Redux's dispatch
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setAnswer,
      actionNum2,
    },
    dispatch
  );

// Connects the JS/JSX object to the Redux Store via connect().
export default connect(mapStateToProps, mapDispatchToProps)(AnswersList);
```

- Use `console.log(state)` in mapStateToProps, and `console.log(this.props)` anywhere else in the JSX and check your browser's console to help debug while developing!

# Using actions

After importing the action creator & putting it into the `connect()` function in the file, you can call the action creator to perform an action through `this.props.actionNum2()` (if you wanted to call the actionNum2 action).

- In the inner workings of Redux, what this does is call store.dispatch() with that action - so we never have to access the store directly to call an action & get it passed to the reducers & update the store state

- Do not call the action directly via the import! It always has to be called through that JS/JSX object's props

# References

- [Redux official documentation](https://react-redux.js.org/using-react-redux/connect-mapstate)
- [Introduction to React Redux (examples)](https://medium.com/javascript-in-plain-english/the-only-introduction-to-redux-and-react-redux-youll-ever-need-8ce5da9e53c6)
- [Immutability Update Patterns](https://redux.js.org/recipes/structuring-reducers/immutable-update-patterns/)

Udemy Course:

- [Stephen Grider's React-Redux Udemy Course](https://www.udemy.com/course/react-redux/)
