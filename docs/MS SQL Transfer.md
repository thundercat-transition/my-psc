Author: Elijah Macdonald

# Moving from postgreSQL to MS SQL

## In the docker-compose

- Under the 'db' service, swap the image to `mcr.microsoft.com/mssql/server:latest`, or whatever version MS sql server you will be using.
- ensure that port 1433 is mapped, as this is the default port that MS SQL uses.
- under the environment section, add `ACCEPT_EULA: "Y"`, and specifiy an admin password under `SA_PASSWORD`
- note that this admin password should only be done for a local build. If you intend to move this to production, the container password shoud be an environment variable and not exposed

Your db section in docker-compose should look something like this.

```yaml
db:
  restart: always
  image: mcr.microsoft.com/mssql/server:latest
  container_name: "msSQL"
  ports:
    - "1433:1433"
  volumes:
    - mssql-data:/var/lib/mssql/data/
  environment:
    TZ: "America/Toronto"
    ACCEPT_EULA: "Y"
    MSSQL_SA_PASSWORD: "someSecurePassword"

  ...

volumes:
  mssql-data:
```

## Dockerfile

Since we are installing the [PYODBC driver](https://github.com/mkleehammer/pyodbc), we also need to ensure the correct build requirements are present: See [here](https://github.com/mkleehammer/pyodbc/wiki/Install#debian-stretch) for pyodbc build requirements. This are satisfied in our DockerFile.

pyodbc needs gcc and g++ to compile, and unixodbc-dev to run. g++ is installed and removed after the build, but unixodbc is kept on the image

We also need to add the ms repository to install the correct ms sql server drivers.

```dockerfile
# Install dependencies
ADD requirements.txt /backend/
RUN apt-get update \
  && apt-get install -y --no-install-recommends g++ unixodbc-dev curl \
  && rm -rf /var/lib/apt/lists/* \
  && pip install -r requirements.txt \
  && apt-get purge -y --auto-remove g++

# apt-get and system utilities
RUN apt-get update && apt-get install -y \
  curl apt-utils apt-transport-https debconf-utils gcc build-essential g++\
  && rm -rf /var/lib/apt/lists/*

# adding custom MS repository
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list

# install SQL Server drivers
RUN apt-get update && ACCEPT_EULA=Y apt-get -y install msodbcsql17
# unixODBC dev headers
RUN apt-get -y install unixodbc-dev
```

## For DJANGO

[Django unfortunately does not officially support MS SQL database](https://docs.djangoproject.com/en/3.0/ref/databases/), however, there is a [driver that allows us to use it](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15#debian17)

The base image we are using is [python:3.8-slim](https://github.com/docker-library/python/blob/6a981ebc3ba38d0668db58813f309e58763438e1/3.8/buster/slim/Dockerfile), which runs on debian buster (aka debian 10)

In backend/requirements.txt, add `django-mssql-backend==2.8.1` as one of the dependencies.

### Django database settings:

The instructions in [this medium article](https://medium.com/@royce236/django-and-ms-sql-server-2012-connection-2018-120c54dfc037) were used to configure this.

Where you have your django database config, replace the engine with "mssql".

By default, the ms sql server docker image generates a 'master' database, which we will be using.
Similarly, the user+password can be adjusted to whatever you specified in the docker-compose, or the credentials for your server.

To find out what drivers you have installed, use the following commands:

`docker exec -it (backend-container) bash`
`python`
`import pyodbc`
`pyodbc.drivers()`

where (backend-container) is the hash or name of your backend.

pick one, and specify it under the Django databases configuration (OPTIONS>driver):

You can change the NAME, USER, and PASSWORD fields as needed to whatever user and database you will be connecting to. For now, we are using the System Administrator, as this is a local build.

When moved to production, ensure that the password is gotten via an environment variable, and not exposed.

```
DATABASES = {
    "default": {
        "ENGINE": "mssql",
        "NAME": "master",
        "USER": "SA",
        "PASSWORD": "someSecurePassword10!",
        "HOST": "my-psc-dev-db",  # set in docker-compose.yml
        "PORT": 1433,  # ms sql port,
        "OPTIONS": {    # odbc driver installed
            "driver": "ODBC Driver 17 for SQL Server"
        }
    }
}
```

## Connecting to the MS SQL database

With any luck, you should have an MS SQL Database up and running, and django should be able to connect to it.

For some simple commands, [see here](https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash#connect-to-sql-server)

Access ms sql command line

`docker exec -it (ms-sql-container) bash`
`/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "<YourPassword>"`

In the repo, this has been shortcutted for convenience in `./scripts.sh`. The command to write in a terminal in the project's root directory (for bash) would be:

`./scripts.sh sql`

### Quick TSQL commands

To show all tables:

```sql
SELECT * FROM INFORMATION_SCHEMA.TABLES;
GO
```

To view the custom_models_sample_model table:

```sql
SELECT * FROM custom_models_sample_model
GO
```

# Reading / Links used

https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash

https://medium.com/@royce236/django-and-ms-sql-server-2012-connection-2018-120c54dfc037

https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15#debian17

https://github.com/mkleehammer/pyodbc/wiki/Install#debian-stretch

https://stackoverflow.com/questions/42224701/odbc-driver-13-for-sql-server-cant-open-lib-on-pyodbc-while-connecting-on-ubunt

note that the answer for the previous stackoverflow question needs to be customized based on what image you are running on.
