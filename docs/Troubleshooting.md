# Known issues and debugging techniques

## Every single yarn test is failing

Run `yarn test --clearCache` and then run `yarn test` once again.

- Reference: https://github.com/facebook/create-react-app/issues/6398

## Docker and the command line

- Cannot use VM on windows 10 and Docker at the same time
- Cannot VPN into network and use Docker (nginex crashes) when working remotely
  - This seems to be related to the network drives?
  - If docker is running and then I vpn into the network, the computer detects no network drives
  - After running 'docker-compose down' and restating docker, the network drives appear
- Cannot execute 'docker exec' command in git bash
- If you are using DockerToolbox rather than Docker for Windows on Windows, see rm-docker-toolbox-setup

## There appears to be trouble with your network connection. Retrying...

i.e.

```shell
frontend-1 | info There appears to be trouble with your network connection. Retrying...
frontend-1 | info There appears to be trouble with your network connection. Retrying...
frontend-1 | error An unexpected error occurred: "https://registry.yarnpkg.com/ p-is-promise/-/p-is-promise-2.0.0.tgz: Client network socket disconnected before secure TLS connection was established".
frontend-1 | info If you think this is a bug, please open a bug report with the information provided in "/home/node/yarn-error.log".
frontend-1 | info Visit https://yarnpkg.com/en/docs/cli/install for documentation about this command.
frontend-1 | yarn run v1.12.3
frontend-1 | \$ react-scripts start
frontend-1 | /bin/sh: 1: react-scripts: not found
frontend-1 | error Command failed with exit code 127.
frontend-1 | info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
my-psc-frontend-1 exited with code 1
```

Delete yarn.lock file and node_modules and run

```shell
docker-compose up
```

again.

This time it should build. If not, or if this results in changes to your yarn.lock file, simply run

```shell
git checkout frontend/yarn.lock
```

to checkout the old version and then it should work again.

## Cannot start service nginx

On Windows 10, sometimes docker shows the following errors when starting up

### Error #1

```shell
$ docker-compose up
Creating network "my-psc-default" with the default driver
Creating my-psc-frontend-1 ... done
Creating my-psc-db-1       ... done
Creating my-psc-backend-1  ... done
Creating my-psc-nginx-1    ... error

ERROR: for my-psc-nginx-1  Cannot start service nginx: OCI runtime create failed: container_linux.go:344: starting container process caused "process_linux.go:424: container init caused \"rootfs_linux.go:58: mounting \\\"/host_mnt/c/_DEV/git/project-thundercat/nginx/nginx-proxy.conf\\\" to rootfs \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged\\\" at \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged/etc/nginx/conf.d/default.conf\\\" caused \\\"not a directory\\\"\"": unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type

ERROR: for nginx  Cannot start service nginx: OCI runtime create failed: container_linux.go:344: starting container process caused "process_linux.go:424: container init caused \"rootfs_linux.go:58: mounting \\\"/host_mnt/c/_DEV/git/project-thundercat/nginx/nginx-proxy.conf\\\" to rootfs \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged\\\" at \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged/etc/nginx/conf.d/default.conf\\\" caused \\\"not a directory\\\"\"": unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type
Encountered errors while bringing up the project.
```

To solve this, do the following steps

Ensure the application is not running

```shell
docker-compose down
```

Run PowerShell as admin.

Run

```shell
Set-NetConnectionProfile -interfacealias "vEthernet (DockerNAT)" -NetworkCategory Private
```

Close powershell

Restart docker (right-click on the docker icon -> restart)

After the restart, run

```shell
docker-compose up
```

Also, make sure that the C drive is properly mounted. To do this, unmount & mount the C drive in `Docker -> Settings -> Shared Drives`.

And everything should be working again

### Error #2

```shell
PS C:\_DEV\IdeaProjects\thundercat\project-thundercat> docker-compose up
Creating network "my-psc-default" with the default driver
Creating my-psc-postgres   ... done
Creating my-psc-frontend-1 ... done
Creating my-psc-backend-1  ... done
Creating my-psc-nginx-1    ... error

ERROR: for my-psc-nginx-1  Cannot start service nginx: driver failed programming external connectivity on endpoint my-psc-nginx-1 (31bebcca94c04c380b7ad1af1bc492488d3bf3fb5b19db9356592e0b5a7028ed): Error starting userland proxy: Bind for 0.0.0.0:80: unexpected error Permission denied

ERROR: for nginx  Cannot start service nginx: driver failed programming external connectivity on endpoint my-psc-nginx-1 (31bebcca94c04c380b7ad1af1bc492488d3bf3fb5b19db9356592e0b5a7028ed): Error starting userland proxy: Bind for 0.0.0.0:80: unexpected error Permission denied
ERROR: Encountered errors while bringing up the project.
```

To fix this issue, you simply need change the nginx port in _docker-compose.yml_ file. You can use ports: 81:8080. If you do that, make sure that you are not committing these changes to master.

And everything should be working again

**Note that this is a temporary fix and we need to discuss it later.**

## Drive sharing errors

if you get a similar error to the following:  
`ERROR: for my-psc-backend-1 Cannot create container for service backend: b'Drive sharing seems blocked by a firewall'`  
Then the following steps can be performed to remedy the situation.

1. Go to the network adapters
2. Go to DockerNAT adapter properties
3. Uninstall File and Printer Sharing
4. Install File and Printer Sharing under Microsoft
5. Run the the "error 1" instructions above

All errors should be resolved when using docker-compose up

## [emerg] 1#1: host not found in upstream "backend:8000" in /etc/nginx/conf.d/default.conf:3

After making changes to the docker config, nginx will not start properly

To fix this, run

```shell
docker-compose up --build
```

rather than

```shell
docker-compose up
```

## Docker doesn't want to succeed for some reason

If you have run Docker pretty extensively in the past, try running `docker image prune -a`, `docker volume prune`, and `docker system prune`. Doing so will remove any images & force them to be obtained & rebuilt from scratch.

## Frontend container fails to build: react-scripts not found

If you encounter this error:

```shell
frontend-1  | /bin/sh: 1: react-scripts: not found
```

Try deleting the node_modules, .yarn/cache and .yarn/unplugged folders and making sure your `yarn.lock` file contents do not differ from the repo's.

Afterwards, run `docker-compose build` in your project, and then run `docker-compose up` afterwards.

# Archives

## Debugging '...\r'

To permanently fix this issue, if adding .gitattributes alone does not fix the issue in the repo, please run some of the git commands here (it should resolve the issue after fresh cloning the repo after trying some of these git commands): https://stackoverflow.com/questions/2517190/how-do-i-force-git-to-use-lf-instead-of-crlf-under-windows

We have seen issues with how new lines are saved in different environemnts depending on where they were created. This _should_ be fixed with the `.vscode/settings.json` file and the `.gitattributes` file, but this will be kept in the document for archive purposes.

Open '.gitattributes'

Add '<file_pattern> -crlf'

Commit '.gitattributes'

Open file in question in notepad++

Open Find+Replace dialog

Replace '\r' with ''. Make sure that 'Search Mode' is in 'Extended mode'

i.e.

' /usr/bin/env: ‘python\r’: No such file or directory' in error message

Open '.gitattributes'

Add '\*.py -crlf'

Commit, replace in notepad++
